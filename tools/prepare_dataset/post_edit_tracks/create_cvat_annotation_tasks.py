import os
import numpy as np
import cv2
import shutil
import argparse
import subprocess
import tempfile
import zipfile


def write_seqinfo_file(seq_length, output_file):
    seq_ini_template = [
        "[Sequence]",
        "name=Test",
        "imDir=img",
        "frameRate=10",
        f"seqLength={seq_length}",
        "imWidth=640",
        "imHeight=480",
        "imExt=.jpg"]
    with open(output_file, "w") as f:
        f.write("\n".join(seq_ini_template))


labels_json_str = """[
    {
        "name": "person",
        "color": "#fa3253",
        "attributes": []
    }
]"""

def write_labels_json_file(output_file):
    with open(output_file, "w") as f:
        f.write(labels_json_str)


def apply_agc_linear(img, min_temp=17, max_temp=30):
    """Linear AGC between fixed temperature range
    img:    uint16 or int16 image
    """
    min_temp = int(min_temp * 100)
    max_temp = int(max_temp * 100)

    if img.dtype == np.uint16:
        img = img.astype(np.int16)

    # clip temperatures
    img = np.clip(img, a_min=min_temp, a_max=max_temp).astype(np.int32)

    # rescale to value between 0 and 255
    img = (((img - min_temp) * 255) / (max_temp - min_temp)).astype(np.uint8)

    return img


def create_thermal_video_file(src_file, dst_file):
    """Create a video file with normalized thermal data from the raw .tiff files
    """
    print(src_file)
    cap = cv2.VideoCapture(src_file, cv2.CAP_IMAGES)
    writer = None
    num_frames = 0
    while True:
        ret, image = cap.read()
        if not ret:
            break
        h, w = image.shape
        num_frames += 1
        # normalize the data
        image = apply_agc_linear(image)
        image = cv2.applyColorMap(image, cv2.COLORMAP_JET)
        if writer is None:
            writer = cv2.VideoWriter(dst_file, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 8, (w, h))
        writer.write(image)
    cap.release()
    writer.release()

    return num_frames


def zipdir(src_dir, zipfilename):
    with zipfile.ZipFile(zipfilename, 'w', zipfile.ZIP_DEFLATED) as zipf:
        # ziph is zipfile handle
        for root, dirs, files in os.walk(src_dir):
            for file in files:
                zipf.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(src_dir, '..')))


def make_annotation_zip(working_dir, input_tracks_file, seq_len_video, zipfilename):
    gt_dir = os.path.join(working_dir, 'gt')
    os.makedirs(gt_dir)
    # copy input tracks file to gt/gt.txt
    shutil.copyfile(input_tracks_file, os.path.join(gt_dir, 'gt.txt'))
    # create gt/labels.txt
    with open(os.path.join(gt_dir, 'labels.txt'), 'w') as f:
        f.write('person\n')
    # create seqinfo.txt file
    write_seqinfo_file(seq_len_video, os.path.join(working_dir, 'seqinfo.ini'))
    # archive folder in .zip file
    zipdir(working_dir, zipfilename)


def docker_cp(src, dst):
    cmd = ['docker', 'cp', src, 'cvat:' + dst]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)


def docker_create_task(task_name):
    """Create task and upload annos"""
    cmd = ['docker', 'exec', '-it', 'cvat', 'bash', '-ic',
           'python3 ~/utils/cli/cli.py '
           '--auth admin:admin '
           f'create "{task_name}" '
           '--labels /tmp/labels.json '
           'local /tmp/video.avi '
           '--annotation_format "MOT 1.1" --annotation_path /tmp/annos.zip']
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)


def generate_task_name(absfoldername, max_name_length=64):
    print(absfoldername)
    path_elements = absfoldername.split(os.sep)
    result = path_elements[-1]
    for path_element in reversed(path_elements[:-1]):
        if len(result + ' ' + path_element) <= max_name_length:
            result += ' ' + path_element
    return result


parser = argparse.ArgumentParser(description='Create annotation tasks in the CVAT tool for editting on the final thermal dataset. This can be usefull to correct for spatial/temporal errors between the color and thermal camera')
parser.add_argument('input', nargs='+', type=str, help='Input folder containing .tiff image frames and a gt.txt file with the ground truth')
args = parser.parse_args()

for input_folder in args.input:
    with tempfile.TemporaryDirectory() as working_dir:

        ### Create pseudo colour video file from raw thermal images
        video_filename =os.path.join(working_dir, 'video.avi')
        seq_len_video = create_thermal_video_file(os.path.join(input_folder, 'image%07d.tiff'), video_filename)

        ### Create annos.zip
        zipfilename = os.path.join(working_dir, 'annos.zip')
        make_annotation_zip(os.path.join(working_dir, 'annos'), os.path.join(input_folder, 'gt.txt'), seq_len_video, zipfilename)

        ### Create labels.json
        labelsfilename = os.path.join(working_dir, 'labels.json')
        write_labels_json_file(labelsfilename)

        ### Copy all required files to docker container /tmp folder
        docker_cp(zipfilename, '/tmp/annos.zip')
        docker_cp(labelsfilename, '/tmp/labels.json')
        docker_cp(video_filename, '/tmp/video.avi')

    ### Create new CVAT task
    task_name = generate_task_name(os.path.abspath(input_folder))
    docker_create_task(task_name)

