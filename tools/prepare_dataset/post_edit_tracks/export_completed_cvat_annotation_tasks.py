import os
import json
import shutil
import argparse
import subprocess
import tempfile
import zipfile


def get_completed_task_ids_and_names():
    cmd = ['docker', 'exec', '-it', 'cvat', 'bash', '-ic',
           'python3 ~/utils/cli/cli.py '
           '--auth admin:admin '
           'ls --json']
    print(" ".join(cmd))
    out = subprocess.check_output(cmd)
    json_str = out.decode("utf-8")
    task_data = json.loads(json_str)
    ids = [task['id'] for task in task_data if task['status'] == "completed" and not task['name'].startswith('cut')]
    names = [task['name'] for task in task_data if task['status'] == "completed"  and not task['name'].startswith('cut')]
    return ids, names


def dump_task(task_id, output_file):
    cmd = ['docker', 'exec', '-it', 'cvat', 'bash', '-ic',
           'python3 ~/utils/cli/cli.py '
           '--auth admin:admin '
           f'dump --format "MOT 1.1" {task_id} /tmp/completed_annos.zip']
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)
    cmd = ['docker', 'cp', 'cvat:/tmp/completed_annos.zip', output_file]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)


def delete_task(task_id):
    cmd = ['docker', 'exec', '-it', 'cvat', 'bash', '-ic',
           'python3 ~/utils/cli/cli.py '
           '--auth admin:admin '
           f'delete {task_id}']
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)


def get_path_from_task_name(root, task_name):
    elems = task_name.split()
    return os.path.join(root, *reversed(elems))


parser = argparse.ArgumentParser(description='Export all completed CVAT annotation tasks in MOT format')
parser.add_argument('root', type=str, help='Root to use as a base path')
parser.add_argument('--delete', help='Delete completed tasks after exporting their annotations', action='store_true')
args = parser.parse_args()


task_ids, task_names = get_completed_task_ids_and_names()

# compute output paths from task name and check whether export is needed and possible
output_paths = []
selected_task_ids = []
for task_id, task_name in zip(task_ids, task_names):
    output_path = get_path_from_task_name(args.root, task_name)
    print(output_path)

    # Can only export is output dir exists
    if not os.path.exists(output_path):
        raise ValueError(f"Inferred output path {output_path} does not exists")

    # Will only export if export wasn't already performed for this task
    output_file = os.path.join(output_path, 'gt_corrected.txt')
    if os.path.exists(output_file):
        print("Found", output_file, "-> Skipping export")
        continue
    output_paths.append(output_path)
    selected_task_ids.append(task_id)

# Do export
for task_id, output_path in zip(selected_task_ids, output_paths):
    with tempfile.TemporaryDirectory() as working_dir:
        # export zip
        local_zip_file = os.path.join(working_dir, f'completed_annos.zip')
        dump_task(task_id, local_zip_file)

        # extract 'gt/gt.txt' file
        anno_file = 'gt/gt.txt'
        with zipfile.ZipFile(local_zip_file, 'r') as zipobj:
           # Extract all the contents of zip file in current directory
              zipobj.extract(anno_file, path=working_dir)

        # copy gt.txt to target location
        output_file = os.path.join(output_path, 'gt_corrected.txt')
        print("Store completed annotations file from task", task_id, "to", output_file)
        shutil.copyfile(os.path.join(working_dir, anno_file), output_file)

    if args.delete:
        print("Deleting tasks")
        delete_task(task_id)
