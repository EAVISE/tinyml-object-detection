import os
import argparse
from pathlib import Path


parser = argparse.ArgumentParser(description='Recursively search for `video_trimmed.mkv` and `timestamps_trimmed.txt`'
                                             'files and rename them to `video.mkv` and `timestamps.txt` (overriding the original video)')
parser.add_argument('folder', type=str, help='Folder to search in')
args = parser.parse_args()

for trimmed_video_file in Path(args.folder).rglob('cut*/video_trimmed.mkv'):
    trimmed_timestamps_file = trimmed_video_file.parents[0] / 'timestamps_trimmed.txt'
    video_file = trimmed_video_file.parents[0] / 'video.mkv'
    timestamps_file = trimmed_video_file.parents[0] / 'timestamps.txt'
    if not trimmed_timestamps_file.is_file():
        print("Did not find", trimmed_timestamps_file, "-> skipping")
        continue
    if not video_file.is_file():
        print("Did not find", video_file, "-> skipping")
        continue
    if not timestamps_file.is_file():
        print("Did not find", timestamps_file, "-> skipping")
        continue
    print("renaming", trimmed_video_file, "->", video_file)
    print("renaming", trimmed_timestamps_file, "->", timestamps_file)
    os.rename(trimmed_video_file, video_file)
    os.rename(trimmed_timestamps_file, timestamps_file)
