import os
import cv2
import argparse
import subprocess

def get_timestamps(filename):
    timestamps = []
    with open(filename) as f:
        for line in f.read().splitlines():
            timestamps.append(line)

    return timestamps


def render_frame(cap, window_name, frame_nr, trim_begin, trim_end):
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_nr)
    ret, frame = cap.read()
    assert ret

    # render begin/end frame numbers
    cv2.putText(frame, f"trim range: [{trim_begin}, {trim_end}]", (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 3)
    cv2.imshow(window_name, frame)


def trim_video(trim_begin, trim_end, input_video, output_video, input_timestamps, output_timestamps):
    """Trim video in range [trim_begin, trim_end] -> including trim_begin and trim_end frame numers
    """
    # trim video
    cmd = ["ffmpeg",
           "-y",                                    # don't ask if it may override an existing file
           "-i", input_video,
           "-vf", f"select=between(n\,{trim_begin}\,{trim_end}),setpts=PTS-STARTPTS",
           "-c:v", "libx264",
           output_video]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)

    # trim timestamps
    timestamps = get_timestamps(input_timestamps)
    trimmed_timestamps = timestamps[trim_begin:trim_end + 1]
    with open(output_timestamps, "w") as f:
        f.write("\n".join(trimmed_timestamps))

parser = argparse.ArgumentParser(description='Trim a video file together with its timestamps file by redefining start and end frame')
parser.add_argument('folder', type=str, help='Folder containing `video.mkv` and `timestamps.txt` file')
args = parser.parse_args()

# filename
video_file = os.path.join(args.folder, 'video.mkv')
timestamps_file = os.path.join(args.folder, 'timestamps.txt')
output_video_file = os.path.join(args.folder, 'video_trimmed.mkv')
output_timestamps_file = os.path.join(args.folder, 'timestamps_trimmed.txt')

# video capture
cap = cv2.VideoCapture(video_file)
video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
title_window = "Video trimmer"
frame_nr = 0
trim_begin = 0
trim_end = video_length - 1


def on_trackbar(val):
    global frame_nr
    frame_nr = val
    render_frame(cap, title_window, frame_nr, trim_begin, trim_end)

cv2.namedWindow(title_window)
trackbar_name = f'Position (frame nrs)'
cv2.createTrackbar(trackbar_name, title_window , 0, video_length - 1, on_trackbar)


while True:
    render_frame(cap, title_window, frame_nr, trim_begin, trim_end)
    k = cv2.waitKey(0)
    if k == ord('d'):
        frame_nr += 1
        if frame_nr == video_length:
            frame_nr = 0
    elif k == ord('a'):
        frame_nr -= 1
        if frame_nr < 0:
            frame_nr = video_length - 1
    elif k == ord('b'):
        trim_begin = frame_nr
    elif k == ord('e'):
        trim_end = frame_nr
    elif k == ord('t'):
        print("Trimming video")
        trim_video(trim_begin, trim_end, video_file, output_video_file, timestamps_file, output_timestamps_file)
    elif k == 27:
        break

    cv2.setTrackbarPos(trackbar_name, title_window, frame_nr)

