import os
import numpy as np
import argparse
import subprocess


def get_timestamps(filename):
    timestamps = []
    with open(filename) as f:
        for line in f.read().splitlines():
            timestamps.append(int(line))

    return np.array(timestamps)


parser = argparse.ArgumentParser(description='Cut a video file into chunks based on large time gaps in the timestamp information')
parser.add_argument('folder', type=str, help='`Camera` folder containing `video.mkv` and `timestamps.txt` file')
args = parser.parse_args()

part_counter = 0
timestamps = get_timestamps(os.path.join(args.folder, 'timestamps.txt'))


def cut_video(start_frame, end_frame):
    """Cut video in range [start_frame, end_frame] (including end frame) and also cut the timestamps file
    """
    global part_counter
    # cut video with ffmpeg command
    input_filename = os.path.join(args.folder, "video.mkv")
    output_filename = os.path.join(args.folder, "cut%03d"%part_counter, "video.mkv")
    output_dir = os.path.dirname(output_filename)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    cmd = ["ffmpeg",
           "-y",                                    # don't ask if it may override an existing file
           "-hide_banner", "-loglevel", "error",    # suppress output of ffmpeg
           "-i", input_filename,
           "-vf", f"select=between(n\,{start_frame}\,{end_frame}),setpts=PTS-STARTPTS",
           "-c:v", "libx264",
           output_filename]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)

    # save cut timestamps file
    output_timestamps_filename = os.path.join(output_dir, "timestamps.txt")
    with open(output_timestamps_filename, "w") as f:
        text = "\n".join([str(timestamp) for timestamp in timestamps[start_frame:end_frame + 1]])
        f.write(text)
    part_counter += 1


start_cut = 0
for frame_counter, diff in enumerate(np.diff(timestamps)):
    # cut the video when the time between two frames >= 1000 ms
    if diff >= 1000:
        # cut video here
        if (frame_counter - start_cut) >= 100:
            cut_video(start_cut, frame_counter)
        start_cut = frame_counter + 1

# cut finale part
cut_video(start_cut, frame_counter + 1)
