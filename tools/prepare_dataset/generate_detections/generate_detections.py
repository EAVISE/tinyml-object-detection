import cv2
import os
import torch
import argparse
import brambox as bb
from tqdm import tqdm
from pathlib import Path
from detectron2.config import get_cfg
from detectron2.engine.defaults import DefaultPredictor


parser = argparse.ArgumentParser(description='Generate bounding boxes for every video recursively found in the given folder')
parser.add_argument('model_config', help='.json config file')
parser.add_argument('input', help='Folder to search for videos')
parser.add_argument('--thresh', help='Confidence threshold', type=float, default=0.1)
args = parser.parse_args()

# configure model
cfg = get_cfg()
cfg.merge_from_file(args.model_config)
cfg.MODEL.RETINANET.SCORE_THRESH_TEST = args.thresh
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = args.thresh
cfg.freeze()
predictor = DefaultPredictor(cfg)


def generate_detections(video_file, detection_file):
    # open video stream
    cap = cv2.VideoCapture(video_file)

    number_of_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    detections = {"image":[],
                  "class_label":[],
                  "x_top_left":[],
                  "y_top_left":[],
                  "width":[],
                  "height":[],
                  "confidence":[],
                  }

    index = 0
    print("Generating detections on", video_file)
    for i in tqdm(range(number_of_frames)):
        ret, image = cap.read()
        if not ret:
            break

        predictions = predictor(image)
        assert "instances" in predictions

        instances = predictions["instances"].to(torch.device("cpu"))
        instances = instances[instances.pred_classes == 0]  # only visualize person class

        boxes = instances.pred_boxes.tensor
        scores = instances.scores
        for score, box in zip(scores, boxes):
            x0, y0, x1, y1 = box.numpy()
            detections["image"].append(index)
            detections["class_label"].append("person")
            detections["x_top_left"].append(x0)
            detections["y_top_left"].append(y0)
            detections["width"].append(x1 - x0)
            detections["height"].append(y1 - y0)
            detections["confidence"].append(score.item())

        index += 1

    detections = bb.util.from_dict(detections)
    print("Saving detections to", detection_file)
    bb.io.save(detections, "det_coco", detection_file, class_label_map=["person"])


for video_file in Path(args.input).rglob('cut*/video.mkv'):
    detection_file = video_file.parents[0] / 'detections.json'
    if not os.path.exists(detection_file):
        generate_detections(str(video_file), str(detection_file))
    else:
        print("Found detections file", detection_file, "-> Skipping")

