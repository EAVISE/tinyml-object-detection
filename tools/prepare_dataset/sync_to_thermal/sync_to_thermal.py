import os
import cv2
import shutil
from pathlib import Path
import numpy as np
import argparse
import subprocess
import brambox as bb


def get_timestamps(filename, offset=0):
    timestamps = []
    with open(filename) as f:
        for line in f.read().splitlines():
            timestamps.append(int(line) + offset)

    return np.array(timestamps)


def get_synced_frame_nr(timestamps, timestamp):
    # find index of timestamp in 'timestamps' that is closest to 'timestamp'
    return np.abs((timestamps - timestamp)).argmin()


def extract_camera_frame(cap, frame_nr, output_folder, output_frame_nr):
    cap.set(cv2.CAP_PROP_POS_FRAMES, int(frame_nr))
    res, image = cap.read()
    assert res == True
    output_filename = str(output_folder / ("image%08d.jpg"%output_frame_nr))
    print("Extracted", output_filename)
    cv2.imwrite(output_filename, image);


def sync_video(anno_file, cam_video_file, cam_timestamps_file, mlx_img_dir, mlx_timestamps_file, output_dir, output_cam_dir, time_offset):
    # parse timestamp and annotation files first
    cam_timestamps = get_timestamps(cam_timestamps_file, time_offset)
    mlx_timestamps = get_timestamps(mlx_timestamps_file)
    labels = ['person']
    cam_annos = bb.io.load('anno_mot', anno_file, class_label_map=labels)

    # find mlx begin and end frame numbers that correspond to the begin and end of the visual camera cut
    mlx_begin_frame_nr = get_synced_frame_nr(mlx_timestamps, cam_timestamps[0])
    mlx_end_frame_nr = get_synced_frame_nr(mlx_timestamps, cam_timestamps[-1])

    cap = cv2.VideoCapture(str(cam_video_file))

    # Find:
    #   * an mlx .tiff frame (filename)
    #   * a set of annotations for this frame
    output_mlx_frame_nr = 0
    mlx_annos = []
    for mlx_frame_nr in range(mlx_begin_frame_nr, mlx_end_frame_nr + 1):
        # copy .tiff frame and rename frame id so it starts with zero
        mlx_img_filename = mlx_img_dir / ('image%07d.tiff'%mlx_frame_nr)
        output_mlx_img_filename = output_dir / 'img' / ('image%07d.tiff'%output_mlx_frame_nr)
        if not output_mlx_img_filename.parents[0].is_dir():
            os.makedirs(output_mlx_img_filename.parents[0])
        print("Copy", mlx_img_filename, "->", output_mlx_img_filename)
        shutil.copyfile(str(mlx_img_filename), str(output_mlx_img_filename))

        mlx_timestamp = mlx_timestamps[mlx_frame_nr]
        cam_frame_nr = get_synced_frame_nr(cam_timestamps, mlx_timestamp)

        # get .jpeg frame from camera video
        if output_cam_dir is not None:
            if not output_cam_dir.is_dir():
                os.makedirs(output_cam_dir)
            extract_camera_frame(cap, cam_frame_nr, output_cam_dir, output_mlx_frame_nr)

        # get all annotations from this cam frame nr and store them with the output_mlx_frame_nr image id
        mlx_anno_current_frame = bb.util.select_images(cam_annos, ["%08d"%cam_frame_nr])
        mlx_anno_current_frame.image.cat.categories = ["%08d"%output_mlx_frame_nr]
        mlx_annos.append(mlx_anno_current_frame)
        output_mlx_frame_nr += 1

    mlx_annos = bb.util.concat(mlx_annos)
    output_gt_file = Path(output_dir) / 'gt' / 'gt.txt'
    if not output_gt_file.parents[0].is_dir():
        os.makedirs(output_gt_file.parents[0])

    # rescale annos to fit for 32x24 images instead of 640x480 images by dividing all x,y,w,h values by 20
    mlx_annos['x_top_left'] = mlx_annos['x_top_left'].div(20)
    mlx_annos['y_top_left'] = mlx_annos['y_top_left'].div(20)
    mlx_annos['width']      = mlx_annos['width'].div(20)
    mlx_annos['height']     = mlx_annos['height'].div(20)

    # save annos
    print("Writing", output_gt_file)
    bb.io.save(mlx_annos, 'anno_mot', str(output_gt_file), class_label_map=labels)


parser = argparse.ArgumentParser(description='Time sync video and annotations with thermal video')
parser.add_argument('input', nargs='+', type=str, help='Folder containing `Camera` and `MLX90640` folders')
parser.add_argument('--offset', type=int, help='Offset to time-shift the camera frames forward in milli seconds. (Positive number will delay the camera frames)', default=0)
parser.add_argument('--visible', help='Also extract visible frames as .jpg files', action='store_true')
args = parser.parse_args()


### Build the following output structure
#
#   folder/MLX90640_processed/cut000/imgs/image0000000.tiff
#   folder/MLX90640_processed/cut000/imgs/image0000001.tiff
#   ...
#   folder/MLX90640_processed/cut000/gt/timestamps.txt
#   folder/MLX90640_processed/cut000/gt/gt.txt
#
#   folder/MLX90640_processed/cut001/imgs/image0000000.tiff
#   folder/MLX90640_processed/cut001/imgs/image0000001.tiff
#   ...

for input_folder in args.input:
    for video_file in Path(input_folder).rglob('cut*/video.mkv'):
        anno_file = video_file.parents[0] / 'completed_tracks.txt'
        if not os.path.exists(anno_file):
            print("Completed tracks file", anno_file, "not found, skipping this cut")
            continue

        cam_timestamps_file = video_file.parents[0] / 'timestamps.txt'
        mlx_img_dir = video_file.parents[2] / 'MLX90640'
        mlx_timestamps_file = mlx_img_dir / 'timestamps.txt'
        output_dir = video_file.parents[2] / 'MLX90640_processed' / video_file.parts[-2]
        output_cam_dir = None
        if args.visible:
            output_cam_dir = video_file.parents[2] / 'Camera_processed' / video_file.parts[-2]

        sync_video(anno_file, video_file, cam_timestamps_file, mlx_img_dir, mlx_timestamps_file, output_dir, output_cam_dir, args.offset)
