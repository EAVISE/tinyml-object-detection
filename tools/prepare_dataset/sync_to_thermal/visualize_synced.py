import cv2
import brambox as bb
import numpy as np
import os
import sys
import argparse


def apply_agc_linear(img, min_temp=17, max_temp=30):
    """Linear AGC between fixed temperature range
    img:    uint16 or int16 image
    """
    min_temp = int(min_temp * 100)
    max_temp = int(max_temp * 100)

    if img.dtype == np.uint16:
        img = img.astype(np.int16)

    # clip temperatures
    img = np.clip(img, a_min=min_temp, a_max=max_temp).astype(np.int32)

    # rescale to value between 0 and 255
    img = (((img - min_temp) * 255) / (max_temp - min_temp)).astype(np.uint8)

    return img


def preprocess_thermal(image):
    image = apply_agc_linear(image)
    image = cv2.applyColorMap(image, cv2.COLORMAP_JET)
    image = cv2.resize(image, (640, 480))
    return image


parser = argparse.ArgumentParser(description='Synchronize recorded color and thermal images time-wise')
parser.add_argument('directory', help='Directory to sync. Expecting dir to contain subfolders Camera_processed and MLX90640_processed')
parser.add_argument('cut', help='Cut to visualize')
parser.add_argument('--blend', help='Blend images rather than displaying them next to eachother', action='store_true')
parser.add_argument('--rot', help='Rotate images 180 degrees', action='store_true')
args = parser.parse_args()


def read_image(cap, img_id):
    print(img_id)
    cap.set(cv2.CAP_PROP_POS_FRAMES, int(img_id))
    res, image = cap.read()
    assert res == True
    if image.dtype == np.uint16:
        image = preprocess_thermal(image)
    return image


# Camera streams
mlx_d = 'MLX90640_processed'
cam_d = 'Camera_processed'
cam_video = os.path.join(args.directory, cam_d, args.cut, 'image%08d.jpg')
mlx_video = os.path.join(args.directory, mlx_d, args.cut, 'img', 'image%07d.tiff')
anno_file = os.path.join(args.directory, mlx_d, args.cut, 'gt', 'gt.txt')

print("Opening mlx video", mlx_video)
cap_mlx = cv2.VideoCapture(mlx_video, cv2.CAP_IMAGES)

labels = ['person']
boxes = bb.io.load('anno_mot', anno_file, class_label_map=labels, seq_length=int(cap_mlx.get(cv2.CAP_PROP_FRAME_COUNT)))

# Resize boxes
boxes['x_top_left'] = boxes['x_top_left'].mul(20)
boxes['y_top_left'] = boxes['y_top_left'].mul(20)
boxes['width']      = boxes['width'].mul(20)
boxes['height']     = boxes['height'].mul(20)

# Visualize
mlx_drawer = bb.util.BoxDrawer(
    images=lambda x: read_image(cap_mlx, x),
    boxes=boxes,
    size=2,
    color=(255, 0, 0),
    method=bb.util.DrawMethod.CV
)

cam_drawer = None
if os.path.exists(os.path.dirname(cam_video)):
    print("Opening cam video", cam_video)
    cap_cam = cv2.VideoCapture(cam_video, cv2.CAP_IMAGES)

    cam_drawer = bb.util.BoxDrawer(
        images=lambda x: read_image(cap_cam, x),
        boxes=boxes,
        size=2,
        color=(255, 0, 0),
        method=bb.util.DrawMethod.CV
    )


i=0
while True:
    mlx_img = mlx_drawer[i]

    canvas = mlx_img
    if cam_drawer is not None:
        cam_img = cam_drawer[i]
        if args.blend:
            canvas = cv2.addWeighted(cam_img, 0.5, mlx_img, 0.5, 0.0)
        else:
            canvas = np.concatenate([cam_img, mlx_img], axis=1)
    cv2.imshow("out", canvas)
    print("image", i, "/", len(mlx_drawer))
    k = cv2.waitKey(0)
    if k == ord('d'):
        i += 1
        if i == len(mlx_drawer):
            i = 0
    elif k == ord('a'):
        i -= 1
        if i < 0:
            i = len(mlx_drawer) - 1
    elif k == 27:
        break
