import os
import cv2
import argparse
import numpy as np
import brambox as bb
from tqdm import tqdm
from pathlib import Path
from motpy import Detection, MultiObjectTracker
from motpy.testing_viz import draw_detection, draw_track


def get_detections(boxes):
    """Convert brambox bounding boxes to list of motpy bounding boxes
    """
    dets = []
    for index, box in boxes.iterrows():
        # box needs to be a list of [xmin, ymin, xmax, ymax]
        det = Detection(box=[int(box['x_top_left']),
                             int(box['y_top_left']),
                             int(box['x_top_left'] + box['width']),
                             int(box['y_top_left'] + box['height'])],
                        score=box['confidence'])
        dets.append(det)

    return dets


def store_track_results(track_results, output_filename, fmt='anno_mot'):
    """Convert motpy tracks to brambox bounding boxes and save the to the given format
    """
    result = {'image':[], 'id':[], 'class_label':[], 'x_top_left':[], 'y_top_left':[], 'width':[], 'height':[]}
    # iterate over all images
    for i, tracks in enumerate(track_results):
        # iterate over each track in an image
        for track in tracks:
            result['image'] += [f'{i:08}']
            result['id'] += [track.id]
            result['class_label'] += ['person']
            result['x_top_left'] += [max(0, track.box[0])]
            result['y_top_left'] += [max(0, track.box[1])]
            result['width'] += [max(0, track.box[2] - max(0, track.box[0]))]
            result['height'] += [max(0, track.box[3] - max(0, track.box[1]))]

    df = bb.util.from_dict(result)
    bb.io.save(df, 'anno_mot', output_filename, class_label_map=['person'])


def generate_tracks(video_file, detections_file, output_file, thresh):
    cap = cv2.VideoCapture(video_file)
    video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # load bbox data
    labels = ["person"]
    boxes = bb.io.load('det_coco', detections_file, class_label_map=labels)

    # create tracker
    tracker = MultiObjectTracker(dt=0.1,
                                 tracker_kwargs={"max_staleness": 20},
                                 active_tracks_kwargs={'min_steps_alive': 10, 'max_staleness_to_positive_ratio': 20},
                                 matching_fn_kwargs={'min_iou': 0.2})

    track_results = []
    for i in tqdm(range(video_length)):
        ret, frame = cap.read()
        if not ret:
            break

        detections = get_detections(boxes[(boxes.image == i) & (boxes.confidence >= thresh)])
        tracker.step(detections=detections, frame=frame)
        tracks = tracker.active_tracks()

        #for det in detections:
        #    draw_detection(frame, det)

        for track in tracks:
            draw_track(frame, track)

        track_results.append(tracks)

        cv2.imshow('preview', frame)
        k = cv2.waitKey(1)
        if k == ord('q') or k == 27:
            print("Quitting without storing tracks")
            return

    store_track_results(track_results, output_file)


parser = argparse.ArgumentParser(description='Generate instance tracks based on pre-generated object detections in MS COCO format.')
parser.add_argument('input', help='Folder to search for videos')
parser.add_argument('--thresh', '-t', type=float, help='Detection threshold', default=0.5)
args = parser.parse_args()


for video_file in Path(args.input).rglob('cut*/video.mkv'):
    detections_file = video_file.parents[0] / 'detections.json'
    tracks_file = video_file.parents[0] / 'tracks.txt'
    if not os.path.exists(tracks_file):
        print("Generating tracks file", tracks_file)
        generate_tracks(str(video_file), str(detections_file), str(tracks_file), args.thresh)
    else:
        print("Found tracks file", tracks_file, "-> Skipping")
