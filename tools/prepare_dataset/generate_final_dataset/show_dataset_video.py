import brambox as bb
import cv2
import os
import numpy as np
import argparse


def apply_agc_linear(img, min_temp=17, max_temp=30):
    """Linear AGC between fixed temperature range
    img:    uint16 or int16 image
    """
    min_temp = int(min_temp * 100)
    max_temp = int(max_temp * 100)

    if img.dtype == np.uint16:
        img = img.astype(np.int16)

    # clip temperatures
    img = np.clip(img, a_min=min_temp, a_max=max_temp).astype(np.int32)

    # rescale to value between 0 and 255
    img = (((img - min_temp) * 255) / (max_temp - min_temp)).astype(np.uint8)

    return img


def preprocess_thermal(image, target_image_size=(640, 480)):
    image = apply_agc_linear(image)
    image = cv2.applyColorMap(image, cv2.COLORMAP_JET)
    image = cv2.resize(image, target_image_size)
    return image


parser = argparse.ArgumentParser(description='Visualize bouding boxes')
parser.add_argument('tracks', help='.txt MOT format track file')
parser.add_argument('video', help='video file or image sequence')
args = parser.parse_args()

# check if video is a video file or folder with (thermal) images
if args.video.endswith('.tiff'):
    cap = cv2.VideoCapture(args.video, cv2.CAP_IMAGES)
else:
    cap = cv2.VideoCapture(args.video)

def read_image(cap, img_id):
    print(img_id)
    cap.set(cv2.CAP_PROP_POS_FRAMES, int(img_id))
    res, image = cap.read()
    assert res == True
    if image.dtype == np.uint16:
        image = preprocess_thermal(image)
    return image

labels = ['person']
boxes = bb.io.load('anno_mot', args.tracks, class_label_map=labels)

# rescale boxes
boxes['x_top_left'] = boxes['x_top_left'].mul(20)
boxes['y_top_left'] = boxes['y_top_left'].mul(20)
boxes['width']      = boxes['width'].mul(20)
boxes['height']     = boxes['height'].mul(20)

# Visualize
drawer = bb.util.BoxDrawer(
    images=lambda x: read_image(cap, x),
    boxes=boxes,
    size=2,
    color=(255, 0, 0),
    method=bb.util.DrawMethod.CV
)

i=0
while True:
    img = drawer[i]
    cv2.imshow("image", img)
    print("image", i, "/", len(drawer))
    k = cv2.waitKey(0)
    if k == ord('d'):
        i += 1
        if i == len(drawer):
            i = 0
    elif k == ord('a'):
        i -= 1
        if i < 0:
            i = len(drawer) - 1
    elif k == 27:
        break
