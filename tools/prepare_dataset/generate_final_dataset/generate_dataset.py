import os
import cv2
import shutil
import argparse
import brambox as bb
from pathlib import Path


def parse_meta(filename):
    # location id, recorder id, angle, folder
    result = []
    with open(str(filename)) as f:
        for line in f.readlines():
            if line.startswith('#'):
                continue
            elems = line.split(',')
            result.append((int(elems[0]), int(elems[1]), float(elems[2]), elems[3].strip()))

    # sort list by location id
    result = sorted(result, key=lambda x: x[0])
    return result


parser = argparse.ArgumentParser(description='Generate the final dataset given the prepared data. '
                                             'The script reorganizes the folders')
parser.add_argument('input', type=str, help='Input folder')
parser.add_argument('output', type=str, help='Output folder. This folder should be empty')
args = parser.parse_args()

meta_file = Path(args.input) / 'meta.txt'
view_id = 0
prev_location_id = -1

# iterate over each entry in the meta.txt file (each entry is a different view)
for location_id, recorder_id, angle, folder in parse_meta(meta_file):
    print(location_id, recorder_id, angle, folder)

    # reset view index to zero if location changes, since we will save a different location in a different folder
    if location_id != prev_location_id:
        view_id = 0
    prev_location_id = location_id

    # iterate over each video cut in this view and copy anno file and video frames to the target folder
    for clip_id, anno_file in enumerate((Path(args.input) / folder).rglob('cut*/gt/gt.txt')):
        video_dir = anno_file.parents[1] / 'img'

        dest_dir = Path(args.output) / f'location{location_id:02}' / f'view{view_id:02}' / f'clip{clip_id:02}'

        # copy all video frames
        print("Copy", video_dir, 'to', dest_dir)
        shutil.copytree(str(video_dir), str(dest_dir))

        # copy annotation file
        print("Copy", anno_file, 'to', dest_dir)
        shutil.copy(str(anno_file), str(dest_dir))

        # write view specific meta file
        meta_filename = dest_dir.parents[0] / 'meta.txt'
        print('Writing metadata file', meta_filename)
        with open(meta_filename, 'w') as f:
            lines = [f'recorder_id = {recorder_id}',
                     f'recorder_angle = {angle}']
            f.write("\n".join(lines))

    view_id += 1
