import brambox as bb
from pathlib import Path
import numpy as np
import pandas as pd
import argparse
import shutil
import cv2
import os

import utils


def parse_clip_list(filename):
    with open(filename) as f:
        folders = f.read().splitlines()
    return folders


def parse_meta(filename):
    with open(filename) as f:
        lines = f.read().splitlines()
    entries = {}
    for line in lines:
        elems = line.split('=')
        entries[elems[0].strip()] = elems[1].strip()
    return entries


def get_video_frame_count(path):
    """Return the number of frames in a video
    """
    cap = cv2.VideoCapture(str(path), cv2.CAP_IMAGES)
    return int(cap.get(cv2.CAP_PROP_FRAME_COUNT))


def calculate_motion(anno, stride=10):
    """Anno is a dataframe of a single clip
    """
    # only select annos that have a valid tracking id
    anno_clip = anno[~anno.id.isnull()]

    if anno_clip.empty:
        anno['motion'] = 0.0
        return anno

    # calculate centroids
    anno_clip['x_center'] = anno_clip.x_top_left + anno_clip.width / 2.
    anno_clip['y_center'] = anno_clip.y_top_left + anno_clip.height / 2.

    def custom_diff(df):
        # repeat first row by an additional stride times
        df_ = df.iloc[np.arange(len(df)).repeat(([stride+1] + (len(df)-1) * [1]))]

        # calculate diff between between current and current - `stride` frame for center x and y
        diff = df_[['x_center', 'y_center']].diff(periods=stride)

        # include id column
        diff['id'] = df.name

        # remove first `stride` rows again
        diff = diff.iloc[stride: , :]
        return diff

    dist = anno_clip.groupby('id').apply(custom_diff).dropna()
    dist['dist'] = np.linalg.norm(dist[['x_center', 'y_center']].values, axis=1)
    anno['motion'] = dist['dist']
    anno['motion'] = anno['motion'].fillna(0.0)

    return anno


parser = argparse.ArgumentParser(description='Generate a single image dataset from the video dataset')
parser.add_argument('input', type=str, help='Input folder from the final video dataset')
parser.add_argument('list', type=str, help='File with clip list to choose from')
parser.add_argument('output', type=str, help='Output folder to store the images')
args = parser.parse_args()

bb.io.register_parser('ml_detection', utils.MlDetectionParser)

frame_counter = 0
all_annos = []
for clip_folder in parse_clip_list(args.list):
    anno_path = Path(args.input).parent / clip_folder / 'gt.txt'
    src_path = anno_path.parent
    dst_path = Path(args.output)
    frame_count = get_video_frame_count(src_path / 'image%07d.tiff')
    print("Video frame count = ", frame_count)
    df = bb.io.load('anno_mot', anno_path, class_label_map=['person'], seq_length=frame_count)
    new_cat_names = []

    # copy images and rename df categories
    for i, image_id in enumerate(df.image.cat.categories):
        src_file = src_path / f'image{int(image_id):07}.tiff'
        dst_name = "_".join(src_file.parts[-4:])

        # add angle of recording to image name
        meta_file = src_path.parent / 'meta.txt'
        entries = parse_meta(meta_file)
        angle = int(float(entries['recorder_angle']))
        dst_name = dst_name.replace('_clip', f'_angle{angle}_clip')

        dst_file = dst_path / 'images' / dst_name
        if not dst_file.parent.is_dir():
            os.makedirs(dst_file.parent)
        print(i, src_file, "->", dst_file)
        shutil.copy(src_file, dst_file)
        new_cat_names += [dst_name.split('.')[0]]
        frame_counter += 1
    df['image'] = df['image'].cat.rename_categories(new_cat_names)
    df = calculate_motion(df)
    all_annos.append(df)

all_annos = bb.util.concat(all_annos)
labels_path = dst_path / 'labels'
if not labels_path.is_dir():
    os.makedirs(labels_path)
bb.io.save(all_annos, 'anno_ml_detection', str(labels_path))
