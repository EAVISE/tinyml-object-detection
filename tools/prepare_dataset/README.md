# Dataset preparation tools

Contains a set of scripts to select candidate video material, to generate tracks, to edit tracks and to generate an output dataset for training.

## Install

Prerequisites

* Detectron 2
* custom motpy
* CVAT tool
* other python requisites from this repo

## Prepare videos

Cut videos at points the recorder stopped recording due to a lack of motion (based on timestamp information):

```
python prepare_videos/cut_video.py path/to/setupXX/setXXX/partXXX/Camera/
```

This will create folders like `cut000`, `cut001`, etc... in the `Camera` directory.

Then we will manually trim each video to a length of maximum 1000 frames, by selecting the most interesting part of each `cutXXX` folder.
This can be done with the following command:

```
python prepare_videos/trim_video_length.py path/to/setupXX/setXXX/partXXX/Camera/cutXXX
```

In the GUI:

1. move the trackbar to the starting position and press `b` to mark the begin
2. move the trackbar to the end position and press `e` to mark the end
3. press `t` to initiate the trim

Make sure to limit the trimmed video to 1000 frames maximum (longer videos are a pain to edit).

If single `cutXXX` folder contains multiple interesting parts, manually copy-paste the `cutXXX` folder under a new name and make a new trim of a different part.

If you are happy with the trimmed results, override the original video and timestamp file in each `cutXXX` folder with the trimmed ones:

```
mv timestamps_trimmed.txt timestamps.txt
mv video_trimmed.mkv video.mkv
```

## Generate tracks

First generate detection (requires a GPU):

```
python generate_detections/generate_detections.py generate_detections/mask_rcnn_X_101_32x8d_FPN_3x.yaml path/to/setupXX/setXXX
```

This will recursively find all `cutXXX` folders and generate detections for each video.

Afterwards, generate the tracks:

```
python generate_tracks/generate_tracks.py path/to/setupXX/setXXX --thres 0.3
```

## Edit the tracks in CVAT

Start CVAT if not already running:

```
cd path/to/cvat/repo
docker-compose up -d
cd -
```

Create an annotation task in CVAT of a trimmed video and upload the generated tracks data simultaniously:

```
python edit_tracks/create_cvat_annotation_tasks.py path/to/setupXX/setXXX/partXXX/Camera/cutXXX
```

The script accepts multiple input folders, so you can use wildcards to upload multiple tasks at once, for example:

```
python edit_tracks/create_cvat_annotation_tasks.py path/to/setupXX/setXXX/part*/Camera/cut*
```

Open chrome (only officially supported browser) and go to `http://localhost:8080/` login if needed.
Go to the 'Tasks' tab and start editting a video. If done editting, click on menu -> 'Finish the job'. Repeat for other videos if needed.

Now export the editted tracks data of all finished jobs:

```
python export_completed_cvat_annotation_tasks.py path/to/
```

The export script will place the editted track data in the original folders next to the generated tracks.txt files and name them `completed_tracks.txt`.
If a `completed_tracks.txt` file already exists in the target folder, the export script will skip the export, so in order to update one in case additional
editting is done after export, the `completed_tracks.txt` files must be manually removed.

## Synchronizing colour video to thermal video

Time synchronize the annotations created on the colour video to the thermal video based on the timestamp information:

```
python sync_to_thermal.py path/to/setupXX/setXXX --offset 0
```

With the offset parameter, an offset in millisecond can be given in case the colour video lags behind the thermal video (provide a negative value) or the
other way around (provide a positive value). This requires some experimentation.

The script accepts multiple input folders, so you can use wildcards to process multiple top level folders:

```
python sync_to_thermal.py path/to/setupXX/set* --offset 0
```

This will create a `MLX90640_processed` folder next to each `Camera` and `MLX90640` folder.

You can also add the '--visual' flag to also copy the synced visual data for inspection purpose.

To visualize/inspect the synchronized data, you can:

```
python sync_to_thermal/visualize_synced.py path/to/setupXX/setXXX/partXXX cutXXX
```

## Generating the final training dataset

First complete the `meta.txt` file with new metadata for each recording session. This meta data is used in the final dataset to give an anonymous id
to each recording location, to know the camera ange (45 or 90 degrees), to have an id on the used recorder and ofcourse what data folders to include in
the final dataset.

```
python generate_final_dataset/generate_dataset.py path/to/input/data path/to/final/dataset
```

The script will cut all videos in short clips of 50 frames each and copies them to a new directory with the following directory tree:

```
data
    locationXX
        viewXX
            meta.txt
            clipXXX
                gt.txt
                imageXXX.tiff
```

These clips can be used to train a video object detector.

In order to train a single image object detector (usually done as initial training step without temporal network components),
another script can be used to extract a single frame dataset from the video dataset generated above:

```
python generate_single_frames_dataset.py path/to/final/dataset path/to/single/image/dataset
```

The script will only include images that contain at least one bounding box
