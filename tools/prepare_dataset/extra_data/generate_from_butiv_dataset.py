import brambox as bb
from pathlib import Path
import argparse
import shutil
import numpy as np
import cv2
import os

import utils


def apply_agc(img):
    # calc histogram
    bincount = 2**14
    hist = cv2.calcHist([img], [0], None, [bincount], [0,bincount])

    min_value = np.min(img)
    max_value = np.max(img)

    # TODO: not sure how to implement max gain parameter

    hist = hist[min_value:max_value+1]

    # apply plateau threshold
    plateau = 0.07
    try:
        plateau = np.max(hist) * plateau
    except ValueError:
        print("Found frame with only 1 grayscale value, skipping to next video")
        return None

    hist[hist > plateau] = plateau

    # calculate pdf
    pdf = hist / np.sum(hist)

    # get cumulative density function (CDF) from Plateau HEQ
    cdf_pheq = np.cumsum(pdf)
    #print(cdf_pheq)
    # calc linear CDF
    cdf_lin = np.linspace(0, 1, len(pdf))

    # apply linear percentage
    linear_percent = 0.2
    cdf = linear_percent * cdf_lin + (1 - linear_percent) * cdf_pheq

    # apply ACE
    ace = 0.8
    cdf_ace = np.power(cdf, 1/ace)

    # remap pixels based on CDF
    out_img = 255 * cdf_ace[img - min_value]

    return out_img.astype(np.uint8)


def fit_annos(annos, img_h, img_w, bbox_min_w=2, bbox_min_h=2):
    """Make sure all annotations are clipped within img_h and img_w bounds.
    Remove annotations with negative or zero area
    """
    # convert to x1, x2, y1, y2 coordinates
    annos['x_top_right'] = annos.x_top_left + annos.width
    annos['y_bottom'] = annos.y_top_left + annos.height

    # clamp coordinates to image frame
    annos.x_top_left = annos.x_top_left.clip(0, img_w)
    annos.y_top_left = annos.y_top_left.clip(0, img_h)
    annos.x_top_right = annos.x_top_right.clip(0, img_w)
    annos.y_bottom = annos.y_bottom.clip(0, img_h)

    # convert back to x, y, w, h coordinates
    annos.width = annos.x_top_right - annos.x_top_left
    annos.height = annos.y_bottom - annos.y_top_left

    # select only the bounding boxes that have width/height > thresh
    annos = annos[(annos.width >= bbox_min_w) & (annos.height >= bbox_min_h)]

    return annos


def transform_annos(annos, crop_roi):
    """Crop annotations in ROI rectangle
    """
    # crop annos
    x, y, w, h = crop_roi
    annos.x_top_left -= x
    annos.y_top_left -= y
    annos = fit_annos(annos, h, w)

    # downsample annotations
    scale_x = w / 32
    scale_y = h / 24
    annos.x_top_left /= scale_x
    annos.y_top_left /= scale_y
    annos.width /= scale_x
    annos.height /= scale_y

    # rename image ids to an incremental number
    annos['image'] = annos['image'].cat.rename_categories([f'{i:08}' for i in range(len(annos['image'].cat.categories))])

    return annos


def transform_image(image, crop_roi):
    """Transform an LWIR image from the BU-TIV dataset to an MLX90640-like image
    and crop and rescale annotations accordingly
    """
    # crop image
    x, y, w, h = crop_roi
    x1 = x
    x2 = x + w
    y1 = y
    y2 = y + h
    image = image[y1:y2, x1:x2]

    # downsample image
    image = cv2.resize(image, (32, 24))

    # blur image
    image = cv2.blur(image, (3, 3))

    # add gaussian noise to image
    sigma = 0.03
    image_max = image.max()
    image_min = image.min()
    image = image.astype(np.int16)
    noise = np.random.normal(loc=0.0, scale=sigma*(image_max - image_min),
                             size=image.shape).astype(image.dtype.type)
    image = np.clip(image + noise, image_min, image_max)

    # offset absolute temperatures to more or less match the same temperature range as other data
    image -= 2000;    # experimental offset
    print(image_max, image_min)

    return image.astype(np.uint16)


def select_roi(image_file, crop_shape):
    """Return ROI
    """
    crop_roi = [0, 0, *crop_shape]
    image = cv2.imread(image_file, -1)
    image = apply_agc(image)
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

    image_shape = (image.shape[1], image.shape[0])

    def draw_roi(event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            x_top_left = int(x - crop_shape[0] / 2)
            y_top_left = int(y - crop_shape[1] / 2)
            x_top_left = min(max(x_top_left, 0), image_shape[0] - crop_shape[0])
            y_top_left = min(max(y_top_left, 0), image_shape[1] - crop_shape[1])
            crop_roi[0], crop_roi[1] = x_top_left, y_top_left

    cv2.namedWindow('select ROI')
    cv2.setMouseCallback('select ROI', draw_roi)
    while True:
        preview_image = image.copy()
        cv2.rectangle(preview_image, crop_roi, (0, 0, 255), 2)
        cv2.imshow('select ROI', preview_image)
        k = cv2.waitKey(1)
        # press escape to continue
        if k == 27:
            break

    cv2.destroyWindow('select ROI')

    return crop_roi


bb.io.register_parser('butiv', utils.BUTIVParser)


parser = argparse.ArgumentParser(description='Generate a video with MOT annotation format from a BU-TIV dataset sequence, given start and stop frames')
parser.add_argument('images', type=str, help='Input folder with images from the initial BUTIV dataset')
parser.add_argument('annos', type=str, help='Input .xml annotations file from the initial BUTIV dataset')
parser.add_argument('output', type=str, help='Output folder to store the result')
parser.add_argument('--stride', type=int, help='Frame stride to enable frame skipping. Must be 1 or greater', default=1)
args = parser.parse_args()

image_files = [str(p) for p in sorted(Path(args.images).glob('*.png'))]
image_files = image_files[::args.stride]

crop_shape = (512, 384)
crop_roi = select_roi(image_files[0], crop_shape)

# preprocess all images first
output_image_file_template = Path(args.output) / 'img' / ('image%07d.tiff')
if not output_image_file_template.parent.is_dir():
    os.makedirs(output_image_file_template.parent)
for i, filename in enumerate(image_files):
    raw_image = cv2.imread(filename, -1)
    image = transform_image(raw_image, crop_roi)
    output_filename = str(output_image_file_template)%i
    print(filename, '->', output_filename)
    cv2.imwrite(output_filename, image);

# convert annotations
annos = bb.io.load('anno_butiv', args.annos)
annos = bb.util.select_images(annos, annos.image.cat.categories[::args.stride])
annos = transform_annos(annos, crop_roi)

# save annotations in MOT format
output_file = Path(args.output) / 'gt' / 'gt.txt'
if not output_file.parent.is_dir():
    os.makedirs(output_file.parent)
bb.io.save(annos, 'anno_mot', str(output_file), class_label_map='person')

#def read_image(image_id):
#    filename = str(output_image_file_template)%(int(image_id[6:]))
#    print(filename)
#    image = cv2.imread(filename, -1)
#    image = apply_agc(image)
#    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
#    return image
#
#
#drawer = bb.util.BoxDrawer(
#    images=read_image,
#    boxes=annos,
#    size=1,
#    color=(255, 0, 0),
#    method=bb.util.DrawMethod.CV
#)
#
#i = 0
#while True:
#    img = drawer[i]
#
#    img = cv2.resize(img, (320, 240))
#    cv2.imshow("out", img)
#    k = cv2.waitKey(0)
#    if k == ord('d'):
#        i += 1
#        if i == len(drawer):
#            i = 0
#    elif k == ord('a'):
#        i -= 1
#        if i < 0:
#            i = len(drawer) - 1
#    elif k == 27:
#        break

