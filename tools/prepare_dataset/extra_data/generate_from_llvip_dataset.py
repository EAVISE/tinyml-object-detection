import brambox as bb
from pathlib import Path
import argparse
import shutil
import numpy as np
import cv2
import os

import utils


def apply_agc_linear(img, min_temp=12, max_temp=37):
    """Linear AGC between fixed temperature range
    img:    uint16 or int16 image
    """
    min_temp = int(min_temp * 100)
    max_temp = int(max_temp * 100)

    if img.dtype == np.uint16:
        img = img.astype(np.int16)

    # clip temperatures
    img = np.clip(img, a_min=min_temp, a_max=max_temp).astype(np.int32)

    # rescale to value between 0 and 255
    img = (((img - min_temp) * 255) / (max_temp - min_temp)).astype(np.uint8)

    return img


def fit_annos(annos, img_h, img_w, bbox_min_w=2, bbox_min_h=2):
    """Make sure all annotations are clipped within img_h and img_w bounds.
    Remove annotations with negative or zero area
    """
    # convert to x1, x2, y1, y2 coordinates
    annos['x_top_right'] = annos.x_top_left + annos.width
    annos['y_bottom'] = annos.y_top_left + annos.height

    # clamp coordinates to image frame
    annos.x_top_left = annos.x_top_left.clip(0, img_w)
    annos.y_top_left = annos.y_top_left.clip(0, img_h)
    annos.x_top_right = annos.x_top_right.clip(0, img_w)
    annos.y_bottom = annos.y_bottom.clip(0, img_h)

    # convert back to x, y, w, h coordinates
    annos.width = annos.x_top_right - annos.x_top_left
    annos.height = annos.y_bottom - annos.y_top_left

    # select only the bounding boxes that have width/height > thresh
    annos = annos[(annos.width >= bbox_min_w) & (annos.height >= bbox_min_h)]

    return annos


def transform_annos(annos, crop_roi):
    """Crop annotations in ROI rectangle
    """
    # crop annos
    x, y, w, h = crop_roi
    annos.x_top_left -= x
    annos.y_top_left -= y
    annos = fit_annos(annos, h, w)

    # downsample annotations
    scale_x = w / 32
    scale_y = h / 24
    annos.x_top_left /= scale_x
    annos.y_top_left /= scale_y
    annos.width /= scale_x
    annos.height /= scale_y

    # rename image ids to an iincremental number
    annos['image'] = annos['image'].cat.rename_categories([f'{i:08}' for i in range(len(annos['image'].cat.categories))])

    return annos


def transform_image(image, crop_roi):
    """Transform an LWIR image from the dataset to an MLX90640-like image
    and crop and rescale annotations accordingly
    """
    # crop image
    x, y, w, h = crop_roi
    x1 = x
    x2 = x + w
    y1 = y
    y2 = y + h
    image = image[y1:y2, x1:x2]

    # downsample image
    image = cv2.resize(image, (32, 24))

    # convert to signed 16-bit image and rescale values to simulate real temperatures
    # this is however an coarse approximation due to the non-linear AGC from FLIR
    # 0 - 255 -> image_min - image_max
    image_max = 3000
    image_min = 1700
    image = image.astype(np.int16)
    image = image * ((image_max - image_min) / 255.) + image_min

    # blur image
    image = cv2.blur(image, (3, 3))

    # add gaussian noise to image
    sigma = 0.03
    noise = np.random.normal(loc=0.0, scale=sigma*(image_max - image_min),
                             size=image.shape).astype(image.dtype.type)
    image = np.clip(image + noise, image_min, image_max)

    return image.astype(np.uint16)


def select_roi(image_file, crop_shape):
    """Return ROI
    """
    crop_roi = [0, 0, *crop_shape]
    image = cv2.imread(image_file)

    image_shape = (image.shape[1], image.shape[0])

    def draw_roi(event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            x_top_left = int(x - crop_shape[0] / 2)
            y_top_left = int(y - crop_shape[1] / 2)
            x_top_left = min(max(x_top_left, 0), image_shape[0] - crop_shape[0])
            y_top_left = min(max(y_top_left, 0), image_shape[1] - crop_shape[1])
            crop_roi[0], crop_roi[1] = x_top_left, y_top_left

    cv2.namedWindow('select ROI')
    cv2.setMouseCallback('select ROI', draw_roi)
    while True:
        preview_image = image.copy()
        cv2.rectangle(preview_image, crop_roi, (0, 0, 255), 2)
        cv2.imshow('select ROI', preview_image)
        k = cv2.waitKey(1)
        # press escape to continue
        if k == 27:
            break

    cv2.destroyWindow('select ROI')

    return crop_roi


bb.io.register_parser('butiv', utils.BUTIVParser)


parser = argparse.ArgumentParser(description='Generate a MLX9064-like video with MOT annotation format from LLVIP dataset')
parser.add_argument('images', type=str, help='Input folder with images from the initial BUTIV dataset')
parser.add_argument('annos', type=str, help='Input folder with pascal voc annotation files from LLVIP')
parser.add_argument('seqnr', type=int, help='Video sequence number to process. Each video sequence nr represents a different camera view')
parser.add_argument('output', type=str, help='Output folder to store the result')
args = parser.parse_args()


# select all annotations and images from a single video sequence
anno_files = [str(p) for p in sorted(Path(args.annos).glob('%02d*.xml'%args.seqnr))]
image_files = [str(p) for p in sorted(Path(args.images).glob('%02d*.jpg'%args.seqnr))]

crop_shape = (640, 480)
#crop_shape = (480, 360)
crop_roi = select_roi(image_files[0], crop_shape)

# process annotations
annos = bb.io.load('anno_pascalvoc', anno_files)
annos = transform_annos(annos, crop_roi)

output_file = Path(args.output) / 'gt' / 'gt.txt'
if not output_file.parent.is_dir():
    os.makedirs(output_file.parent)
bb.io.save(annos, 'anno_mot', str(output_file), class_label_map='person')

# preprocess images
output_image_file_template = Path(args.output) / 'img' / ('image%07d.tiff')
if not output_image_file_template.parent.is_dir():
    os.makedirs(output_image_file_template.parent)
for frame_nr, filename in enumerate(image_files):
    raw_image = cv2.imread(str(filename), 0)
    image = transform_image(raw_image, crop_roi)
    output_filename = str(output_image_file_template)%frame_nr
    print(filename, '->', output_filename)
    cv2.imwrite(output_filename, image);


#def read_image(image_id):
#    filename = str(output_image_file_template)%(int(image_id))
#    print(filename)
#    image = cv2.imread(filename, -1)
#    image = apply_agc_linear(image)
#    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
#    return image
#
#
#drawer = bb.util.BoxDrawer(
#    images=read_image,
#    boxes=annos,
#    size=1,
#    color=(255, 0, 0),
#    method=bb.util.DrawMethod.CV
#)
#
#i = 0
#while True:
#    img = drawer[i]
#
#    img = cv2.resize(img, (320, 240))
#    cv2.imshow("out", img)
#    k = cv2.waitKey(0)
#    if k == ord('d'):
#        i += 1
#        if i == len(drawer):
#            i = 0
#    elif k == ord('a'):
#        i -= 1
#        if i < 0:
#            i = len(drawer) - 1
#    elif k == 27:
#        break
