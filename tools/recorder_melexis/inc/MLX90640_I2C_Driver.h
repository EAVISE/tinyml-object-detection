#include <bcm2835.h>  



int I2CInit(void);
int I2CDeInit(void);
int I2CRead(uint8_t slaveAddr, uint16_t startAddr, uint32_t nMemAddressRead, uint16_t *data);
int I2CWrite(uint8_t slaveAddr, uint16_t writeAddr, uint16_t data);
int I2CFreqSet(uint32_t freq);
