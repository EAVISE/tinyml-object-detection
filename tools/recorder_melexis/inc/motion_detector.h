#ifndef MOTION_DETECTOR_H
#define MOTION_DETECTOR_H

#include <opencv2/opencv.hpp>

/*
 *  Detect if the input image contains motion
 */
class MotionDetector
{
public:
    cv::Mat get_mean(cv::Mat image)
    {
        if (m_image_buffer.size() < k_buffer_len) {
            m_image_buffer.push_back(image);
        } else {
            m_image_buffer[m_buffer_index] = image;
            m_buffer_index++;
            if (m_buffer_index == k_buffer_len)
                m_buffer_index = 0;
        }

        cv::Mat mean = cv::Mat::zeros(image.rows, image.cols, CV_64FC1);

        cv::Mat temp;
        for (auto& im: m_image_buffer) {
            im.convertTo(temp, CV_64FC1);
            mean += temp;
        }

        mean.convertTo(mean, CV_8UC1, 1. / m_image_buffer.size());

        return mean;
    }

    bool detect(cv::Mat& image)
    {
        bool frame_has_motion = false;
        cv::Mat image_small, delta_image, thresh_image;

        // preprocess to small gray deblurred image
        cv::resize(image, image_small, cv::Size(80, 60));
        cv::cvtColor(image_small, image_small, cv::COLOR_BGR2GRAY);
        cv::GaussianBlur(image_small, image_small, cv::Size(5, 5), 0);

        // take mean
        cv::Mat ref_image = get_mean(image_small);

        // calculate motion based based on background subtraction
        cv::absdiff(image_small, ref_image, delta_image);
        cv::threshold(delta_image, thresh_image, 25, 255, cv::THRESH_BINARY);
        cv::dilate(thresh_image, thresh_image, cv::Mat(), cv::Point(-1, -1), 2);

        // for debugging
        //cv::cvtColor(delta_image, image, cv::COLOR_GRAY2BGR);
        //cv::resize(image, image, cv::Size(640, 480));

        // detect contours
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(thresh_image, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

        // if a contour has an area size >= k_min_area, motion is detected
        for (auto& contour: contours) {
            double contour_area = cv::contourArea(contour);
            if ((contour_area < k_min_area) || (contour_area > k_max_area))
                continue;
            cv::Rect rect = cv::boundingRect(contour);
            rect.x *= k_scale;
            rect.y *= k_scale;
            rect.width *= k_scale;
            rect.height *= k_scale;
            cv::rectangle(image, rect, cv::Scalar(0, 255, 0), 2);
            frame_has_motion = true;
        }

        // filter out spurious detections
        if (m_motion_detected) {
            if (frame_has_motion) {
                m_counter = 0;
            } else {
                m_counter++;
                if (m_counter == 180) {
                    m_motion_detected = false;
                    m_counter = 0;
                }
            }
        } else {
            if (frame_has_motion) {
                m_counter++;
                if (m_counter == 6) {
                    m_motion_detected = true;
                    m_counter = 0;
                }
            } else {
                m_counter = 0;
            }
        }

        return m_motion_detected;
    }

private:
    static constexpr int k_buffer_len = 75;
    static constexpr double k_min_area = 125;
    static constexpr double k_max_area = 5000;
    static constexpr int k_scale = 8;
    std::vector<cv::Mat> m_image_buffer;
    int m_buffer_index = 0;
    int m_counter = 0;
    bool m_motion_detected = false;
};

#endif /* MOTION_DETECTOR_H */
