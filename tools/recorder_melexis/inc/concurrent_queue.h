#ifndef CONCURRENT_QUEUE_H
#define CONCURRENT_QUEUE_H

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

template <typename T>
class ConcurrentQueue {
public:
    bool empty() {
        std::unique_lock<std::mutex> mlock(mutex_);
        bool empty = queue_.empty();
        mlock.unlock();
        return empty;
    }

    void flush() {
        std::unique_lock<std::mutex> mlock(mutex_);
        std::queue<T> empty;
        std::swap(queue_, empty);
    }

    /*
     *  Get an item when notified but leave it in the queue
     *  and don't notify that it has been read
     */
    T peek_wait()
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.empty()) {
            cond_push_.wait(mlock);
        }
        auto val = queue_.front();
        mlock.unlock();
        return val;
    }

    T pop()
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.empty()) {
            cond_push_.wait(mlock);
        }
        auto val = queue_.front();
        queue_.pop();
        mlock.unlock();
        cond_pop_.notify_all();
        return val;
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.empty()) {
            cond_push_.wait(mlock);
        }
        item = queue_.front();
        queue_.pop();
        mlock.unlock();
        cond_pop_.notify_all();
    }

    void push(const T& item) {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.size() >= size_) {
            cond_pop_.wait(mlock);
        }
        queue_.push(item);
        mlock.unlock();
        cond_push_.notify_all();
    }

    ConcurrentQueue(unsigned int size=10) :
        size_(size)
    {
    }

    ConcurrentQueue(const ConcurrentQueue&) = delete;            // disable copying
    ConcurrentQueue& operator=(const ConcurrentQueue&) = delete; // disable assignment

private:
    std::queue<T> queue_;
    std::mutex mutex_;
    std::condition_variable cond_push_;
    std::condition_variable cond_pop_;
    unsigned int size_;
};

#endif

