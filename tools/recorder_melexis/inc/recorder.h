#ifndef RECORDER_H
#define RECORDER_H

#include <opencv2/opencv.hpp>
#include <thread>
#include <atomic>

#include "concurrent_queue.h"

class Recorder
{
public:
    /*
     *  Initialize recorder and start all threads
     */
    void start(std::string file_path, bool blend, int max_lwir_frames=5000);

    void stop();

    bool is_running()
    {
        return m_keep_running;
    }

private:
    bool m_keep_running = false;
    bool m_recording = false;
    bool m_blend = false;
    int m_max_lwir_frames;
    std::atomic<int> m_part_number;
    ConcurrentQueue<std::tuple<uint64_t, cv::Mat> > m_camera_image_save_queue;
    ConcurrentQueue<std::tuple<uint64_t, cv::Mat> > m_lwir_image_save_queue;
    ConcurrentQueue<cv::Mat> m_camera_image_process_queue;
    ConcurrentQueue<cv::Mat> m_lwir_image_process_queue;

    std::thread m_t_capture_camera;
    std::thread m_t_capture_lwir;
    std::thread m_t_save_camera;
    std::thread m_t_save_lwir;
    std::thread m_t_render_video_display;

    void task_capture_lwir();
    void task_capture_camera();
    void task_save_lwir(std::string file_path);
    void task_save_camera(std::string file_path);
    void task_render_video_display();
};

#endif /* RECORDER_H */
