import os
import fcntl
import signal
import time
import re
import http.server
import socketserver
import threading
import signal
from urllib.parse import urlparse
from urllib.parse import parse_qs
import subprocess


g_bin_root = os.path.dirname(__file__)
if g_bin_root == "":
    g_bin_root = "./"
g_recording_root = "/media/usb0"
g_blend = False
g_folder_name = ""


class RTSPServer:
    """ Run RTSP as terminal command """

    def __init__(self):
        self.running = False

    def start(self):
        if not self.running:
            self.subproc = subprocess.Popen(['test-launch', "'( udpsrc port=5000 ! application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264 ! rtph264depay ! rtph264pay name=pay0 pt=96 )'"], shell=False)
            self.running = True

    def stop(self):
        if self.running and self.subproc is not None:
            self.running = False
            self.subproc.send_signal(signal.SIGINT)
            self.subproc.wait()


class Recorder:
    """ Run recorder program as terminal command """

    def __init__(self):
        self.running = False
        self.subproc = None
        self.stdout_lines = []
        self.stdout_max_len = 100
        self.mutex = threading.Lock()
        self.buffer_thread = None

    def start(self, file_path, blend):
        if not self.running:
            self.stdout_lines = []
            self.running = True
            print("Starting recorder")
            self.subproc = subprocess.Popen([os.path.join(g_bin_root, 'main'), file_path, '--blend' if blend else ""],
                                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False, bufsize=1)

            # for non blocking read of stdout
            fcntl.fcntl(self.subproc.stdout.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)
            # start thread for reading stdout buffer
            self.buffer_thread = threading.Thread(target=self._stdio_read_buffer)
            self.buffer_thread.daemon = True
            self.buffer_thread.start()

    def _stdio_read_buffer(self):
        while self.running:
            time.sleep(0.1)
            try:
                data = self.subproc.stdout.read()
                if data is None:
                    continue

                lines = data.decode('utf8').splitlines()
                with self.mutex:
                    self.stdout_lines += lines
                    if len(self.stdout_lines) > self.stdout_max_len:
                        self.stdout_lines = self.stdout_lines[-self.stdout_max_len:]
            except IOError:
                pass

    def stop(self):
        if self.running and self.subproc is not None:
            print("Stopping recorder")
            self.running = False
            self.subproc.send_signal(signal.SIGINT)
            self.subproc.wait()
            self.buffer_thread.join()

    @property
    def is_running(self):
        if self.running:
            if self.subproc.poll() is None:
                return True
            else:
                self.running = False
        return False

    @property
    def returncode(self):
        if self.subproc is not None:
            return self.subproc.returncode
        return 0

    @property
    def stdout(self):
        with self.mutex:
            res = "\n".join(self.stdout_lines)
        return res


class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):

    def do_POST(self):
        global g_blend, g_folder_name

        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        query_components = parse_qs(post_data.decode('utf-8'))

        if 'name' in query_components:
            g_folder_name = query_components['name'][0]

        g_blend = 'blend' in query_components and query_components['recording'][0] == 'on'
        recording = 'recording' in query_components and query_components['recording'][0] == 'on'

        error_msg = ""
        if recording:
            if not g_recorder.is_running:
                if re.match("^[A-Za-z0-9_-]*$", g_folder_name):
                    file_path = os.path.join(g_recording_root, g_folder_name)
                    g_recorder.start(file_path, g_blend)
                else:
                    error_msg = 'Name can only contain letters, numbers underscores and dashes'
        elif g_recorder.is_running:
            g_recorder.stop()
            g_folder_name = self.get_folder_suggestion(g_recording_root)

        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        # redirect to main page
        html = '<html><head><link rel="icon" href="data:,"><meta http-equiv="refresh" content="0; URL=/" /></head><body></body></html>'
        self.wfile.write(bytes(html, "utf8"))

    def do_GET(self):
        global g_folder_name

        if g_folder_name == "":
            g_folder_name = self.get_folder_suggestion(g_recording_root)

        msg = ""
        if not g_recorder.is_running and g_recorder.returncode != 0:
            msg += '<p style="color:red;">Recorder returned error code: %d</p>'%g_recorder.returncode
        msg += "</br>Output:</br>" + g_recorder.stdout.replace('\n', '</br>')

        html = self.generate_html(g_folder_name, g_blend, g_recorder.is_running, msg)

        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header("Cache-Control", "no-cache, no-store, must-revalidate")
        self.send_header("Pragma", "no-cache")
        self.send_header("Expires", "0")
        self.end_headers()
        self.wfile.write(bytes(html, "utf8"))

    def get_folder_suggestion(self, root):
        folders = os.listdir(root)
        counter = 0
        while True:
            candidate_folder = 'set%03d'%counter
            if candidate_folder not in folders:
                break
            counter += 1
        return candidate_folder

    def generate_html(self, name, blend, recording, msg):

        with open(os.path.join(g_bin_root, "index.html")) as f:
            html_template = f.read()

        name_filler = name
        blend_filler = "checked" if blend else ""
        recording_filler = "checked" if recording else ""
        if recording:
            name_filler += ' disabled="disabled"'
            blend_filler += ' disabled="disabled"'


        html = html_template%(name_filler, blend_filler, recording_filler, msg)

        return html


def handle_exit(signum, frame):
    print('Stop RTSP server')
    g_rtsp_server.stop()
    print('Stopping recorder')
    g_recorder.stop()
    print('All stopped, exit')
    exit(0)


signal.signal(signal.SIGTERM, handle_exit)
signal.signal(signal.SIGINT, handle_exit)

g_recorder = Recorder()
g_rtsp_server = RTSPServer()

# start RTSP server
g_rtsp_server.start()

# Create an object of the above class
handler_object = MyHttpRequestHandler

PORT = 8080
socketserver.TCPServer.allow_reuse_address = True
my_server = socketserver.TCPServer(("", PORT), handler_object)

# Star the server
my_server.serve_forever()
