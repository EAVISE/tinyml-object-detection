#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>

#include "recorder.h"

#include <signal.h>

bool g_keep_running = true;

void exit_handler(int s)
{
    std::cout << "Exit signal caught" << std::endl;
    g_keep_running = false;
}

int main(int argc, char* argv[])
{
    std::string keys =
        "{help h usage ? |      | Print this message      }"
        "{@dirname       |<none>| Directory to record too }"
        "{blend          |      | Blend color and thermal views to a single view in the display stream}";

    cv::CommandLineParser parser(argc, argv, keys);
    parser.about("Melexis recorder");

    if (parser.has("help")) {
        parser.printMessage();
        return -1;
    }

    if (!parser.check()) {
        parser.printErrors();
        return 0;
    }

    Recorder recorder;

    signal (SIGINT, exit_handler);

    std::cout << "Stating recorder" << std::endl;
    recorder.start(parser.get<std::string>("@dirname"), parser.has("blend"));

    while(g_keep_running && recorder.is_running()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    std::cout << "Stopping recorder" << std::endl;
    recorder.stop();

    return 0;
}
