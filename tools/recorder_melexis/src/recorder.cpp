#include <iostream>
#include <fstream>
#include <filesystem>

extern "C" {
#include "MLX90640_I2C_Driver.h"
#include "MLX90640_API.h"

#define MLX_I2C_ADDR 0x33
}

#include "motion_detector.h"
#include "recorder.h"


uint64_t get_time()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}


std::string format_duration(uint64_t millis)
{
    using namespace std::chrono;
    auto ms = milliseconds(millis);
    auto secs = duration_cast<seconds>(ms);
    ms -= duration_cast<milliseconds>(secs);
    auto mins = duration_cast<minutes>(secs);
    secs -= duration_cast<seconds>(mins);
    auto hour = duration_cast<hours>(mins);
    mins -= duration_cast<minutes>(hour);

    char str[20];
    snprintf(str, sizeof(str), "%02llu:%02llu:%02llu:%02llu", hour.count(), mins.count(), secs.count(), ms.count() / 10);
    return str;
}


/*
 *  src: CV_32FC1
 *  dst: CV_8UC1
 */
void apply_agc_linear(cv::Mat& src, cv::Mat& dst, float min_temp=17.0, float max_temp=32.0)
{

    // clip temperatures to given range
    dst = src.clone();
    dst.setTo(min_temp, dst < min_temp);
    dst.setTo(max_temp, dst > max_temp);

    // normalize between 0 and 255
    dst = ((dst - min_temp) * 255) / (max_temp - min_temp);
    dst.convertTo(dst, CV_8UC1);
}

#define PIN RPI_GPIO_P1_24

void Recorder::task_capture_lwir()
{
    I2CInit();
    I2CFreqSet(250);

#ifdef APP_STEPPED
    // init gpio and pull pin down
    bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(PIN, LOW);
#endif

    static uint16_t eeMLX90640[832];
    float emissivity = 1;
    uint16_t frame[834];
    float eTa;

    MLX90640_SetRefreshRate(MLX_I2C_ADDR, 0b100);

    paramsMLX90640 mlx90640;
    MLX90640_DumpEE(MLX_I2C_ADDR, eeMLX90640);
    MLX90640_ExtractParameters(eeMLX90640, &mlx90640);

    int refresh = MLX90640_GetRefreshRate(MLX_I2C_ADDR);
    std::cout << "refresh rate = " << refresh << std::endl;

    static float mlx90640To[768];
    cv::Mat image(24, 32, CV_32FC1, mlx90640To);
    cv::Mat image_rotated;

    int frame_counter = 0;
    uint64_t timestamp;
    uint64_t timestamp_prev = get_time();

    while (m_keep_running) {
#ifdef APP_STEPPED
        bcm2835_gpio_write(PIN, HIGH);

        for (int i=0; i<5; i++) {
#endif
        MLX90640_GetFrameData(MLX_I2C_ADDR, frame);
        eTa = MLX90640_GetTa(frame, &mlx90640)-8.0;
        MLX90640_CalculateTo(frame, &mlx90640, emissivity, eTa, mlx90640To);
#ifdef APP_STEPPED
        }
        bcm2835_gpio_write(PIN, LOW);
#endif
        timestamp = get_time();

        // flip image and put in queue
        cv::flip(image, image_rotated, 0);
        if (m_recording)
            m_lwir_image_save_queue.push(std::make_tuple(timestamp, image_rotated.clone()));
        if (frame_counter % 2 == 0)
            m_lwir_image_process_queue.push(image_rotated.clone());

        std::cout << "MLX FPS = " << 1000. / (timestamp - timestamp_prev) << std::endl;
        timestamp_prev = timestamp;

#ifdef APP_STEPPED
        std::this_thread::sleep_for(std::chrono::milliseconds(4000));
#else
        frame_counter++;
#endif
    }

    I2CDeInit();
    // signal consumer to quit
    m_lwir_image_save_queue.push(std::make_tuple(timestamp, cv::Mat()));
}


void Recorder::task_capture_camera()
{
    cv::Mat image;
    uint64_t timestamp = 0;
    cv::VideoCapture cap;

    if (!cap.open("v4l2src device=/dev/video0 ! video/x-raw,width=640,heigth=480,framerate=30/1 ! videoconvert ! appsink")) {
        std::cout << "Error: Failed to open camera video capture" << std::endl;
        m_camera_image_save_queue.push(std::make_tuple(timestamp, cv::Mat()));
        m_camera_image_process_queue.push(cv::Mat());
        m_keep_running = false;
        return;
    }

    int frame_counter = 0;
    uint64_t timestamp_prev = get_time();

    while (m_keep_running) {

        if (!cap.read(image)) {
            std::cout << "Error: Failed to read camera image" << std::endl;
            m_keep_running = false;
            break;
        }
        timestamp = get_time();

        if (m_recording)
            m_camera_image_save_queue.push(std::make_tuple(timestamp, image.clone()));
        if (frame_counter % 2 == 0)
            m_camera_image_process_queue.push(image.clone());

        // we use a fixed delay. Camera framerate may vary between 10FPS to 30FPS (depending on amount of light)
        // we limit the framerate to max 1000 / 80 = 12.5FPS (less in practice)
        std::this_thread::sleep_for(std::chrono::milliseconds(80));

        std::cout << "FPS = " << 1000. / (timestamp - timestamp_prev) << std::endl;
        timestamp_prev = timestamp;

        frame_counter++;
    }

    // signal consumers to quit
    m_camera_image_save_queue.push(std::make_tuple(timestamp, cv::Mat()));
    m_camera_image_process_queue.push(cv::Mat());
    cap.release();
}


void Recorder::task_save_lwir(std::string file_path)
{
    cv::Mat image;
    cv::Mat image_u16;
    uint64_t timestamp;
    std::ofstream file_handle;
    int frame_counter = 0;
    std::string lwir_path = file_path + "/part000/MLX90640";

    if (std::filesystem::exists(lwir_path)) {
        // remove dir in case it exists to ensure an empty dir to start with
        std::filesystem::remove_all(lwir_path);
    }
    std::filesystem::create_directories(lwir_path);

    while (true) {
        std::tie(timestamp, image) = m_lwir_image_save_queue.pop();
        if (image.empty())
            break;

        if (!file_handle.is_open()) {
            std::string lwir_timestamps_file = lwir_path + "/timestamps.txt";
            std::cout << "Opening new file: " << lwir_timestamps_file << std::endl;
            file_handle.open(lwir_timestamps_file);
        }

        // convert to 16-bit integer values
        image.convertTo(image_u16, CV_16UC1, 100);

        char filename[20];
        snprintf(filename, sizeof(filename), "/image%07d.tiff", frame_counter);
        cv::imwrite(lwir_path + filename, image_u16);
        file_handle << timestamp << std::endl;
        frame_counter++;

        // optionally close current timestamps file and prepare new folder
        if (frame_counter == m_max_lwir_frames) {
            frame_counter = 0;
            m_part_number++;
            int part_number = m_part_number;
            file_handle.close();
            char lwir_sub_path[20];
            snprintf(lwir_sub_path, sizeof(lwir_sub_path), "/part%03d/MLX90640", part_number);
            lwir_path = file_path + lwir_sub_path;
            std::filesystem::create_directories(lwir_path);
        }
    }
}


void Recorder::task_save_camera(std::string file_path)
{
    cv::Mat image;
    uint64_t timestamp;
    cv::VideoWriter writer;
    std::ofstream file_handle;
    int prev_part_number = m_part_number;
    std::string cam_path = file_path + "/part000/Camera";

    std::filesystem::create_directories(cam_path);

    while (true) {
        std::tie(timestamp, image) = m_camera_image_save_queue.pop();
        if (image.empty())
            break;

        if (!writer.isOpened()) {
            int width = image.cols;
            int height = image.rows;
            std::string cam_video_file = cam_path + "/video.mkv";
            std::string gst_pipeline = "appsrc ! videoconvert ! omxh264enc target-bitrate=2500000 control-rate=1 ! h264parse ! matroskamux ! filesink location=" + cam_video_file;
            std::cout << "Opening new file: " << cam_video_file << std::endl;
            if (!writer.open(gst_pipeline, 0, 10, cv::Size(width, height))) {
                std::cout << "Error: Failed to open writer" << std::endl;
                m_keep_running = false;
                break;
            }
        }

        if (!file_handle.is_open()) {
            std::string cam_timestamps_file = cam_path + "/timestamps.txt";
            std::cout << "Opening new file: " << cam_timestamps_file << std::endl;
            file_handle.open(cam_timestamps_file);
        }

        // store video frame and capture time
        writer.write(image);
        file_handle << timestamp << std::endl;

        // optionally close current video and timestamps file and prepare new folder
        int part_number = m_part_number;
        if (prev_part_number != part_number) {
            prev_part_number = part_number;
            writer.release();
            file_handle.close();
            char cam_sub_path[20];
            snprintf(cam_sub_path, sizeof(cam_sub_path), "/part%03d/Camera", part_number);
            cam_path = file_path + cam_sub_path;
            std::filesystem::create_directories(cam_path);
        }
    }
}

void Recorder::task_render_video_display()
{
    cv::Mat camera_image;
    cv::Mat canvas;
    cv::Mat lwir_image_raw;
    cv::Mat lwir_image;
    cv::VideoWriter writer;
    MotionDetector detector;
    std::string gst_pipeline = "appsrc ! videoconvert ! omxh264enc target-bitrate=500000 control-rate=1 ! video/x-h264,profile=baseline,stream-format=byte-stream ! " \
                               "h264parse ! rtph264pay name=pay0 pt=96 ! udpsink host=127.0.0.1 port=5000 sync=false";

    // ensure we have an lwir image before we start the loop
    lwir_image_raw = m_lwir_image_process_queue.pop();

    uint64_t recording_start_time;
    uint64_t total_recording_time = 0;
    bool has_motion = false;

    while (true) {
        camera_image = m_camera_image_process_queue.pop();
        //std::cout << "Got camera frame" << std::endl;
        if (camera_image.empty())
            break;

        if (!m_lwir_image_process_queue.empty()) {
            lwir_image_raw = m_lwir_image_process_queue.pop();
            //std::cout << "LWIR frame updated" << std::endl;
        }

        // normalize thermal image, resize and add pseudo color
        apply_agc_linear(lwir_image_raw, lwir_image);
        cv::resize(lwir_image, lwir_image, cv::Size(640, 480));
        cv::applyColorMap(lwir_image, lwir_image, cv::COLORMAP_HOT);

        // detect motion
        if (detector.detect(camera_image)) {
            if (!has_motion) {
                has_motion = true;
                m_recording = true;
                recording_start_time = get_time();
            }
        } else {
            if (has_motion) {
                has_motion = false;
                m_recording = false;
                total_recording_time += (get_time() - recording_start_time);
            }
        }

        if (m_blend) {
            // blend with camera image
            cv::addWeighted(camera_image, 0.5, lwir_image, 0.5, 0.0, canvas);
        } else {
            // catenate with camera image
            cv::hconcat(camera_image, lwir_image, canvas);
        }

        if (m_recording) {
            const uint64_t rec_time = total_recording_time + (get_time() - recording_start_time);
            const std::string rec_time_str = format_duration(rec_time);
            cv::putText(canvas, "Rec: " + rec_time_str, cv::Point(10, 40), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 0, 255), 2);
        }

        if (!writer.isOpened()) {
            int width = canvas.cols;
            int height = canvas.rows;
            if (!writer.open(gst_pipeline, 0, 3, cv::Size(width, height))) {
                std::cout << "Error: Failed to open video stream writer" << std::endl;
                m_keep_running = false;
                return;
            }
        }

        writer.write(canvas);

    }
}

void Recorder::start(std::string file_path, bool blend, int max_lwir_frames)
{
    m_recording = false;
    m_keep_running = true;
    m_blend = blend;
    m_max_lwir_frames = max_lwir_frames;
    m_part_number = 0;

    // start threads
    m_t_save_camera = std::thread(&Recorder::task_save_camera, this, file_path);
    m_t_save_lwir = std::thread(&Recorder::task_save_lwir, this, file_path);
    m_t_capture_camera = std::thread(&Recorder::task_capture_camera, this);
    m_t_capture_lwir = std::thread(&Recorder::task_capture_lwir, this);
    m_t_render_video_display = std::thread(&Recorder::task_render_video_display, this);
}

void Recorder::stop()
{
    m_keep_running = false;
    if (m_t_capture_camera.joinable())
        m_t_capture_camera.join();

    if (m_t_capture_lwir.joinable())
        m_t_capture_lwir.join();

    if (m_t_save_camera.joinable())
        m_t_save_camera.join();

    if (m_t_save_lwir.joinable())
        m_t_save_lwir.join();

    if (m_t_render_video_display.joinable())
        m_t_render_video_display.join();
}
