#include <bcm2835.h>
#include "MLX90640_I2C_Driver.h"

int I2CInit(void){
	int ret;

	ret = bcm2835_init();
    if (!ret){
		return 1;
	}

    ret = bcm2835_i2c_begin();
    return 0;
}

int I2CDeInit(void){
    return bcm2835_close();
}

int I2CRead(uint8_t slaveAddr, uint16_t startAddr, uint32_t nMemAddressRead, uint16_t *data){
	char cmd[2];
	char buf[1664];
	uint32_t cmds_len = 2;
	uint16_t *p;
	int i=0;
	int ret;

	p = data;

	cmd[0] = (char)((startAddr>>8)&0x00FF);
	cmd[1] = (char)(startAddr&0x00FF);

	bcm2835_i2c_setSlaveAddress(slaveAddr);
	ret = bcm2835_i2c_write_read_rs(&cmd[0], cmds_len, buf, 2*nMemAddressRead);

	if(ret!=0){
		return ret;
	}

	for (int cnt=0; cnt<nMemAddressRead; cnt++){
		i = cnt <<1;
		*p++ = (uint16_t)buf[i]*256 + (uint16_t)buf[i+1];
	}
	return 0;
}


int I2CWrite(uint8_t slaveAddr, uint16_t writeAddr, uint16_t data){
	char cmd[4];
	uint32_t cmds_len = 4;

	int ret;

	cmd[0] = (char)((writeAddr>>8)&0x00FF);
	cmd[1] = (char)(writeAddr&0x00FF);
	cmd[2] = (char)((data>>8)&0x00FF);
	cmd[3] = (char)(data&0x00FF);
	bcm2835_i2c_setSlaveAddress(slaveAddr);
	ret = bcm2835_i2c_write(cmd, cmds_len);

	return ret;
}

int I2CFreqSet(uint32_t freq){
	bcm2835_i2c_set_baudrate(freq * 1000);
	return 0;
}
