# Melexis recorder

Recorder on RPi 3b with web page control. Recorder is a C++ program and the web service that starts the recorder is a python program.
Video can be seen through RTSP stream.

## Setup

### RPi image

Start from a real-time linux RPi image from https://github.com/guysoft/RealtimePi and write it to an SD card:

```
wget http://unofficialpi.org/Distros/RealtimePi/2021-06-15_2021-05-07-realtimepi-buster-armhf-lite-0.5.0.zip
unzip 2021-06-15_2021-05-07-realtimepi-buster-armhf-lite-0.5.0.zip
sudo dd if=2021-06-15_2021-05-07-realtimepi-buster-armhf-lite-0.5.0.img of=/dev/xxx bs=4M conv=fsync
```

### Setup WIFI

#### Home WIFI:

Add the folliwing entry to `/etc/wpa_supplicant/wpa_supplicant.conf`:

```
network={
    ssid="MySSID"
    psk="mypasswd"
    key_mgmt=WPA-PSK
}
network={
    ssid="MaSiOrange"
    psk="Y59XRWzv"
    key_mgmt=WPA-PSK
}

```

#### Enterprise WIFI:

Since there is a bug in the wpa_supplicant package in the buster raspbian release installed on the RPI, we need to update the wpa_supplicant package first:

```
wget https://w1.fi/releases/wpa_supplicant-2.9.tar.gz
tar -xvf wpa_supplicant-2.9.tar.gz
sudo apt install libnl-3-dev libdbus-1-dev libnl-genl-3-dev libssl-dev
cd wpa_supplicant-2.9/wpa_supplicant/
cp -p defconfig .config
make -j4
sudo make install

sudo systemctl stop wpa_supplicant
sudo mv /usr/sbin/wpa_supplicant /usr/sbin/wpa_supplicant_old
sudo ln -s /usr/local/sbin/wpa_supplicant /usr/sbin/wpa_supplicant
sudo systemctl daemon-reload
sudo systemctl restart wpa_supplicant
```

Add the folliwing entry to `/etc/wpa_supplicant/wpa_supplicant.conf`:

```
network={
    ssid="eduroam"
    priority=1
    proto=RSN
    key_mgmt=WPA-EAP
    phase2="auth=MSCHAPV2"
    eap=PEAP
    identity="u0113870@kuleuven.be"
    password="password"
}

```

### Automount USB disk

```
sudo apt-get install usbmount
```

Add `uid=pi,gid=pi` to `MOUNTOPTIONS in `/etc/usbmount/usbmount.conf` to be able to write files as pi user

Modify in file `/lib/systemd/system/systemd-udevd.service`:

```
PrivateMounts=yes
```

to

```
PrivateMounts=no
```

Now reboot

### Install dependencies

#### BCM library

The BCM2835 library is for controlling IO pins and I2C device (needed for MLX90640)

```
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.68.tar.gz
tar -xvf bcm2835-1.68.tar.gz
cd bcm2835-1.68
./configure
make
sudo make install
```

#### OpenCV with gstreamer support

Install the following apt packages:

```
# for numpy (needed by opencv)
libatlas-base-dev

# for opencv
ffmpeg
libgstreamer1.0-dev
libgstreamer-plugins-base1.0-dev

# for command line
gstreamer1.0-tools
gstreamer1.0-plugins-good
gstreamer1.0-plugins-bad
gstreamer1.0-omx
```

Install pre-build OpenCV .deb package included in this repo:

```
sudo dpkg -i opencv_4.5.2-pre-1_armhf.deb
```

If you want opencv in python to work also install:

```
sudo apt install libgtk-3-0 python3-pip
pip3 install numpy
```

#### RTSP server

```
wget https://gstreamer.freedesktop.org/src/gst-rtsp-server/gst-rtsp-server-1.10.4.tar.xz
tar -xvf gst-rtsp-server-1.19.1.tar.xz
cd gst-rtsp-server-1.19.1
./configure
make
sudo ln -s `pwd`/examples/test-launch /usr/bin/test-launch
```

### Install recorder software

In the downloaded repo, just run:

```
make
```

## Running manually

Test recorder with:

```
sudo python3 server.py
```

Then surf to `http://<ip address>:8080. You should see a web page.

You can view the RTSP stream with:

```
gst-launch-1.0 rtspsrc location=rtsp://<ip address>:8554/test latency=0 buffer-mode=auto ! decodebin ! videoconvert ! autovideosink sync=false
```

Hit ctrl-C to stop.

## Running as a system service

Install as a system service that will start the web server at boot:

```
sudo cp melexis-recorder.sh /etc/init.d/
# Adapt BASEDIR to your environment
sudo vi /etc/init.d/melexis-recorder.sh
sudo update-rc.d melexis-recorder.sh defaults
sudo service melexis-recorder start
```

## Setup autossh

In case you want to connect to the outside world, we can setup an ssh tunnel to an external server. Through this server, we can connect to the node from anywhere.

```
sudo apt install autossh
```

Generate key to be able to connect to my server (no password, default file location)

```
sudo ssh-keygen
```

Copy public key to the server

```
sudo ssh-copy-id debian@94.104.84.89
```

To test if the key is working, login as root and connect to the server with:

```
ssh debian@94.104.84.89
```

You should be able to login without using a password.

Create a file: /etc/systemd/system/autossh.service with following content:

```
[Unit]
Description=AutoSSH service
After=network.target

[Service]
Environment="AUTOSSH_GATETIME=0"
ExecStart=/usr/bin/autossh -M 0 -N -R88XX:localhost:22 -o TCPKeepAlive=yes -o ServerAliveInterval=45 -o ServerAliveCountMax=2 -o ExitOnForwardFailure=yes debian@94.104.84.89

[Install]
WantedBy=multi-user.target
```

IMPORTANT: for setup1 replace XX with 21 and for setup2 replace XX with 22

Then execute:

```
sudo systemctl daemon-reload
sudo systemctl enable autossh
sudo systemctl start autossh
```
