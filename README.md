# TinyML Object Detection

This repo contains the code from the paper [*Person Detection Using an Ultra Low-Resolution Thermal Imager on a Low-Cost MCU*](https://link.springer.com/chapter/10.1007/978-3-031-25825-1_3).

<p align="center">
<a href="https://www.youtube.com/watch?v=UA8LkVilUfY"> <img src="https://img.youtube.com/vi/UA8LkVilUfY/default.jpg" width="300"> </a>
</p>

*(Click the image for demo video)*

The folder `lightnet_examples` contains pytorch code for developing (training/testing/exporting) models (see this [README.md](lightnet_examples/README.md) for more details).

The folder `embedded` contains C-code implementations for evaluating the models on MCU targets (see this [README.md](embedded/README.md) for more details).

The folder `tools` contains the source code for our recorder hardware and source code for processing and annotating recorded videos. These tools are only usefull in case you would like to extend the dataset.

# Cite our work

```
@inproceedings{vandersteegen2022person,
  title={Person Detection Using an Ultra Low-Resolution Thermal Imager on a Low-Cost MCU},
  author={Vandersteegen, Maarten and Reusen, Wouter and Beeck, Kristof Van and Goedem{\'e}, Toon},
  booktitle={International Conference on Image and Vision Computing New Zealand},
  pages={33--47},
  year={2022},
  organization={Springer}
}

```
