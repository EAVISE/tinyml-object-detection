import torch
from torchvision import transforms as tf
import lightnet as ln
import models

__all__ = ['params']


params = ln.engine.HyperParameters(
    # Network
    input_dimension = (32, 24),
    batch_size = 8,
    mini_batch_size = 8,
    max_batches = 10000,
    use_background_image = True,
    subtract_background_image = True,
    diff_channel_stride = 0,

    # Dataset
    _train_set = 'data/video/meta_top/train.txt',
    _valid_set = 'data/video/meta_top/val.txt',
    _test_set = 'data/video/meta_test/all_90.txt',
    min_bbox_area_train = 15,    # area in pixels
    min_bbox_area_test = 15,     # area in pixels

    # dataset preprocessing type
    color=False,

    # Temperature range sensor
    min_value = 0,
    max_value = 10000,  # divide by 100 to get degrees

    # Thermal dataset statistics
    mean = 2237,
    std = 155,

    # Data Augmentation
    contrast = .1,
    brightness = .01,
    hflip = .5,
    vflip = .5,
    noise = 0.,

    # Early stopping criteria
    stop_pruning_loss_scale = 3.0,      # stop pruning when the test loss >= the starting validation loss times this scale
    stop_finetune_loss_scale = 1.03,    # stop (or don't start) fine-tuning when the validation loss <= the starting validation loss times this scale

)

input_channels = 1
params.network = models.MicroYolo(
    motion_output=False,
    input_channels=input_channels,
    anchors=utils.get_anchors('90'),
    backbone='darknet_mobile_micro',
    pretrained_backbone = False,
)

# Loss
params.loss = ln.network.loss.RegionLoss(
    1,
    params.network.anchors,
    params.network.stride,
    coord_prefill = 0,
)

# Postprocessing
params._post = ln.data.transform.Compose([
    ln.data.transform.GetDarknetBoxes(0.001, params.network.stride, params.network.anchors),
    ln.data.transform.NMS(0.5),
    ln.data.transform.TensorToBrambox(['person']),
])

# Optimizer
params.optimizer = torch.optim.SGD(
    params.network.parameters(),
    lr = .0001,
    momentum = .9,
    weight_decay = .03,
    dampening = 0,
)

# Scheduler
params.scheduler = torch.optim.lr_scheduler.MultiStepLR(
    params.optimizer,
    milestones = [params.max_batches/2],
    gamma = .1,
)

# Pruner
params.pruner = ln.prune.L2Pruner(
    params.network,
    params.input_dimension + (input_channels,),
    params.optimizer,
)
