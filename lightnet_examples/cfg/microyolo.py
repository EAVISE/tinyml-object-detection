import torch
from torchvision import transforms as tf
import lightnet as ln
import models
import utils

__all__ = ['params']


params = ln.engine.HyperParameters(
    # Network
    input_dimension = (32, 24),
    batch_size = 8,
    mini_batch_size = 8,
    max_batches = 50000,
    use_background_image = True,
    subtract_background_image = True,
    diff_channel_stride = 0,

    # Dataset
    _train_set = 'data/video/meta_top/train.txt',
    _valid_set = 'data/video/meta_top/val.txt',
    _test_set = 'data/video/meta_test/all_90.txt',
    min_bbox_area_train = 15,    # area in pixels
    min_bbox_area_test = 15,     # area in pixels

    # dataset preprocessing type
    color=False,

    # Temperature range sensor
    min_value = 0,
    max_value = 10000,  # divide by 100 to get degrees

    # Thermal dataset statistics
    mean = 2237,
    std = 155,

    # Data Augmentation
    contrast = .1,
    brightness = .01,
    hflip = .5,
    vflip = .5,
    noise = 0.,
)

# Network
def init_weights(m):
    if isinstance(m, torch.nn.Conv2d):
        torch.nn.init.kaiming_normal_(m.weight, nonlinearity='leaky_relu')

params.network = models.MicroYolo(
    motion_output=False,
    input_channels=1,
    anchors=utils.get_anchors('90'),
    backbone='darknet_mobile_micro',
    pretrained_backbone = False,
)
params.network.apply(init_weights)

# Loss
params.loss = ln.network.loss.RegionLoss(
    1,
    params.network.anchors,
    params.network.stride,
)

# Postprocessing
params._post = ln.data.transform.Compose([
    ln.data.transform.GetDarknetBoxes(0.001, params.network.stride, params.network.anchors),
    ln.data.transform.NMS(0.5),
    ln.data.transform.TensorToBrambox(['person']),
])

# Optimizer
params.optimizer = torch.optim.SGD(
    params.network.parameters(),
    lr = .001,
    momentum = .9,
    weight_decay = .03,
    dampening = 0,
)

# Scheduler
burn_in = torch.optim.lr_scheduler.LambdaLR(
    params.optimizer,
    lambda b: (b / 1000) ** 4,
)
#step = torch.optim.lr_scheduler.MultiStepLR(
#    params.optimizer,
#    milestones = [10000, 40000],
#    gamma = .1,
#)
plateau = torch.optim.lr_scheduler.ReduceLROnPlateau(
    params.optimizer,
    patience=5000,      # number of batches
    threshold=1e-4,
    mode='min',
    factor=0.1,
)
params.scheduler = ln.engine.SchedulerCompositor(
#   batch   scheduler
    (0,     burn_in),
    (1000,  plateau),
)
