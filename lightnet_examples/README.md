# Install dependencies

Make a python virtual environment and run (tested with python3.6 and python3.7):

```
pip install -r requirements.txt
```

Note that the best model files are included with git FLS

# Download and unpack the dataset

The dataset can be downloaded [here](https://iiw.kuleuven.be/onderzoek/eavise/mldetection/home).

Unpack the dataset in a directory of choice, for example `/path/to/unpacked/dataset`, and create two symlinks that refer to it:

```
cd data/video
ln -s /path/to/unpacked/dataset/data data
ln -s /path/to/unpacked/dataset/test datatest
```

# Test a pre-trained model

Test the accuracy of our un-pruned 90-degree model:

```
python bin/test.py logs/set16_top/maarten-denayer_August31_18_20_37_microyolo/checkpoints/best.state.pt -n logs/set16_top/maarten-denayer_August31_18_20_37_microyolo/microyolo.py --bg_builder --visual
```

The `--visual` flag will display a PR-curve at the end and, when you close the plot window, it will display the detections in the test videos.

Test the accuracy of our pruned 90-degree model:

```
python bin/test.py logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/checkpoints/pruned-49.pt -n logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/microyolo_l2_prune_90.py --bg_builder --visual
```

Test the accuracy of our quantized and pruned 90-degree model. This model is ran in tflite instead of pytorch, to simulate the quantization behaviour on target:

```
python bin/test.py logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/checkpoints/pruned-49.tflite -n logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/microyolo_l2_prune_90.py --bg_builder --visual
```

Note that all test commands are listed in `run_paper_tests.sh`.

# Train a model

Models are trained from scratch, so no pre-trained weights are needed here:

```
python bin/train.py -n cfg/microyolo.py -l logs/my_output_set -c -d -m 0
```

The option `-d` enables deterministic mode which means that exactly the same training results will come out when we run this command multiple times. You can ommit `-d` or change the random seed to `-m 1` for example to disable this behaviour. During training, tensorboard loggings are written to the output folder in `logs/my_output_set/some_generated_folder_name` together with stdout loggings, a copy of `microyolo.py` and the best weights file.

# Pruning a model

Pruning can be started on a pre-trained model. First extract the pre-trained weights from the training state file for example:

```
python bin/best_state_to_best.py logs/set16_top/maarten-denayer_August31_18_20_37_microyolo/checkpoints/best.state.pt
```

This will produce a `best.pt` file in the same folder as the 'best.state.pt'. Then run:

```
python bin/prune.py -n cfg/microyolo_l2_prune.py -c logs/set16_top/maarten-denayer_August31_18_20_37_microyolo/checkpoints/best.pt -l logs/my_output_prune_set -p 0.05 -d -m 0
```

The best model from each pruning step is saved in `logs/my_output_prune_set/some_generated_folder_name/`, together with stdout loggings, tensorboard loggings and a copy of `microyolo_l2_prune.py`.


# Quantizing a model

After pruning (or without pruning), a model can be quantized with:

```
python bin/export_to_tflite.py logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/microyolo_l2_prune_90.py logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/checkpoints/pruned-49.pt my_model.tflite --quant -d -m 0
```

This script first loads the pytorch model, folds the batchnorm layers, then converts it into a tensorflow model and finally converts it into a tensorflow lite model. During conversion from Tensorflow to Tensorflow Lite, PTQ is applied. Since 100 random samples from the training set are used for PTQ calibration, deterministic behaviour can also be enabled with `-d`.
