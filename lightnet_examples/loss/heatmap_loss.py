import logging
import math
import numpy as np
import torch
import torch.nn as nn

__all__ = ['HeatmapLoss']
log = logging.getLogger(__name__)


class HeatmapLoss(nn.modules.loss._Loss):
    """ Computes heatmap loss from TTFNet

    Args:
        stride (int, optional):     The downsampling factor of the network (input_dimension / output_dimension); Default **4**
        alpha (float, optional):    Width scaling factor for the target gassians
        alpha_f (float, optional):  Alpha of modified focal loss (from cornernet/centernet)
        beta_f (float, optional):   Beta of modified focal loss (from cornernet/centernet)
    """
    def __init__(self, stride=4, alpha=0.54, alpha_f=2.0, beta_f=4):
        super().__init__()

        self.stride = stride
        self.alpha = alpha
        self.alpha_f = alpha_f
        self.beta_f = beta_f

        self.loss_heatmap = torch.tensor(0.0)

    @property
    def values(self):
        """ Return various sub-losses values as a dictionary.

        Note:
            You can access the individual loss values directly as ``object.loss_<name>`` as well. |br|
            This will return the actual loss tensor with its attached computational graph and gives you full freedom for modifying this loss prior to the backward pass.
        """
        return {
            'heatmap': self.loss_heatmap.item(),
        }

    @property
    def loss(self):
        log.deprecated('The "loss" attribute is deprecated in favor for "loss_total"')
        return self.loss_heatmap

    def extra_repr(self):
        repr_str = f'stride={self.stride}, alpha={self.gaussian_iou}, alpha_f={self.alpha_f}, beta_f={self.beta_f}, gamma_f={self.gamma_f}'
        return repr_str

    def forward(self, output, target):
        """ Compute heatmap loss.

        Args:
            output (1 Tensor): Output from the network
            target (brambox annotation dataframe): Brambox annotations
        """
        # Parameters
        device = output.device
        nB, nC, nH, nW = output.shape
        assert nC == 1, "Only support single class for now"

        # limit output range
        out_heatmaps = output.sigmoid().clamp(min=1e-4, max=1-1e-4)

        # calculate targets
        target_heatmaps = self.build_targets(target, nH, nW)

        # calculate loss
        self.loss_heatmap = self.ct_focal_loss(out_heatmaps, target_heatmaps.to(device))

        return self.loss_heatmap

    def build_targets(self, ground_truth, nH, nW):
        """ Convert ground truth to heatmap tensors """
        heatmaps = []

        for b, gt_batch in ground_truth.groupby('batch_number', sort=False):
            # for now, treat ignored bboxes as background since its difficult to really ignore with heatmap loss
            gt_batch = gt_batch[gt_batch.ignore == False]   # just remove them

            size = torch.from_numpy(gt_batch[['width', 'height']].values).float() / self.stride
            coords = torch.empty((gt_batch.shape[0], 4), requires_grad=False)
            coords[:, 0:2] = torch.from_numpy(gt_batch[['x_top_left', 'y_top_left']].values).float() / self.stride
            coords[:, 2:4] = coords[:, 0:2] + size
            heatmap = self.target_single_image(coords, nH, nW)
            heatmaps.append(heatmap)

        return torch.stack(heatmaps)

    def ct_focal_loss(self, pred, gt):
        """
        Focal loss used in CornerNet & CenterNet. Note that the values in gt (target) are in [0, 1] since
        gaussian is used to reduce the punishment and we treat [0, 1) as neg example.

        Args:
            pred: tensor, any shape.
            gt: tensor, same as pred.

        Returns:

        """
        pos_inds = gt.eq(1).float()
        neg_inds = gt.lt(1).float()

        neg_weights = torch.pow(1 - gt, self.beta_f)  # reduce punishment
        pos_loss = -torch.log(pred) * torch.pow(1 - pred, self.alpha_f) * pos_inds
        neg_loss = -torch.log(1 - pred) * torch.pow(pred, self.alpha_f) * neg_weights * neg_inds

        num_pos = pos_inds.float().sum()
        pos_loss = pos_loss.sum()
        neg_loss = neg_loss.sum()

        if num_pos == 0:
            return neg_loss
        return (pos_loss + neg_loss) / num_pos

    def target_single_image(self, gt_boxes, output_h, output_w):
        """
        Args:
            gt_boxes: tensor, tensor <=> img, (num_gt, 4).
                -> [(x1, y1, w1, h1), (x2, y2, w2, h2), ...]

        Returns:
            heatmap: tensor, tensor <=> img, (h, w).
        """
        heatmap = gt_boxes.new_zeros((output_h, output_w))
        fake_heatmap = gt_boxes.new_zeros((output_h, output_w))

        # get indices of sorted boxes by log of box area: largest area first
        boxes_areas_log = bbox_areas(gt_boxes).log()
        _, boxes_ind = torch.topk(boxes_areas_log, boxes_areas_log.size(0))

        gt_boxes = gt_boxes[boxes_ind]

        gt_boxes[:, [0, 2]] = torch.clamp(gt_boxes[:, [0, 2]], min=0, max=output_w - 1)
        gt_boxes[:, [1, 3]] = torch.clamp(gt_boxes[:, [1, 3]], min=0, max=output_h - 1)
        feat_hs, feat_ws = (gt_boxes[:, 3] - gt_boxes[:, 1], gt_boxes[:, 2] - gt_boxes[:, 0])

        # we calc the center and ignore area based on the gt-boxes of the origin scale
        # no peak will fall between pixels
        ct_ints = (torch.stack([(gt_boxes[:, 0] + gt_boxes[:, 2]) / 2,
                                (gt_boxes[:, 1] + gt_boxes[:, 3]) / 2],
                               dim=1)).to(torch.int)

        h_radiuses_alpha = (feat_hs / 2. * self.alpha).int()
        w_radiuses_alpha = (feat_ws / 2. * self.alpha).int()

        # larger boxes are rendered first, then smaller boxes
        for k in range(boxes_ind.shape[0]):

            fake_heatmap = fake_heatmap.zero_()
            draw_truncate_gaussian(fake_heatmap, ct_ints[k],
                                   h_radiuses_alpha[k].item(), w_radiuses_alpha[k].item())
            heatmap = torch.max(heatmap, fake_heatmap)

        return heatmap


def gaussian_2d(shape, sigma_x=1, sigma_y=1):
    """TODO: port to calculation with torch rather than numpy
    """
    m, n = [(ss - 1.) / 2. for ss in shape]
    y, x = np.ogrid[-m:m + 1, -n:n + 1]

    h = np.exp(-(x * x / (2 * sigma_x * sigma_x) + y * y / (2 * sigma_y * sigma_y)))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0

    return h


def draw_truncate_gaussian(heatmap, center, h_radius, w_radius, k=1):
    h, w = 2 * h_radius + 1, 2 * w_radius + 1
    sigma_x = w / 6
    sigma_y = h / 6
    gaussian = gaussian_2d((h, w), sigma_x=sigma_x, sigma_y=sigma_y)
    gaussian = heatmap.new_tensor(gaussian)

    x, y = int(center[0]), int(center[1])

    height, width = heatmap.shape[0:2]

    left, right = min(x, w_radius), min(width - x, w_radius + 1)
    top, bottom = min(y, h_radius), min(height - y, h_radius + 1)

    masked_heatmap = heatmap[y - top:y + bottom, x - left:x + right]
    masked_gaussian = gaussian[h_radius - top:h_radius + bottom,
                      w_radius - left:w_radius + right]
    if min(masked_gaussian.shape) > 0 and min(masked_heatmap.shape) > 0:
        torch.max(masked_heatmap, masked_gaussian * k, out=masked_heatmap)
    return heatmap


def bbox_areas(bboxes, keep_axis=False):
    x_min, y_min, x_max, y_max = bboxes[:, 0], bboxes[:, 1], bboxes[:, 2], bboxes[:, 3]
    areas = (y_max - y_min + 1) * (x_max - x_min + 1)
    if keep_axis:
        return areas[:, None]
    return areas

