# generate train/valid video clip lists
import random
from pathlib import Path


def parse_meta(filename):
    with open(filename) as f:
        lines = f.read().splitlines()
    entries = {}
    for line in lines:
        elems = line.split('=')
        entries[elems[0].strip()] = elems[1].strip()
    return entries

clips = []
for meta_file in Path('../data').rglob('meta.txt'):
    entries = parse_meta(meta_file)
    angle = float(entries['recorder_angle'])
    location = meta_file.parents[1].stem
    if location in ['location09', 'location10', 'location11']:
        continue
    #if angle > 45.0:
    #    continue

    for clip_file in Path(meta_file.parent).rglob('clip*'):
        clips.append(str(Path(*clip_file.parts[1:])))

# limit the number of clips in location12 and location13 to 50
clips_loc12 = [clip for clip in clips if 'location12' in clip]
clips_loc13 = [clip for clip in clips if 'location13' in clip]
clips_other = [clip for clip in clips if not 'location12' in clip and not 'location13' in clip]
random.shuffle(clips_loc12)
clips_loc12 = clips_loc12[:50]
random.shuffle(clips_loc13)
clips_loc13 = clips_loc13[:50]
clips = sorted(clips_other + clips_loc12 + clips_loc13)

# split between train and valid: location07 and location08 are valid
valid_clips = [clip for clip in clips if 'location07' in clip or 'location08' in clip]
train_clips = [clip for clip in clips if not 'location07' in clip and not 'location08' in clip]

with open("train.txt", 'w') as f:
    f.write('\n'.join(train_clips))

with open("val.txt", 'w') as f:
    f.write('\n'.join(valid_clips))
