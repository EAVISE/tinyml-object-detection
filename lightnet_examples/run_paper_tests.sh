#!/bin/bash

# 90-degree PR-curve
python bin/test.py logs/set16_top/maarten-denayer_September13_23_57_58_microyolo_4/checkpoints/best.state.pt -n logs/set16_top/maarten-denayer_September13_23_57_58_microyolo_4/microyolo_4.py --output notebooks/detections/set16_top_maarten-denayer_September13_23_57_58_microyolo_4.parquet
python bin/test.py logs/set16_top/maarten-denayer_September14_05_29_51_microyolo_5/checkpoints/best.state.pt -n logs/set16_top/maarten-denayer_September14_05_29_51_microyolo_5/microyolo_5.py --output notebooks/detections/set16_top_maarten-denayer_September14_05_29_51_microyolo_5.parquet
python bin/test.py logs/set16_top/maarten-denayer_August31_18_20_37_microyolo/checkpoints/best.state.pt -n logs/set16_top/maarten-denayer_August31_18_20_37_microyolo/microyolo.py --bg_builder --output notebooks/detections/set16_top_maarten-denayer_August31_18_20_37_microyolo.parquet
python bin/test.py logs/set16_top/maarten-denayer_September06_18_07_26_microyolo_2/checkpoints/best.state.pt -n logs/set16_top/maarten-denayer_September06_18_07_26_microyolo_2/microyolo_2.py --bg_builder --output notebooks/detections/set16_top_maarten-denayer_September06_18_07_26_microyolo_2.parquet

# 45-degree PR-curve
python bin/test.py logs/set05/maarten-denayer_September10_03_43_59_microyolo_all_1/checkpoints/best.state.pt -n logs/set05/maarten-denayer_September10_03_43_59_microyolo_all_1/microyolo_all_1.py --output notebooks/detections/set05_maarten-denayer_September10_03_43_59_microyolo_all_1.parquet
python bin/test.py logs/set05/maarten-denayer_September13_21_11_49_microyolo_all_4/checkpoints/best.state.pt -n logs/set05/maarten-denayer_September13_21_11_49_microyolo_all_4/microyolo_all_4.py --output notebooks/detections/set05_maarten-denayer_September13_21_11_49_microyolo_all_4.parquet
python bin/test.py logs/set05/maarten-denayer_September10_08_31_18_microyolo_all_2/checkpoints/best.state.pt -n logs/set05/maarten-denayer_September10_08_31_18_microyolo_all_2/microyolo_all_2.py --bg_builder --output notebooks/detections/set05_maarten-denayer_September10_08_31_18_microyolo_all_2.parquet
python bin/test.py logs/set05/maarten-denayer_September09_18_26_13_microyolo_all_3/checkpoints/best.state.pt -n logs/set05/maarten-denayer_September09_18_26_13_microyolo_all_3/microyolo_all_3.py --bg_builder --output notebooks/detections/set05_maarten-denayer_September09_18_26_13_microyolo_all_3.parquet

# 90-degree compression
python bin/test.py logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/checkpoints/pruned-49.pt -n logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/microyolo_l2_prune_90.py --bg_builder --output notebooks/detections/set06_prune_maarten-denayer_September17_20_48_43_microyolo_l2_prune_90_pruned-49.parquet
python bin/test.py logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/checkpoints/pruned-49.tflite -n logs/set06_prune/maarten-denayer_September17_20_48_43_microyolo_l2_prune_90/microyolo_l2_prune_90.py --bg_builder --output notebooks/detections/set06_prune_maarten-denayer_September17_20_48_43_microyolo_l2_prune_90_pruned-49_quant.parquet

# 45-degree compression
python bin/test.py logs/set06_prune/maarten-denayer_September19_11_21_11_microyolo_l2_prune/checkpoints/pruned-44.pt -n logs/set06_prune/maarten-denayer_September19_11_21_11_microyolo_l2_prune/microyolo_l2_prune.py --bg_builder --output notebooks/detections/set06_prune_maarten-denayer_September19_11_21_11_microyolo_l2_prune_pruned-44.parquet
python bin/test.py logs/set06_prune/maarten-denayer_September19_11_21_11_microyolo_l2_prune/checkpoints/pruned-44.tflite -n logs/set06_prune/maarten-denayer_September19_11_21_11_microyolo_l2_prune/microyolo_l2_prune.py --bg_builder --output notebooks/detections/set06_prune_maarten-denayer_September19_11_21_11_microyolo_l2_prune_pruned-44_quant.parquet
