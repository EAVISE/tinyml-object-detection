#
#   Darknet Tiny YOLOv2 model
#   Copyright EAVISE
#

import functools
import logging
from collections import OrderedDict, Iterable
import torch.nn as nn
import lightnet.network as lnn
from .darknet_micro import darknet_micro, darknet_mobile_micro

__all__ = ['MicroYolo']
log = logging.getLogger('lightnet.models')


class MicroYolo(lnn.module.Lightnet):
    """ Micro YOLO
    """
    def __init__(self, motion_output=False, input_channels=3, anchors=[(1.08, 1.19), (3.42, 4.41), (6.63, 11.38), (9.42, 5.11), (16.62, 10.52)],
                 backbone='darknet_micro', backbone_width=1.0, stride=4, pretrained_backbone=True):
        super().__init__()
        if not isinstance(anchors, Iterable) and not isinstance(anchors[0], Iterable):
            raise TypeError('Anchors need to be a 2D list of numbers')

        # Parameters
        self.input_channels = input_channels
        self.anchors = anchors
        self.stride = stride
        self.inner_stride = stride

        # Backbone
        if backbone == 'darknet_micro':
            backbone_model = darknet_micro(pretrained_backbone, input_channels=input_channels, width=backbone_width, stride=self.stride)
            self.features = backbone_model.features
        elif backbone == 'darknet_mobile_micro':
            backbone_model = darknet_mobile_micro(pretrained_backbone, input_channels=input_channels, width=backbone_width, stride=self.stride)
            self.features = backbone_model.features
        else:
            raise ValueError(f'Backbone "{backbone}" unknown')

        feature_channels = backbone_model.feature_channels
        print("Feature channels", feature_channels)

        # Regressor
        num_motion_channels = 2 if motion_output else 0
        self.regressor = nn.Sequential(nn.Conv2d(feature_channels, len(self.anchors)*(5+num_motion_channels), kernel_size=1))

    def forward(self, x):
        x = self.features(x)
        return self.regressor(x)
