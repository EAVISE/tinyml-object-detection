import torch
import torch.nn as nn
import lightnet.network as lnn
import functools

from .utils import adapt_network_input_channels

class DarknetMicro(nn.Module):
    def __init__(self, input_channels=3, width=1.0, mobile=False, stride=4, **kwargs):
        super(DarknetMicro, self).__init__()
        #relu = functools.partial(nn.LeakyReLU, 0.125, inplace=True)
        relu = functools.partial(nn.ReLU6, inplace=True)
        if stride == 4:
            maxpool_2 = nn.MaxPool2d(2, 2)
        elif stride == 2:
            maxpool_2 = nn.Identity()
        else:
            raise ValueError("Only strides 4, 2 and 1 supported")

        if mobile:
            self.features = nn.Sequential(
                lnn.layer.Conv2dBatchReLU(input_channels, int(width*16), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*16),  int(width*32), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*32),  int(width*64), 3, 2, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*64),  int(width*128), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*128), int(width*256), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*256), int(width*512), 3, 2, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*512), int(width*1024), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dDepthWise(int(width*1024), int(width*512), 3, 1, 1, relu=relu),
            )
        else:
            self.features = nn.Sequential(
                lnn.layer.Conv2dBatchReLU(input_channels, int(width*16), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dBatchReLU(int(width*16),  int(width*32), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dBatchReLU(int(width*32),  int(width*64), 3, 1, 1, relu=relu),
                nn.MaxPool2d(2, 2),
                lnn.layer.Conv2dBatchReLU(int(width*64),  int(width*128), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dBatchReLU(int(width*128), int(width*256), 3, 1, 1, relu=relu),
                maxpool_2,
                lnn.layer.Conv2dBatchReLU(int(width*256), int(width*512), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dBatchReLU(int(width*512), int(width*1024), 3, 1, 1, relu=relu),
                lnn.layer.Conv2dBatchReLU(int(width*1024), int(width*512), 3, 1, 1, relu=relu),
            )
        self.feature_channels = int(width*512)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.classifier = nn.Sequential(
                nn.Dropout(p=0.5),
                nn.Linear(self.feature_channels, 10))

    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        x = self.classifier(torch.flatten(x, 1))
        return x


def load_pretrained_model(model, init_weights_file, average_pretrained_input_weights=True):

    print("Loading", init_weights_file)
    pretrained_weights = torch.load(init_weights_file)
    adapt_network_input_channels(pretrained_weights, 'features.0.layers.0.weight', model.features[0].layers[0].in_channels,
                                 average_pretrained_input_weights)
    model.load_state_dict(pretrained_weights, strict=False)

    return model


def darknet_micro(pretrained=False, **kwargs):
    model = DarknetMicro(**kwargs)
    if pretrained:
        width = 1.0
        if 'width' in kwargs:
            width = kwargs['width']
        weight_file = f"model_data/backbone/darknet_micro_{str(width).replace('.', '')}_init.pt"
        load_pretrained_model(model, weight_file)
    return model


def darknet_micro_10(pretrained=False, **kwargs):
    return darknet_micro(pretrained, width=1.0, **kwargs)


def darknet_micro_05(pretrained=False, **kwargs):
    return darknet_micro(pretrained, width=0.5, **kwargs)


def darknet_micro_025(pretrained=False, **kwargs):
    return darknet_micro(pretrained, width=0.25, **kwargs)


def darknet_mobile_micro(pretrained=False, **kwargs):
    model = DarknetMicro(mobile=True, **kwargs)
    if pretrained:
        width = 1.0
        if 'width' in kwargs:
            width = kwargs['width']
        weight_file = f"model_data/backbone/darknet_mobile_micro_{str(width).replace('.', '')}_init.pt"
        load_pretrained_model(model, weight_file)
    return model


def darknet_mobile_micro_10(pretrained=False, **kwargs):
    return darknet_mobile_micro(pretrained, width=1.0, **kwargs)


def darknet_mobile_micro_05(pretrained=False, **kwargs):
    return darknet_mobile_micro(pretrained, width=0.5, **kwargs)


def darknet_mobile_micro_025(pretrained=False, **kwargs):
    return darknet_mobile_micro(pretrained, width=0.25, **kwargs)


def test():
    net = darknet_micro_05()
    x = torch.randn(2,3,24,32)
    print(net)
    y = net(x)
    print(y.size())

if __name__ == '__main__':
    test()
