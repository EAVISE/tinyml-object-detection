import torch
import sys

# from CIFAR10 training, create backbone init

weights_file_in = sys.argv[1]
weights_file_out = sys.argv[2]

sd = torch.load(weights_file_in)
sd = sd['net']

extraction_weights = {}
for k,v in sd.items():
    k = k.replace('module.', '')
    extraction_weights[k] = v

torch.save(extraction_weights, weights_file_out)
