import torch
from torch.utils.data.dataloader import default_collate
import ast
import pandas as pd
import brambox as bb
from tqdm import tqdm
import cv2
import numpy as np
import json
import xml.etree.ElementTree as ET
import socket
import shutil
import logging
from datetime import datetime
import collections
import os
from matplotlib import pyplot as plt

import tensorflow as tf


class SuperviselyParser(bb.io.parser.AnnotationParser):
    parser_type = bb.io.parser.ParserType.MULTI_FILE
    extension = '.json'
    image_height = 24
    image_width = 32

    # We want to serialize per image, as this is how the files are stored
    serialize_group = 'image'

    def deserialize(self, rawdata, file_id):
        """ parse raw string data -> dataframe
            Since this is a multifile format, we get the file_id (name of file without extension) as an argument.
        """
        data = json.loads(rawdata)

        # Add image (for files without boxes, like img03)
        image_id = file_id.replace('label', 'image')
        self.append_image(image_id)

        for bbox in data['objects']:
            # Add bounding box
            x_top_left = bbox['points']['exterior'][0][0]
            y_top_left = bbox['points']['exterior'][0][1]
            x_bottom_right = bbox['points']['exterior'][1][0]
            y_bottom_right = bbox['points']['exterior'][1][1]

            self.append(
                # image
                image_id,

                # properties
                class_label=bbox['classTitle'],
                x_top_left=float(x_top_left),
                y_top_left=float(y_top_left),
                width=float(x_bottom_right - x_top_left),
                height=float(y_bottom_right - y_top_left),
            )

    def serialize(self, df):
        """ parse dataframe -> string
            We set `serialize_group` property to 'image', so here we get a (sub-)dataframe
            with the elements from one certain image (this function gets called as `groupby().apply()`)
        """
        data = {"description": "", "size": {"height": image_height, "width": image_width}}

        def get_id():
            return uuid.uuid1().int

        def add_tag(name):
            return {"id": get_id(),
                    "tagId": int.from_bytes(name.encode(), 'little'),
                    "name": name,
                    "value": None,
                    }

        tags = []
        if 'train' in df.columns and True in df['train'].values:
             tags += [add_tag("train")]
        if 'val' in df.columns and True in df['val'].values:
             tags += [add_tag("val")]

        data["tags"] = tags

        objects = []
        for i, row in enumerate(df.itertuples()):

            objects += [{
                    "id": get_id(),
                    "classId": int.from_bytes(row.class_label.encode(), 'little'),
                    "description": "",
                    "geometryType": "rectangle",
                    "tags":[],
                    "classTitle": row.class_label,
                    "points": {
                        "exterior": [[round(row.x_top_left), round(row.y_top_left)],
                                    [round(row.x_top_left + row.width), round(row.y_top_left + row.height)]],
                        "interior": []},
                    }]

        data["objects"] = objects
        return json.dumps(data)


class BUTIVParser(bb.io.parser.AnnotationParser):
    """For parsing annotations from http://csr.bu.edu/BU-TIV/BUTIV.html
    """
    parser_type = bb.io.parser.ParserType.SINGLE_FILE
    extension = '.xml'

    def deserialize(self, rawdata, file_id=None):

        dataset = ET.fromstring(rawdata)
        # access each 'frame'
        for frame in dataset:
            frame_nr = int(frame.attrib['number'])
            image_id = "frame_%05d"%frame_nr
            # ensure the image is created evenn if no annotations
            self.append_image(image_id)
            # access each bbox in frame 'objectlist'
            for obj in frame[0]:
                obj_id = int(obj.attrib['id'])
                x1 = float(obj.attrib['x1'])
                x2 = float(obj.attrib['x2'])
                y1 = float(obj.attrib['y1'])
                y2 = float(obj.attrib['y2'])
                width = x2 - x1
                height = y2 - y1

                self.append(
                    image_id,
                    id=obj_id,
                    class_label='person',
                    x_top_left=x1,
                    y_top_left=y1,
                    width=width,
                    height=height,
                )


class MlDetectionParser(bb.io.parser.AnnotationParser):
    parser_type = bb.io.parser.ParserType.MULTI_FILE
    extension = '.txt'

    # We want to serialize per image, as this is how the files are stored
    serialize_group = 'image'

    def __init__(self):
        super().__init__()

        self.add_column('motion', 0.0)

    def deserialize(self, rawdata, file_id):
        """ parse raw string data -> dataframe
            Since this is a multifile format, we get the file_id (name of file without extension) as an argument.
        """
        # Add image (for files without boxes, like img03)
        #image_id = file_id.replace('label', 'image')
        image_id = file_id
        self.append_image(image_id)

        for bbox in ast.literal_eval(rawdata):
            obj_id = float("nan")
            if 'id' in bbox and float(bbox['id']) >= 0:
                obj_id = float(bbox['id'])

            motion = 0.0
            if 'motion' in bbox:
                motion = float(bbox['motion'])

            # Add bounding box
            self.append(
                # image
                image_id,
                id=obj_id,

                # properties
                class_label=bbox['label'],
                x_top_left=float(bbox['topleft']['x']),
                y_top_left=float(bbox['topleft']['y']),
                width=float(bbox['bottomright']['x'] - bbox['topleft']['x']),
                height=float(bbox['bottomright']['y'] - bbox['topleft']['y']),
                motion=motion,
            )

    def serialize(self, df):
        """ parse dataframe -> string
            We set `serialize_group` property to 'image', so here we get a (sub-)dataframe
            with the elements from one certain image (this function gets called as `groupby().apply()`)
        """
        result = []

        for row in df.itertuples():
            obj_id = -1
            if not np.isnan(row.id):
                obj_id = int(row.id)

            if np.isnan(row.motion):
                print(row)

            result += [{'label': row.class_label,
                        'id': obj_id,
                        'motion': round(row.motion),
                        'topleft': {'x': round(row.x_top_left), 'y': round(row.y_top_left)},
                        'bottomright': {'x': round(row.x_top_left + row.width), 'y': round(row.y_top_left + row.height)},
                        }]

        return str(result)


class ExponentialMovingAverage:
    """Keep track of the EMA of a single number/Tensor
    """
    def __init__(self, decay=0.999):

        self.decay = decay
        self.biased_ema = 0
        self.step = 0

    def __call__(self, value):
        """Exponential moving average calculation
        """
        self.step += 1
        self.biased_ema = self.biased_ema * self.decay + (1 - self.decay) * value
        unbiased_ema = self.biased_ema / (1 - self.decay ** self.step)  # Bias correction
        return unbiased_ema


class BackgroundImageBuilder:
    """Constructs a background image from input images and detections
    """

    def __init__(self, init_steps=3, update_interval=10, threshold=0.3, ema_decay=0.99, box_margin=1):
        """
        init_steps (int):       How many images are needed to get a valid initialization of the background
        update_interval (int):  How frequently the background in updated in number of calls to the forward method
        threshold (float):      Detection threshold. All boxes equal or above this threshold are used in the background construction
        ema_decay (float):      Decay parameter for the Exponential Moving Average filter that updates the background
        box_margin (int):       Number of pixels to enlarge a bbox on each side before creating cutouts.
        """
        self.init_steps = init_steps
        self.update_interval = update_interval
        self.threshold = threshold
        self.ema_decay = ema_decay
        self.box_margin = box_margin
        self.ema = None
        self.steps = None
        self.clip_id = ''
        self.latest_background = None

    def __call__(self, image, detections):
        """
        image (torch.tensor):       Input image to use for updating the background (expected shape is 1 x H x W)
        detections (pd.DataFrame):  Detections from the model on the given input image
        Return 1 x H x W background image. During initialization, None is returned
        """
        clip_id = os.path.dirname(detections.image.cat.categories[0])
        if clip_id != self.clip_id:
            # first call or new clip, rebuild the background from scratch
            self.clip_id = clip_id
            self.ema = ExponentialMovingAverage(self.ema_decay)
            self.steps = 1

        if self.ema.step < self.init_steps:
            # Initialization phase
            self.latest_background = self.ema(image)
            return None

        if (self.steps % self.update_interval) == 0:
            # Update phase
            # cut out detection-box areas in the image and fill them with values from the current background
            detections = detections[detections.confidence >= self.threshold]
            mask = create_bbox_mask(image, detections, self.box_margin)
            image = image * mask + self.latest_background * (1 - mask)
            self.latest_background = self.ema(image)

        self.steps += 1
        return self.latest_background


class TfLiteModelRunner(torch.nn.Module):
    """Wrapper module that infers a tflite model as if it was a pytorch model. This is usefull for testing
    the accuracy of tflite quantized models
    """
    def __init__(self, model_file):
        """
        """
        super().__init__()

        self.model = tf.lite.Interpreter(model_file)
        self.model.allocate_tensors()
        self.input_details = self.model.get_input_details()
        self.output_details = self.model.get_output_details()
        self.quant_input = self.input_details[0]['dtype'] == np.int8
        self.quant_output = self.input_details[0]['dtype'] == np.int8

    def forward(self, data):
        """Forward the model
        x (torch.tensor):   NCHW
        """
        # simulate a batched model
        out = []
        for x in data:
            # CHW to 1CHW
            x = x.unsqueeze(0)

            # NCHW to NHWC and to numpy
            x = x.permute(0, 2, 3, 1).numpy()

            # quantize input
            if self.quant_input:
                q_scale = self.input_details[0]['quantization'][0]
                q_zero = self.input_details[0]['quantization'][1]
                x = (x / q_scale + q_zero).astype(np.int8)

            self.model.set_tensor(self.input_details[0]['index'], x)
            self.model.invoke()
            y = self.model.get_tensor(self.output_details[0]['index'])

            # dequantize output
            if self.quant_output:
                q_scale = self.output_details[0]['quantization'][0]
                q_zero = self.output_details[0]['quantization'][1]
                y = (y.astype(np.int32) - q_zero) * q_scale

            # NHWC to NCHW and to torch tensor
            y = torch.from_numpy(y).permute(0, 3, 1, 2).contiguous()
            out.append(y)

        y = torch.cat(out, dim=0)
        return y


# We copied the this function from lightnet
# The only modification is that `sort_image_categories=False` is added to fix a bug
def brambox_collate(batch):
    """ Function that collates dataframes by concatenating them.

    Note:
        If the dataframes contain an 'image' categorical column (aka. brambox dataframes),
        they will be concatenated with the :func:`brambox.util.concat` function.
    """
    if isinstance(batch[0], pd.DataFrame):
        for i, df in enumerate(batch):
            df['batch_number'] = i
        if 'image' in batch[0].columns and batch[0].image.dtype == 'category':
            return bb.util.concat(batch, sort_image_categories=False, ignore_index=True, sort=False)
        else:
            return pd.concat(batch, ignore_index=True, sort=False)
    elif isinstance(batch[0], collections.abc.Sequence) and not isinstance(batch[0], (str, bytes)):
        transposed = zip(*batch)
        return [brambox_collate(samples) for samples in transposed]
    else:
        return default_collate(batch)


def get_anchors(key):
    """Return an anchor set based on a key value
    """
    anchors = {'both':  [(8.853734918993446, 12.044021027232077),
                         (7.7503849920115115, 6.525571303406621),
                         (4.530664042752944, 7.975254206687415),
                         (4.25046050720126, 5.26986904910139),
                         (2.5676662786561533, 3.5365033789243747)],

               '90':    [(12.37206703910614, 12.145251396648053),
                         (8.439821566440761, 8.173372222698827),
                         (8.098631653879599, 5.3617021276595676),
                         (6.031671690238817, 6.248736378390914),
                         (3.888849888849902, 3.5401895401895542)],

               '45':    [(8.011713819368874, 12.705340043525588),
                         (7.480296486518036, 6.964729777051475),
                         (4.286825815305717, 7.6890443858572),
                         (3.7505217792827437, 4.621673079883025),
                         (2.1262839441838928, 3.4783492559782285)]
               }
    try:
        return anchors[key]
    except KeyError:
        raise ValueError("Do not know anchor set {key}, choose from {anchors.keys()}")


def create_bbox_mask(img, boxes, margin=1):
    """create a binary mask from bboxes: 0 inside the area of a bbox, 1 elsewhere
    img (np.array|torch.tensor):    Either HxW for np.array or 1x1xHxW for torch.tensor
    boxes:                          Boxes dataframe
    Returns a HxW numpy array or float torch tensor with zeros and ones. Dtypes and/or devices
    are the same as given img
    """
    boxes = boxes.copy()

    # enlarge bbox areas by margin
    boxes.loc[:, 'x_top_left'] -= margin
    boxes.loc[:, 'y_top_left'] -= margin
    boxes.loc[:, 'width'] += 2*margin
    boxes.loc[:, 'height'] += 2*margin

    # calculate bottom right coordinates
    img_h, img_w = img.shape[-2:]

    boxes['x_bottom_right'] = boxes.x_top_left + boxes.width
    boxes['y_bottom_right'] = boxes.y_top_left + boxes.height

    # ensure boxes are within image bounds
    boxes.loc[:, 'x_top_left'].clip(0, img_w, inplace=True)
    boxes.loc[:, 'y_top_left'].clip(0, img_h, inplace=True)
    boxes.loc[:, 'x_bottom_right'].clip(0, img_w, inplace=True)
    boxes.loc[:, 'y_bottom_right'].clip(0, img_h, inplace=True)

    if isinstance(img, np.ndarray):
        mask = np.ones_like(img)
    else:
        mask = img.new_ones((img_h, img_w))

    for _, row in boxes.iterrows():
        mask[int(row.y_top_left):int(row.y_bottom_right), int(row.x_top_left):int(row.x_bottom_right)] = 0

    return mask


def create_heatmap(img, boxes):
    """Create a heatmap from boxes.
    img:        image reference to base heatmap size, type and device on
                expected dims: B x C x H x W or C x H x W
    boxes:      Boxes dataframe
    """
    # calculate bottom right coordinates
    img_h, img_w = img.shape[-2:]
    boxes['x_bottom_right'] = boxes.x_top_left + boxes.width
    boxes['y_bottom_right'] = boxes.y_top_left + boxes.height

    # ensure boxes are within image bounds
    boxes['x_top_left'] = boxes['x_top_left'].clip(0, img_w)
    boxes['y_top_left'] = boxes['y_top_left'].clip(0, img_h)
    boxes['x_bottom_right'] = boxes['x_bottom_right'].clip(0, img_w)
    boxes['y_bottom_right'] = boxes['y_bottom_right'].clip(0, img_h)

    # ensure confidence columns is present
    if 'confidence' not in boxes.columns:
        boxes['confidence'] = 1.0

    # create a heatmap that contains the confidence in areas of bboxes.
    if img.dim() == 4:
        heatmap = img.new_zeros([img.shape[0]] + [1, img_h, img_w]) # B x 1 x H x W
    elif img.dim() == 3:
        heatmap = img.new_zeros([1, 1, img_h, img_w])               # 1 x 1 x H x W
    else:
        raise ValueError("Expected input image to have either 3 or 4 dims")

    for i, (_, per_image_boxes) in enumerate(boxes.groupby('image', sort=False)):
        per_image_boxes = per_image_boxes.sort_values(by=['confidence'])
        for _, row in per_image_boxes.iterrows():
            heatmap[i, 0, int(row.y_top_left):int(row.y_bottom_right), int(row.x_top_left):int(row.x_bottom_right)] = row.confidence

    if img.dim() == 3:
        heatmap = heatmap.squeeze(0)

    return heatmap


def create_gaussian_heatmap(img, boxes, alpha=1/4):
    """Create a gaussian heatmap from boxes.
    img:        image reference to base heatmap size, type and device on
                expected dims: B x C x H x W or C x H x W
    boxes:      Boxes dataframe
    alpha:      Gaussian standard deviation scale. A large scale will result in wider 2D gaussians
    """
    # ensure boxes are within image bounds
    img_h, img_w = img.shape[-2:]
    boxes['x_bottom_right'] = boxes.x_top_left + boxes.width
    boxes['y_bottom_right'] = boxes.y_top_left + boxes.height
    boxes['x_top_left'] = boxes['x_top_left'].clip(0, img_w)
    boxes['y_top_left'] = boxes['y_top_left'].clip(0, img_h)
    boxes['x_bottom_right'] = boxes['x_bottom_right'].clip(0, img_w)
    boxes['y_bottom_right'] = boxes['y_bottom_right'].clip(0, img_h)

    # calculate center coordinates of each box
    boxes['x_center'] = boxes.x_top_left + boxes.width / 2
    boxes['y_center'] = boxes.y_top_left + boxes.height / 2

    # ensure confidence columns is present
    if 'confidence' not in boxes.columns:
        boxes['confidence'] = 1.0

    # create a heatmap that contains the confidence in areas of bboxes.
    if img.dim() == 4:
        heatmap = img.new_zeros([img.shape[0]] + [1, img_h, img_w]) # B x 1 x H x W
    elif img.dim() == 3:
        heatmap = img.new_zeros([1, 1, img_h, img_w])               # 1 x 1 x H x W
    else:
        raise ValueError("Expected input image to have either 3 or 4 dims")

    # create x and y mesh grids which serve as linear inputs for the gaussian function
    y = torch.linspace(0, img_h, img_h, device=img.device)
    x = torch.linspace(0, img_w, img_w, device=img.device)
    y, x = torch.meshgrid(y, x)

    def gaussian_2d(x, y, mx, my, sx, sy, conf):
        eps = 1e-9
        # note that we skip calculating the constant term of the gaussian, since we rescale it below anyway
        h = torch.exp(-((x - mx)**2 / (2*sx**2 + eps) + (y - my)**2 / (2*sy**2 + eps)))
        h[h < 0.001 * h.max()] = 0   # truncate gaussian when below certain value
        h *= conf / (h.max() + eps)  # rescale the values to make sure that the peak of the gaussian equals the box confidence
        return h

    for i, (_, per_image_boxes) in enumerate(boxes.groupby('image', sort=False)):
        per_image_boxes = per_image_boxes.sort_values(by=['confidence'])
        for _, row in per_image_boxes.iterrows():
            heatmap[i, 0, ...] += gaussian_2d(x, y, row.x_center, row.y_center,
                                              row.width*alpha, row.height*alpha,
                                              row.confidence)

    if img.dim() == 3:
        heatmap = heatmap.squeeze(0)

    return heatmap


def test_detector(network, dataloader, device, post=None, criteria=None, sigint_ref=None, progress_bar=True, set_eval=True, return_input_data=False, background_builder=None):
    """Test object detector network
    Returns detection dataframe, annotation dataframe and tuple with loss values: (loss_tot, loss_coord, loss_conf, loss_class)
    If criteria is not provided, the loss values are None
    """
    # Ensure network and criteria are in eval mode
    if set_eval:
        network.eval()
        if criteria is not None:
            criteria.eval()

    loss_dict = {}
    anno, det = [], []
    input_data = []
    background = None

    with torch.no_grad():
        for idx, (data, target) in enumerate(tqdm(dataloader) if progress_bar else dataloader):
            data = data.to(device)

            model_input = data
            if background is not None:
                # apply background subtraction if background in available
                background = background.expand(data.shape[0], 1, *data.shape[2:])
                model_input = data.clone()
                model_input[:, 0,...] = data[:, 0,...] - background[:, 0,...]

            # model forward
            output = network(model_input)

            # visualization if requested
            if return_input_data:
                vis_data = model_input
                if background_builder is not None:
                    vis_background = background # B x H x W or None
                    if vis_background is None:
                        s = list(vis_data.shape)
                        s[1] = 1
                        vis_background = vis_data.new_zeros(s)
                    vis_data = torch.cat([model_input, vis_background], axis=1)
                input_data.append(vis_data.cpu())

            # combine batch and temporal dims if temporal dim present
            if output.dim() == 5:
                # (B x T x C x H x W) -> (B*T x C x H x W)
                output = squash_dims(output, (0, 1))
                # combine batch and frame numbers on target too
                seq_length = data.size(1)
                target['batch_number'] = target['frame_number'] + target['batch_number'] * seq_length

            # calculate losses
            if criteria is not None:
                criteria(output, target)
                num_img = output.shape[0]
                for name, value in criteria.values.items():
                    if torch.is_tensor(value):
                        value = value.item()
                    value *= num_img

                    if name not in loss_dict:
                        loss_dict[name] = [value]
                    else:
                        loss_dict[name] += [value]

            # do post processing
            if post is not None:
                output = post(output)
                output.image = pd.Categorical.from_codes(output.image, categories=target.image.cat.categories)

                if background_builder is not None:
                    # remove all detections if background was not yet initialized, since these detection are likely wrong
                    if background is None:
                        output = output[0:0]

                    # run background builder for all images in the batch
                    for _data, image_id in zip(data, output.image.cat.categories):
                        background = background_builder(_data[0], bb.util.select_images(output, [image_id]))

                det.append(output)

            anno.append(target)

            # Condition to early return from test loop
            if sigint_ref is not None and sigint_ref.sigint:
                return None, None, None

    anno = bb.util.concat(anno, ignore_index=True, sort=False)
    det = bb.util.concat(det, ignore_index=True, sort=False)

    loss_final = {}
    if criteria is not None:
        for name, values in loss_dict.items():
            loss_final[name] = sum(values) / len(anno.image.cat.categories)

    if len(input_data) != 0:
        return anno, det, loss_final, torch.cat(input_data, dim=0)

    return anno, det, loss_final


# TODO: clean duplicate code
def test_video_detector(network, dataloader, device, post=None, criteria=None, sigint_ref=None, progress_bar=True, set_eval=True, reset_mem=True,
                        return_input_data=False):
    """Test video object detector network
    Returns detection dataframe, annotation dataframe and tuple with loss values: (loss_tot, loss_coord, loss_conf, loss_class)
    If criteria is not provided, the loss values are None
    """
    # Ensure network and criteria are in eval mode
    if set_eval:
        network.eval()
        if criteria is not None:
            criteria.eval()

    loss_dict = {'tot': [], 'coord': [], 'conf': [], 'class': []}
    anno, det = [], []
    input_data = []

    with torch.no_grad():
        for data, target in tqdm(dataloader) if progress_bar else dataloader:
            assert data.dim() == 5, "Expecting video data"
            # (B x T x C x H x W) -> (T x B x C x H x W)
            data = data.permute(1, 0, 2, 3, 4)
            if hasattr(network, 'heatmap_memory') and not network.post[-1].initialized:
                network.post[-1].init(data[1].shape[0])

            for i, data_ in enumerate(data):
                img_ids = target.image.cat.categories[i::data.size(0)]
                target_ = bb.util.select_images(target, img_ids)
                data_ = data_.to(device)
                #if i == 0 and hasattr(network, 'heatmap_memory'):
                #    network.heatmap_memory = None
                #    network.post[-1].init(data_.shape[0], target_)   # init tracker
                output = network(data_)

                if return_input_data:
                    input_data.append(data_.cpu())

                if criteria is not None:
                    criteria(output, target_)
                    num_img = output.shape[0]
                    loss_dict['tot'].append(criteria.loss_total.item() * num_img)
                    loss_dict['coord'].append(criteria.loss_coord.item() * num_img)
                    loss_dict['conf'].append(criteria.loss_conf.item() * num_img)

                if post is not None:
                    output = post(output)
                    output.image = pd.Categorical.from_codes(output.image, categories=target_.image.cat.categories)
                    det.append(output)
                    anno.append(target_)

                # Condition to early return from test loop
                if sigint_ref is not None and sigint_ref.sigint:
                    return None, None, None

    anno = bb.util.concat(anno, ignore_index=True, sort=False)
    det = bb.util.concat(det, ignore_index=True, sort=False)

    loss_tot = loss_coord = loss_conf = loss_class = None
    if criteria is not None:
        loss_tot = sum(loss_dict['tot']) / len(anno.image.cat.categories)
        loss_coord = sum(loss_dict['coord']) / len(anno.image.cat.categories)
        loss_conf = sum(loss_dict['conf']) / len(anno.image.cat.categories)
        loss_class = sum(loss_dict['class']) / len(anno.image.cat.categories)

    if len(input_data) != 0:
        return anno, det, (loss_tot, loss_coord, loss_conf, loss_class), torch.cat(input_data, dim=0)

    return anno, det, (loss_tot, loss_coord, loss_conf, loss_class)


def calculate_ap(anno, det, iou=0.5, plot_pr=False):
    """Calculate AP metric with given iou
    returns AP matric in % (x100)
    """
    pr = bb.stat.pr(det, anno, iou)
    ap = bb.stat.ap(pr)
    if plot_pr:
        ax = pr.plot('recall', 'precision', label=f'AP = {round(100*ap, 2)}%')
        ax.set_xlim([0,1])
        ax.set_ylim([0,1])
        ax.set_xlabel('Recall')
        ax.set_ylabel('Precision')
        plt.show()

    return ap * 100


def find_optimal_working_point(anno, det, match_criteria_thresh=0.5, match_criteria=bb.stat.coordinates.ioa, plot_pr=False):
    """Calculate PR-curve and find optimal working point.
    Function returns the Average precision, the optimal threshold, the precision and recall at this threshold and the F1-score for this working point
    """
    # Compute TP,FP
    tpfp_det = bb.stat.match_det(det, anno, match_criteria_thresh, criteria=match_criteria)

    # calculate PR-curve
    pr = bb.stat.pr(tpfp_det, anno, match_criteria_thresh)
    ap = bb.stat.ap(pr)

    # Find detection threshold with maximal F1
    f1 = bb.stat.fscore(pr)
    threshold = bb.stat.peak(f1)

    # Find point on PR-curve that matches the computed detection threshold
    pr_point = bb.stat.point(pr, threshold.confidence)

    if plot_pr:

        # Plot
        ax = pr.plot('recall', 'precision', label=f'AP = {round(100*ap, 2)}%')
        plt.scatter(pr_point.recall, pr_point.precision, s=50)
        f1.plot('recall', 'f1', label='F1', ax=ax)
        plt.scatter(threshold.recall, threshold.f1, s=50)

        ax.set_xlim([0,1])
        ax.set_ylim([0,1])
        ax.set_xlabel('Recall')
        ax.set_ylabel('Precision / F1')
        plt.show()

    return ap, threshold.confidence, pr_point.precision, pr_point.recall, threshold.f1


def test_point_detector(network, dataloader, device, post=None, criteria=None, sigint_ref=None, progress_bar=True, set_eval=True):
    """Test object point detector network and return mAP scores and
    the test loss value if criteria is provided
    """
    # Ensure network and criteria are in eval mode
    if set_eval:
        network.eval()
        if criteria is not None:
            criteria.eval()

    loss_dict = {'heatmap': []}
    counter = 0

    with torch.no_grad():
        for idx, (data, target) in enumerate(tqdm(dataloader) if progress_bar else dataloader):
            data = data.to(device)
            #target = target.to(device, dtype=torch.long)

            output = network(data)

            # combine batch and temporal dims if temporal dim present
            if output.dim() == 5:
                # (B x T x C x H x W) -> (B*T x C x H x W)
                output = squash_dims(output, (0, 1))
                # combine batch and frame numbers on target too
                seq_length = data.size(1)
                target['batch_number'] = target['frame_number'] + target['batch_number'] * seq_length

            if criteria is not None:
                criteria(output, target)
                num_img = output.shape[0]
                loss_dict['heatmap'].append(criteria.loss_heatmap.item() * num_img)

            if post is not None:
                # TODO
                pass
            else:
                counter += data.size(0)

            # Condition to early return from test loop
            if sigint_ref is not None and sigint_ref.sigint:
                return None, None, None, None

    loss_heatmap = None
    if criteria is not None:
        loss_heatmap = sum(loss_dict['heatmap']) / counter

    return loss_heatmap


def adapt_network_input_channels(pretrained_params, input_layer_key, num_channels, average_input_weights=False):
    """
    If average_input_weights == True: num_channels can be any positive number
    else: num_channels must be a multiple of the number of pretrained input channels
    """

    input_weights = pretrained_params[input_layer_key]

    if average_input_weights:
        input_weights = torch.mean(input_weights, dim=1)
        repeat_count = num_channels
    else:
        num_pretrained_channels = input_weights.size(1)
        if num_channels % num_pretrained_channels != 0:
            raise RuntimeError("If average_input_weights is False, expecting num_channels to be a multiple " +
                                "of {}, got {}".format(num_pretrained_channels, num_channels))
        repeat_count = num_channels // num_pretrained_channels

    pretrained_params[input_layer_key] = input_weights.unsqueeze(1).repeat(1, repeat_count, 1, 1)


def apply_agc(img):
    # calc histogram
    bincount = 2**14
    hist = cv2.calcHist([img], [0], None, [bincount], [0,bincount])

    min_value = np.min(img)
    max_value = np.max(img)

    # TODO: not sure how to implement max gain parameter

    hist = hist[min_value:max_value+1]

    # apply plateau threshold
    plateau = 0.07
    try:
        plateau = np.max(hist) * plateau
    except ValueError:
        print("Found frame with only 1 grayscale value, skipping to next video")
        return None

    hist[hist > plateau] = plateau

    # calculate pdf
    pdf = hist / np.sum(hist)

    # get cumulative density function (CDF) from Plateau HEQ
    cdf_pheq = np.cumsum(pdf)
    #print(cdf_pheq)
    # calc linear CDF
    cdf_lin = np.linspace(0, 1, len(pdf))

    # apply linear percentage
    linear_percent = 0.2
    cdf = linear_percent * cdf_lin + (1 - linear_percent) * cdf_pheq

    # apply ACE
    ace = 0.8
    cdf_ace = np.power(cdf, 1/ace)

    # remap pixels based on CDF
    out_img = 255 * cdf_ace[img - min_value]

    return out_img.astype(np.uint8)


def apply_agc_linear(img, min_temp=17, max_temp=40):
    """Linear AGC between fixed temperature range
    img:    uint16 or int16 image
    """
    min_temp = int(min_temp * 100)
    max_temp = int(max_temp * 100)

    if img.dtype == np.uint16:
        img = img.astype(np.int16)

    # clip temperatures
    img = np.clip(img, a_min=min_temp, a_max=max_temp).astype(np.int32)

    # rescale to value between 0 and 255
    img = (((img - min_temp) * 255) / (max_temp - min_temp)).astype(np.uint8)

    return img


def prepare_logdir(args):
    """
    Generate new subdirectory structure for logging this training session
    Ensure the following folder structure is created:
    args.logdir
        <machine>_<date_time>_<network_cfg_file_name>
            tb
                <tensorboard_log_files>
            checkpoints
                <checkpoints>
            <network_cfg_file_name>.py
            std.log
    """
    # Generate new subdirectory structure for logging this training session
    dir_name = socket.gethostname() + datetime.now().strftime("_%B%d_%H_%M_%S_") + os.path.splitext(os.path.basename(args.network))[0]
    dir_name = os.path.join(args.logdir, dir_name)
    tb_dir_name = os.path.join(dir_name, 'tb')
    checkpoints_dir_name = os.path.join(dir_name, 'checkpoints')
    os.makedirs(tb_dir_name)
    os.makedirs(checkpoints_dir_name)
    shutil.copy(args.network, dir_name)
    if args.weight is not None:
        shutil.copy(args.weight, os.path.join(checkpoints_dir_name, "init.pt"))

    # log to file too
    logging.basicConfig(filename=os.path.join(dir_name, 'std.log'), filemode='w')

    return tb_dir_name, checkpoints_dir_name


# TODO: implement more efficient with reshape
def squash_dims(tensor, dims):
    """
    Squashes dimension, given in dims into one, which equals to product of given.

    Args:
        tensor (Tensor): input tensor
        dims: dimensions over which tensor should be squashed

    """
    assert len(dims) >= 2, "Expected two or more dims to be squashed"

    size = tensor.size()

    squashed_dim = size[dims[0]]
    for i in range(1, len(dims)):
        assert dims[i] == dims[i - 1] + 1, "Squashed dims should be consecutive"
        squashed_dim *= size[dims[i]]

    result_dims = size[:dims[0]] + (squashed_dim,) + size[dims[-1] + 1:]
    return tensor.reshape(*result_dims)


def unsquash_dim(tensor, dim, res_dim):
    """
    Unsquashes dimension, given in dim into separate dimensions given is res_dim
    Args:
        tensor (Tensor): input tensor
        dim (int): dimension that should be unsquashed
        res_dim (tuple): list of dimensions, that given dim should be unfolded to

    """
    size = tensor.size()
    result_dim = size[:dim] + res_dim + size[dim + 1:]
    return tensor.reshape(*result_dim)
