import lightnet as ln
import numpy as np
import dataset
import argparse
import torch
import torch.nn as nn
import onnx
import os
import tensorflow as tf
import random
from dataset import MlDetectionTrainData
import bn_fold
import utils



parser = argparse.ArgumentParser(description='Read pytorch model and convert it to keras and than to tflite')
parser.add_argument('network', help='network config file')
parser.add_argument('weights', help='network weight file (.pt)')
parser.add_argument('output', help='output model (.tflite)')
parser.add_argument('-d', '--deterministic', help='Enable deterministic behaviour', action='store_true')
parser.add_argument('-m', '--manual_seed', type=int, help='Set manual seed value for random generators', default=0)
parser.add_argument('--quant', help='quantize to 8-bit', action='store_true')
parser.add_argument('--calib_size', help='Size of the calibration dataset', type=int, default=100)
parser.add_argument('--save_io_folder', help='Output folder to store (quantized) input/output tensors for testing purposes', default="")
args = parser.parse_args()


if args.deterministic:
    torch.manual_seed(args.manual_seed)
    random.seed(args.manual_seed)
    np.random.seed(args.manual_seed)

###############################################################################
#
# Load pytorch network and weights
#
###############################################################################

params = ln.engine.HyperParameters.from_file(args.network)

if args.weights is not None:
    if 'pruned' in args.weights:
        params.network.load_pruned(args.weights)
    else:
        params.network.load(args.weights)

# Already fold batchnorm layers to make conversion to keras easier and to avoid loss in precision
network_fused = bn_fold.fuse_bn_recursively(params.network)


###############################################################################
#
# Prepare dataset for evaluation and calibration
#
###############################################################################

print('Preparing calibration dataset')
params.train_set = params.train_set

dataset = MlDetectionTrainData(params, augment=False)
data_loader = ln.data.DataLoader(
    dataset,
    batch_size = 1,
    shuffle = True,
    num_workers = 0,
    collate_fn = utils.brambox_collate,
)

torch_calib_data = []
tf_calib_data = []
for i, (data, target) in enumerate(data_loader):
    if i == args.calib_size:
        break

    # Filter out faulty frames (with sampling artifacts). These usually contain abnormally high temperatures
    # and can cause poorly chosen quantization input scale and zero points
    if (data.max() * params.std + params.mean) > 10000:
        continue

    torch_calib_data.append(data)
    tf_data = data.permute(0, 2, 3, 1).numpy()  # NCHW to NHWC and to numpy
    tf_calib_data.append(tf_data)


###############################################################################
#
# Convert pytorch model to keras and copy its weights
# WARNING: this code is not generic and only works with a simple network architecture
#
###############################################################################

img_shape = (params.input_dimension[1], params.input_dimension[0], params.network.input_channels)

inputs = tf.keras.Input(shape=img_shape)
x = inputs
for name, module in network_fused.named_modules():
    if isinstance(module, nn.Conv2d):

        # regular convolution
        if module.groups == 1:
            # create conv
            conv = tf.keras.layers.Conv2D(module.out_channels,
                                          module.kernel_size,
                                          padding='same',
                                          use_bias=module.bias is not None,
                                          strides=module.stride,
                                          name=name)
            x = conv(x)

            weights = module.weight.permute(2, 3, 1, 0).detach().numpy()

        # depthwise convolution
        elif module.groups == module.out_channels:
            # add custom padding if stride is 2 and h or w are even, in order to match
            # the behaviour of pytorch
            _, h, w, _ = x.shape
            h_even, w_even = (h % 2 == 0), (w % 2 == 0)
            padding = 'same'
            if module.stride == (2, 2) and (h_even or w_even):
                x = tf.keras.layers.ZeroPadding2D(padding=((int(h_even), 0), (int(w_even), 0)))(x)
                padding = 'valid'

            # create conv
            conv = tf.keras.layers.DepthwiseConv2D(
                                          module.kernel_size,
                                          padding=padding,
                                          use_bias=module.bias is not None,
                                          strides=module.stride,
                                          name=name)
            x = conv(x)

            weights = module.weight.permute(2, 3, 0, 1).detach().numpy()

        # copy weights and bias
        if module.bias is not None:
            bias = module.bias.detach().numpy()
            conv.set_weights([weights, bias])
        else:
            conv.set_weights([weights])

    elif isinstance(module, nn.ReLU6):
        x = tf.keras.layers.ReLU(max_value=6., name=name)(x)

    elif isinstance(module, nn.MaxPool2d):
        x = tf.keras.layers.MaxPool2D(pool_size=module.kernel_size,
                                      strides=module.stride)(x)

tf_model = tf.keras.Model(inputs, x)
tf_model.summary()


###############################################################################
#
# Convert keras model to tflite
#
###############################################################################

def representative_dataset():
    for data in tf_calib_data:
        res = [data]
        yield res


converter = tf.lite.TFLiteConverter.from_keras_model(tf_model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
if args.quant:
    converter.representative_dataset = representative_dataset
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    converter.inference_input_type = tf.int8
    converter.inference_output_type = tf.int8

tflite_model = converter.convert()

with open(args.output, 'wb') as f:
    f.write(tflite_model)


###############################################################################
#
# Compare output of pytorch model to that of the tflite model
#
###############################################################################


def calc_error(res1, res2):
    mse = ((res1 - res2) ** 2).mean(axis=None)
    mae = np.abs(res1 - res2).mean(axis=None)
    metrics = {'mse': mse, 'mae': mae}
    print(f"\n\nMean-Square-Error between predictions:\t{metrics['mse']}")
    print(f"Mean-Abs-Error between predictions:\t{metrics['mae']}\n\n")
    return metrics

# Prepare pytorch model for testing
params.network.eval()

# Prepare tflite model for testing
tflite_model = tf.lite.Interpreter(args.output)
tflite_model.allocate_tensors()
input_details = tflite_model.get_input_details()
output_details = tflite_model.get_output_details()

torch_output = []
tf_output = []
for i, (torch_data, tf_data) in enumerate(zip(torch_calib_data, tf_calib_data)):

    # Infer Pytorch
    torch_out = params.network(torch_data)
    torch_out = torch_out.permute(0, 2, 3, 1).detach().numpy()  # NCHW to NHWC and to numpy
    torch_output.append(torch_out)

    # Infer TFLite
    if args.quant:
        q_scale = input_details[0]['quantization'][0]
        q_zero = input_details[0]['quantization'][1]
        tf_data = (tf_data / q_scale + q_zero).astype(np.int8)

    if args.save_io_folder:
        tf_data.tofile(os.path.join(args.save_io_folder, f'input_{i}.bin'))

    tflite_model.set_tensor(input_details[0]['index'], tf_data)
    tflite_model.invoke()
    tf_out = tflite_model.get_tensor(output_details[0]['index'])

    if args.save_io_folder:
        tf_out.tofile(os.path.join(args.save_io_folder, f'output_{i}.bin'))

    if args.quant:
        q_scale = output_details[0]['quantization'][0]
        q_zero = output_details[0]['quantization'][1]
        tf_out = (tf_out.astype(np.int32) - q_zero) * q_scale

    tf_output.append(tf_out)

torch_output = np.concatenate(torch_output)
tf_output = np.concatenate(tf_output)

assert torch_output.shape == tf_output.shape
calc_error(torch_output, tf_output)

