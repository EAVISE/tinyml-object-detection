import random
import numpy as np
import lightnet as ln


# TODO: change looping to padding
class TemporalRandomCrop(object):
    """Temporally crop the given frame indices at a random location.

    If the number of frames is less than the size,
    loop the indices as many times as necessary to satisfy the size.

    Args:
        size (int): Desired output size of the crop.
    """

    def __init__(self, size):
        self.size = size

    def __call__(self, frame_indices):
        """
        Args:
            frame_indices (list): frame indices to be cropped.
        Returns:
            list: Cropped frame indices.
        """

        rand_end = max(0, len(frame_indices) - self.size - 1)
        begin_index = random.randint(0, rand_end)
        end_index = min(begin_index + self.size, len(frame_indices))

        out = frame_indices[int(begin_index):int(end_index)]

        for index in out:
            if len(out) >= self.size:
                break
            out.append(index)

        return out


# TODO: change looping to padding
class TemporalCenterCrop(object):
    """Temporally crop the given frame indices at a center.

    If the number of frames is less than the size,
    loop the indices as many times as necessary to satisfy the size.

    Args:
        size (int): Desired output size of the crop.
    """

    def __init__(self, size):
        self.size = size

    def __call__(self, frame_indices):
        """
        Args:
            frame_indices (list): frame indices to be cropped.
        Returns:
            list: Cropped frame indices.
        """

        center_index = len(frame_indices) // 2
        begin_index = max(0, center_index - (self.size // 2))
        end_index = min(begin_index + self.size, len(frame_indices))

        out = frame_indices[begin_index:end_index]

        for index in out:
            if len(out) >= self.size:
                break
            out.append(index)

        return out


class TemporalRandomScale(object):
    """Scale the temporal domain by repeating/removing frame indices
        The final result is cropped/padded to keep the same number of frame_indices at the end
    """

    def __init__(self, scale = 0.0):
        """A scale < 1.0 shortens the video, a scale > 1.0 elongates the video"""

        self.scale_range = (1.0 - scale, 1.0 + scale)
        self.scale = None

    def __call__(self, frame_indices):

        if self.scale is None:
            scale = random.uniform(self.scale_range[0], self.scale_range[1])
        else:
            scale = self.scale

        num_frame_indices = len(frame_indices)
        scaled_num_frame_indices = round(scale * num_frame_indices)

        # stretch/shrink
        indices = np.round(np.linspace(0, num_frame_indices - 1, num=scaled_num_frame_indices)).astype(np.int)
        scaled_frame_indices = [frame_indices[i] for i in indices]

        # crop/pad
        if scaled_num_frame_indices > num_frame_indices:
            # crop
            diff = (scaled_num_frame_indices - num_frame_indices)
            odd = diff % 2
            crop_len = diff // 2
            scaled_frame_indices = scaled_frame_indices[crop_len:-(crop_len + odd)]

        elif scaled_num_frame_indices < num_frame_indices:
            # pad
            diff = (num_frame_indices - scaled_num_frame_indices)
            odd = diff % 2
            pad_len = diff // 2
            scaled_frame_indices = [scaled_frame_indices[0]] * pad_len +\
                                    scaled_frame_indices +\
                                   [scaled_frame_indices[-1]] * (pad_len + odd)

        return scaled_frame_indices


def create_temporal_transform(params, augment):
    """Make temporal transformation
    """
    if augment:
        transform = ln.data.transform.Compose([
                    TemporalRandomScale(params.temporal_scale),
                    TemporalRandomCrop(params.temporal_size),
                    ])
    else:
        transform = ln.data.transform.Compose([
                    TemporalCenterCrop(params.temporal_size)
                    ])

    return transform
