# WARNING: this script requires lightnet-private for now!!!!
import lightnet as ln
import brambox as bb
import argparse
from dataset import parse_annos

import utils


# Load training annotations
video_path_list = 'data/video/meta_45/train.txt'
annos = parse_annos(video_path_list, 'data/video', 15)
annos = bb.util.concat(annos)

print(annos)

# obtain preprocessing
transform = ln.data.transform.Letterbox((32, 24))

# This function can take a while, so debug level logs will give us slightly more information on progress
ln.logger.setConsoleLevel(0)
# Run clustering
anchors = ln.util.compute_anchors(
     # Annotations
     annos,
     # We want 5 anchors
     5,
     transform,
     # Function to get the image path from the image column names in the anno dataframe
     lambda name: name + '.tiff',
)
# Show anchor values compared to input size (see warning)
print(anchors)
# Show anchor values compared to output of microyolo
# While not entirely identical, they closely match the values from darknet
print(anchors.as_output(4))
