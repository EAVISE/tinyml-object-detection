import numpy as np
import os
import cv2
import argparse
import serial
import struct
import lightnet as ln
import brambox as bb
import logging
from tqdm import tqdm

from dataset import MlDetectionTestData
import utils

log = logging.getLogger('test_on_target')
logging.basicConfig(level=logging.INFO)


class TargetDevice:
    def __init__(self, port, baud):
        #self.detections_payload_fmt = "<iiiif"
        self.detections_payload_fmt = "<fffff"
        self.ser = serial.Serial(port, baud)

    def infer_image(self, image):
        """Send serialized image over serial port
        """
        # serialize image data
        image_data = image.tobytes()

        # send data header: preamble|length
        header = struct.pack("<HH", 0xbeef, len(image_data))
        self.ser.write(header)

        # write image data
        self.ser.write(image_data)

    def get_tensor(self):
        """Block until a tensor is returned
        """
        log.debug("Waiting for response")
        pre0 = pre1 = 0
        while pre0 != 0xbe or pre1 != 0xef:
            pre1 = pre0
            pre0 = self.ser.read(1)[0]
        log.debug("Received preamble")

        # read payload length
        payload_length_bytes = struct.unpack_from("<H", self.ser.read(2))
        log.debug(f"Reading payload of {payload_length_bytes} bytes")

        # read tensor
        tensor = self.ser.read(payload_length_bytes[0])

        return tensor

    def get_output(self, image_id):
        """Block until detection output is received from target
        """
        log.debug("Waiting for response")
        pre0 = pre1 = 0
        while pre0 != 0xbe or pre1 != 0xef:
            pre1 = pre0
            pre0 = self.ser.read(1)[0]
        log.debug("Received preamble")

        # read header
        infer_time, payload_length_bytes = struct.unpack_from("<IH", self.ser.read(6))
        log.debug(f"Reading payload of {payload_length_bytes} bytes")

        num_bytes_per_detection = struct.calcsize(self.detections_payload_fmt)
        num_detections = payload_length_bytes // num_bytes_per_detection

        detections = {"image":[],
                      "class_label":[],
                      "x_top_left":[],
                      "y_top_left":[],
                      "width":[],
                      "height":[],
                      "confidence":[],
                      "infer_time":[]}
        # create detections dict
        for i in range(num_detections):
            log.debug(f"Reading detection {i+1} from {num_detections}")
            values = struct.unpack_from(self.detections_payload_fmt, self.ser.read(num_bytes_per_detection))
            detections["image"].append(image_id)
            detections["class_label"].append("person")
            detections["x_top_left"].append(values[0])
            detections["y_top_left"].append(values[1])
            detections["width"].append(values[2])
            detections["height"].append(values[3])
            detections["confidence"].append(values[4])
            detections["infer_time"].append(infer_time)

        return bb.util.from_dict(detections)

    def close(self):
        self.ser.close()


def show_bboxes(det, anno):
    """Display annos and ground truth using brambox visualization
    """
    # display detections and annotations in different colors
    det['color'] = [(31, 119, 180)] * len(det)
    anno['color'] = [(255, 127, 14)] * len(anno)
    boxes = bb.util.concat([anno, det], ignore_index=True, sort=False)

    def read_image(img_id):
        img_path = img_id + '.tiff'
        print(img_path)
        img = cv2.imread(img_path, -1)
        img = utils.apply_agc_linear(img, max_temp=30)
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        return img

    # Visualize
    drawer = bb.util.BoxDrawer(
        images=read_image,
        boxes=boxes,
        size=1,
        method=bb.util.DrawMethod.CV
    )

    i=0
    while True:
        img = drawer[i]
        img = cv2.resize(img, (320, 240))
        cv2.imshow("image", img)
        print("image", i, "/", len(drawer))
        k = cv2.waitKey(0)
        if k == ord('d'):
            i += 1
            if i == len(drawer):
                i = 0
        elif k == ord('a'):
            i -= 1
            if i < 0:
                i = len(drawer) - 1
        elif k == 27:
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run test images on target')
    parser.add_argument('port', help='Serial port the device is connected too')
    parser.add_argument('--baud', help='Baudrate', type=int, default=115200)
    parser.add_argument('--view', help='View detections rather than calculating AP metrics', action="store_true")
    args = parser.parse_args()

    device = TargetDevice(args.port, args.baud)

    # create required params for test dataset
    params = lambda: None
    params.diff_channel_stride = 0
    params.use_background_image = False
    params.test_set = "data/video/meta_test/clip000.txt"
    params.min_bbox_area_test = 15
    params.color = False
    params.input_dimension = (32, 24)
    params.mean = 0.0
    params.std = 1.0

    test_dataset = MlDetectionTestData(params)

    data_loader = ln.data.DataLoader(
        test_dataset,
        batch_size = 1,
        shuffle = False,
        drop_last = False,
        num_workers = 0,
        collate_fn = utils.brambox_collate,
    )

    det = []
    anno = []
    for idx, (data, target) in enumerate(tqdm(data_loader)):
        data = data.numpy().astype(np.int16)
        device.infer_image(data)
        output = device.get_output(target.image.cat.categories[0])
        det.append(output)
        anno.append(target)

    det = bb.util.concat(det, ignore_index=True, sort=False)
    anno = bb.util.concat(anno, ignore_index=True, sort=False)

    device.close()

    log.info(f"Average infer time (ms): {det['infer_time'].mean()/1000:.2f}")

    if args.view:
        show_bboxes(det, anno)
    else:
        # target sends detections with conf scores >= F1 optimized threshold.
        # In order to create pr data with only 1 precision and recall value to calculate F1-score,
        # we need to set all confidence scores to the same value
        det['confidence'] = 1.0
        match_criteria_thresh = 0.5
        tpfp_det = bb.stat.match_det(det, anno, match_criteria_thresh, criteria=bb.stat.coordinates.ioa)
        pr = bb.stat.pr(tpfp_det, anno, match_criteria_thresh)
        f1 = bb.stat.fscore(pr)

        log.info(f'Precision: {100*float(pr.precision):.2f}%, ')
        log.info(f'Recall: {100*float(pr.recall):.2f}%, ')
        log.info(f'F1: {100*float(f1.f1):.2f}%')
