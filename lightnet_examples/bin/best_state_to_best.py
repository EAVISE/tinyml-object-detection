import torch
import sys
import os

filename = sys.argv[1]

sd = torch.load(filename)
out_filename = os.path.join(os.path.dirname(filename), 'best.pt')
torch.save(sd['network'], out_filename)
