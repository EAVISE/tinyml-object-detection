import os
from tqdm import tqdm
import cv2
import torch
import logging
import numpy as np
import brambox as bb
import lightnet as ln
import pandas as pd

import utils
if ln.__version__ == '2.0.0':
    import spatial_transforms


DATA_ROOT = 'data/video'


bb.io.register_parser('ml_detection', utils.MlDetectionParser)

log = logging.getLogger('lightnet.MlDetection.dataset')


def get_video_frame_paths(path, fmt_string):
    """Return the ordered image frame paths of the video frames in directory `path`
    """
    fmt_path = os.path.join(path, fmt_string)
    cap = cv2.VideoCapture(fmt_path, cv2.CAP_IMAGES)
    num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    return [fmt_path%i for i in range(num_frames)]


def calculate_motion_target(anno, stride):
    """Calculate the x,y motion of an object based on the displacement of a bbox between the current frame and `stride` frames ago.
    Only bboxes with the same track id are compared. A `motion_x` and `motion_y` column is added to `anno` and the function returns the modified
    `anno`. Both x and y motion values are calculated by:

        motion_x = (x2 - x1) / w2
        motion_y = (y2 - y1) / h2

    where indices 1 and 2 refer to the coordinates of the previous and current bbox respectively.
    `anno` is a dataframe of a single clip.
    """
    # only select annos that have a valid tracking id
    anno_valid_ids = anno[~anno.id.isnull()]

    if anno_valid_ids.empty:
        anno_valid_ids['motion_x'] = 0.0
        anno_valid_ids['motion_y'] = 0.0
        return anno_valid_ids

    def calc_motion(df):
        # repeat first row by an additional stride times
        df_ = df.iloc[np.arange(len(df)).repeat(([stride+1] + (len(df)-1) * [1]))]

        # separate boxes from `stride` frames ago (df_prev) and current boxes (df_curr)
        df_prev = df_.iloc[:len(df), :]
        df_curr = df_.iloc[stride:, :]
        df_prev.index=df_curr.index.copy()  # ensure the index of df_prev matches that of df_curr

        # add motion columns
        df_curr['motion_x'] = (df_curr['x_top_left'] - df_prev['x_top_left']) / df_curr['width']
        df_curr['motion_y'] = (df_curr['y_top_left'] - df_prev['y_top_left']) / df_curr['height']

        # include id column
        df_curr['id'] = df.name

        return df_curr

    return anno_valid_ids.groupby('id').apply(calc_motion).dropna()


def parse_annos(video_path_list, root, min_bbox_area):
    """
    video_path_list: Filename containing al list of video paths
    """
    # parse video_path_list file
    with open(video_path_list) as f:
        paths = [os.path.join(root, line.strip()) for line in f]

    annos = []
    log.info("Parsing annotations")
    for path in tqdm(paths):
        anno_path = os.path.join(path, 'gt_corrected.txt')
        if not os.path.exists(anno_path):
            anno_path = os.path.join(path, 'gt.txt')

        image_paths = get_video_frame_paths(path, 'image%07d.tiff')
        df = bb.io.load('anno_mot', anno_path, class_label_map=['person'], seq_length=len(image_paths))

        # rename image ids to their respective image file paths without extension, which makes them easy to read and unique across all videos
        df['image'] = df['image'].cat.rename_categories([os.path.splitext(img_path)[0] for img_path in image_paths])

        # set ignore flag to True when bbox area < min_bbox_area
        df['ignore'] = (df['width'] * df['height']) < min_bbox_area

        annos.append(df)

    return annos


def create_background_images(annos):
    """Create a background image for each video sequence in annos
    annos: a list of pandas dataframes, where each dataframe contains the annotations of one video clip
    """
    log.info("Creating background images")
    bg_images = {}
    for per_clip_anno in tqdm(annos):

        # calculate clip_id
        clip_id = os.path.dirname(per_clip_anno.image.cat.categories[0])

        # select N evenly spaced images from the clip to build the background
        N = 25
        image_ids = np.array(per_clip_anno.image.cat.categories)
        image_ids = image_ids[list(set(np.round(np.linspace(0, len(image_ids) - 1, N)).astype(int)))]
        per_clip_anno = bb.util.select_images(per_clip_anno, image_ids)

        # construct the background image from the selected images and their corresponding annotations
        sum_of_images = None
        sum_of_masks = None
        for image_id, anno in per_clip_anno.groupby('image', sort=False):
            img = opencv_read_image(image_id).astype(np.int32)    # ensure summation of several images does not overflow

            # check if frame is valid (filter out corrupt frames)
            if img.max() > 7000:
                continue

            mask = utils.create_bbox_mask(img, anno).astype(np.int32)   # create a binary mask: 0 inside the area of a bbox, 1 elsewhere

            if sum_of_images is None:
                sum_of_images = img * mask
                sum_of_masks = mask
            else:
                sum_of_images += img * mask
                sum_of_masks += mask

        sum_of_masks[sum_of_masks == 0] = -1        # to avoid division by zero
        bg_img = (sum_of_images / sum_of_masks).astype(np.int16)

        # set the values in bg_images that are still zero to a valid temperature
        fill_temperature = bg_img[sum_of_masks != -1].mean()
        bg_img[sum_of_masks == -1] = fill_temperature

        bg_images[clip_id] = bg_img

    return bg_images


def opencv_read_image(path):
    # read raw image, preserving its bit depth and number of channels
    image = cv2.imread(path + '.tiff', -1)

    # interpret thermal images as signed ints since values can be negative
    if image.dtype.type == np.uint16:
        image = image.astype(np.int16)

    return image


def get_image_id_neighbor(image_id, stride):
    """Get the image id of a neighbor frame in the image sequence, given the current image id.
    Stride:
        -2 -> return image_id current frame - 2
        -1 -> return image_id current frame - 1
         0 -> return image_id current frame
         1 -> return image_id current frame + 1
         2 -> return image_id current frame + 2
    """
    # assume the last N characters of the image id are the frame number digits
    N = 7
    frame_nr = int(image_id[-N:])
    frame_nr_neighbor = max(0, (frame_nr + int(stride)))
    image_id_neighbor = f'{image_id[:-N]}{frame_nr_neighbor:0{N}}'

    return image_id_neighbor


class MlDetectionData(ln.data.Dataset):
    """Dataset class for MlDetection dataset
    """
    def __init__(self, input_dimension, annotations, params, transform=None):
        """
        input_dimension (tuple): (width,height) tuple with default dimensions of the network
        annotations (pd.DataFrame): Brambox dataframe with annotations
        params:                     Parameters from .cfg
        transform:                  Lightnet spatial transform that is applied for each frame in the video
        """
        super().__init__(input_dimension)

        self.annos = bb.util.concat(annotations)
        self.params = params
        self.transform = transform

        if params.use_background_image:
            self.bg_images = create_background_images(annotations)

        self.keys = self.annos.image.cat.categories

        # generate class ids, required for loss function
        class_label_map = list(np.sort(self.annos.class_label.unique()))
        self.annos['class_id'] = self.annos.class_label.map(dict((l, i) for i, l in enumerate(class_label_map)))

    def __len__(self):
        return len(self.keys)

    @ln.data.Dataset.resize_getitem
    def __getitem__(self, index):
        """ Get transformed image and annotations based of the index of ``self.keys``

        Args:
            index (int): index of the ``self.keys`` list containing all the image identifiers of the dataset.

        Returns:
            tuple: (transformed image, list of transformed brambox boxes)
        """

        if index >= len(self):
            raise IndexError(f'list index out of range [{index}/{len(self)-1}]')

        # Load
        image_id = self.keys[index]
        img = opencv_read_image(image_id)
        anno = bb.util.select_images(self.annos, [image_id])

        # randomize transforms
        if self.transform is not None:
            for tf in self.transform:
                if hasattr(tf, '_randomize_params'):
                    tf._randomize_params()

        # Transform
        if self.transform is not None:
            (img, anno) = self.transform((img, anno))

        # Add background image if requested
        bg_img = None
        if self.params.use_background_image:
            clip_id = os.path.dirname(image_id)
            bg_img = self.bg_images[clip_id]

            # Transform bg image
            if self.transform is not None:
                bg_img = self.transform(bg_img)

        # Add differential channel if requested
        diff = None
        if self.params.diff_channel_stride > 0:
            prev_image_id = get_image_id_neighbor(image_id, -self.params.diff_channel_stride)
            prev_img = opencv_read_image(prev_image_id)

            # Transform previous image
            if self.transform is not None:
                prev_img = self.transform(prev_img)

            diff = img - prev_img

        if bg_img is not None:
            if self.params.subtract_background_image:
                img -= bg_img
            else:
                img = torch.cat([img, bg_img], 0)

        if diff is not None:
            img = torch.cat([img, diff], 0)

        return img, anno


class MlDetectionTrainData(MlDetectionData):
    def __init__(self, params, augment=True, data_root=DATA_ROOT):

        transform = spatial_transforms.create_spatial_transform(params, augment)
        annotations = parse_annos(params.train_set, data_root, params.min_bbox_area_train)

        super().__init__(params.input_dimension, annotations, params, transform)


class MlDetectionValidData(MlDetectionData):
    def __init__(self, params, augment=False, data_root=DATA_ROOT):

        transform = spatial_transforms.create_spatial_transform(params, augment)
        annotations = parse_annos(params.valid_set, data_root, params.min_bbox_area_test)

        super().__init__(params.input_dimension, annotations, params, transform)


class MlDetectionTestData(MlDetectionData):
    def __init__(self, params, augment=False, data_root=DATA_ROOT):

        transform = spatial_transforms.create_spatial_transform(params, augment)
        annotations = parse_annos(params.test_set, data_root, params.min_bbox_area_test)

        super().__init__(params.input_dimension, annotations, params, transform)


class MlVideoDetectionData(ln.data.Dataset):
    """Dataset class for MlDetection dataset that returns video snippets rather than single images
    """
    def __init__(self, input_dimension, annotations, params, transform=None):
        """
        input_dimension (tuple): (width,height) tuple with default dimensions of the network
        annotations (list):         List of pd.DataFrame objects
        params:                     Parameters from .cfg
        transform:                  Lightnet spatial transform that is applied for each frame in the video
        """
        super().__init__(input_dimension)
        self.annos = annotations
        self.params = params

        if params.use_background_image:
            self.bg_images = create_background_images(annotations)

        # Each video will be split in N parts of the given temporal_size

        # construct video snippets data
        self.video_snippets = []
        for clip_idx, annos in enumerate(annotations):
            video_len = len(annos.image.cat.categories)
            # cut video in snippets of size (temporal_size * temporal_stride)
            snippet_size = self.params.temporal_size * self.params.temporal_stride
            for image_idx in range(0, video_len, snippet_size):
                # don't include the last snippet if its smaller the snippet_size
                if (video_len - image_idx) < snippet_size:
                    break
                clip_id = os.path.dirname(annos.image.cat.categories[0])
                image_indices = [image_idx + round(i * self.params.temporal_stride) for i in range(self.params.temporal_size)]
                image_ids = [os.path.join(clip_id, 'image%07d'%i) for i in image_indices]
                self.video_snippets.append((clip_idx, image_ids))

            # generate class ids, required for loss function
            class_label_map = list(np.sort(annos.class_label.unique()))
            annos['class_id'] = annos.class_label.map(dict((l, i) for i, l in enumerate(class_label_map)))

        self.transform = transform

    def __len__(self):

        return len(self.video_snippets)

    @ln.data.Dataset.resize_getitem
    def __getitem__(self, index):

        # select a video path and annotations
        clip_idx, image_ids = self.video_snippets[index]
        annos = self.annos[clip_idx]

        # randomize spatial transform parameters (parameters need to be the same for every frame in this video sequence)
        if self.transform is not None:
            for tf in self.transform:
                if hasattr(tf, '_randomize_params'):
                    tf._randomize_params()

        # preprocess background image if requested
        bg_img = None
        if self.params.use_background_image:
            clip_id = os.path.dirname(image_ids[0])
            bg_img = self.bg_images[clip_id]

            # Transform bg image
            if self.transform is not None:
                bg_img = self.transform(bg_img)

        # read the video frames and apply per-frame spatial transform on video and annotations
        video_frames = []
        video_annos = []

        for i, image_id in enumerate(image_ids):
            img = opencv_read_image(image_id)

            # select corresponding annotation and apply spatial transform
            anno = bb.util.select_images(annos, [image_id])
            anno['frame_number'] = i    # add frame number column

            if self.transform is not None:
                img, anno = self.transform((img, anno))

            if bg_img is not None:
                if self.params.subtract_background_image:
                    img -= bg_img
                else:
                    img = torch.cat([img, bg_img], 0)

            video_frames.append(img)
            video_annos.append(anno)

        video_annos = bb.util.concat(video_annos, sort_image_categories=False, ignore_index=True, sort=False)
        video_frames = torch.stack(video_frames)

        return video_frames, video_annos


class MlVideoDetectionTrainData(MlVideoDetectionData):
    def __init__(self, params, augment=True, data_root=DATA_ROOT):

        transform = spatial_transforms.create_spatial_transform(params, augment)
        annotations = parse_annos(params.train_set, data_root, params.min_bbox_area_test)

        super().__init__(params.input_dimension, annotations, params, transform)


class MlVideoDetectionValidData(MlVideoDetectionData):
    def __init__(self, params, augment=False, data_root=DATA_ROOT):

        transform = spatial_transforms.create_spatial_transform(params, augment)
        annotations = parse_annos(params.valid_set, data_root, params.min_bbox_area_test)

        super().__init__(params.input_dimension, annotations, params, transform)


class MlVideoDetectionTestData(MlVideoDetectionData):
    def __init__(self, params, augment=False, data_root=DATA_ROOT):

        transform = spatial_transforms.create_spatial_transform(params, augment)
        annotations = parse_annos(params.test_set, data_root, params.min_bbox_area_test)

        super().__init__(params.input_dimension, annotations, params, transform)
