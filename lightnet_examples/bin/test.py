import os
import cv2
from tqdm import tqdm
import pandas as pd
import brambox as bb
import logging
import argparse
import torch
import lightnet as ln
from dataset import MlDetectionTestData

import utils
import numpy as np

log = logging.getLogger('lightnet.MlDetection.test')

# RGB
DET_COLOR = (31, 119, 180)  # blue
ANNO_COLOR = (255, 127, 14) # orange


def visualize_input_data(input_data, display_size, min_value, max_value):
    """Visualize all image channels side by side
    assume input_data has shape C x H x W
    """
    # normalize for visualization
    input_data = torch.clamp(input_data, min=min_value, max=max_value)
    input_data = ((input_data - min_value) / (max_value - min_value)).numpy()
    input_data = (input_data * 255).astype(np.uint8)  # covert float32 grayscale to 8-bit

    image_channels = []
    for image in input_data:
        image = cv2.applyColorMap(image, cv2.COLORMAP_JET)
        image = cv2.resize(image, display_size)
        image_channels.append(image)

    return  cv2.hconcat(image_channels)


def draw_image(drawer, idx):
    """Draw bboxes on image, but also draw motion vector if present
    """
    # draw bboxes
    img = drawer[idx]

    # draw motion vector on top if present in boxes
    lbl = drawer.image_labels[idx]
    boxes = drawer.boxes[drawer.boxes.image == lbl]
    if 'motion_x' in boxes.columns:
        for i, row in boxes.iterrows():
            if np.isnan(row.motion_x) or np.isnan(row.motion_y):
                continue
            color = row.color[::-1] # colors are in RGB, convert to BGR
            pt1 = (int(row.x_top_left + row.width / 2), int(row.y_top_left + row.height / 2))
            pt2 = (pt1[0] + int(row.motion_x * row.width), pt1[1] + int(row.motion_y * row.height))
            img = cv2.arrowedLine(img, pt1, pt2, color, 2)

    return img


def visualize(anno, det, input_data=None, visualize_gt=True, video_filename=""):
    # display detections and annotations in different colors
    det['color'] = [DET_COLOR] * len(det)
    anno['color'] = [ANNO_COLOR] * len(anno)

    # add confidence as label
    det['label'] = [f'{c*100:.1f}%' for c in det.confidence]
    anno['label'] = len(anno) * ['']

    # combine in one dataframe
    boxes = det
    if visualize_gt:
        boxes = bb.util.concat([anno, det], ignore_index=True, sort=False)

    # upscale coordinates to display size
    display_size = (320, 240)
    boxes['x_top_left'] = boxes['x_top_left'].mul(10)
    boxes['y_top_left'] = boxes['y_top_left'].mul(10)
    boxes['width']      = boxes['width'].mul(10)
    boxes['height']     = boxes['height'].mul(10)

    def read_image(img_id):
        img_path = img_id + '.tiff'
        #print(img_path)
        img = cv2.imread(img_path, -1)
        #img = utils.apply_agc_linear(img, max_temp=30)
        img = utils.apply_agc(img)
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        img = cv2.resize(img, display_size)
        return img

    # Visualize
    drawer = bb.util.BoxDrawer(
        images=read_image,
        boxes=boxes,
        method=bb.util.DrawMethod.CV
    )

    if input_data is not None:
        max_value = min(3*input_data.std()/2, input_data.max())
        min_value = max(-3*input_data.std()/2, input_data.min())
        #max_value = input_data.max()
        #min_value = input_data.min()
        print(min_value, max_value)


    i=0
    if video_filename:
        # Write a video file too
        writer = cv2.VideoWriter(video_filename, cv2.VideoWriter_fourcc('M','J','P','G'), 8, display_size, True)
        for img in tqdm(drawer):
            if input_data is not None:
                in_data = visualize_input_data(input_data[i], display_size, min_value, max_value)
                img = cv2.hconcat([img, in_data])
            writer.write(img)

    while True:
        img = draw_image(drawer, i)
        if input_data is not None:
            in_data = visualize_input_data(input_data[i], display_size, min_value, max_value)
            img = cv2.hconcat([img, in_data])
        cv2.imshow("image", img)
        print("image", i, "/", len(drawer))
        k = cv2.waitKey(0)
        if k == ord('d'):
            i += 1
            if i == len(drawer):
                i = 0
        elif k == ord('a'):
            i -= 1
            if i < 0:
                i = len(drawer) - 1
        elif k == 27:
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test network')
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file')
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-w', '--workers', type=int, help='Number of dataloader workers', default=8)
    parser.add_argument('-v', '--visual', help='Visualize the detections too', action='store_true')
    parser.add_argument('-i', '--input_data', help='Visualize input data too', action='store_true')
    parser.add_argument('-b', '--bg_builder', help='Enalbe online background estimation', action='store_true')
    parser.add_argument('-t', '--thresh', help='Detection Threshold for visualization', type=float, default=0.4)
    parser.add_argument('-o', '--output', help='Write detection to this file in .parquet format', default="")
    parser.add_argument('--output-video', help='Write output video file instead of visualizing. Note that --visual need to be provided too', default="")
    args = parser.parse_args()

    # Log output to file
    log_dir = os.path.dirname(args.network)
    logging.basicConfig(filename=os.path.join(log_dir, 'test.log'), filemode='w')

    # Parse arguments
    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    params = ln.engine.HyperParameters.from_file(args.network)
    if args.weight is not None:
        if args.weight.endswith('.tflite'):
            # override torch network with tflite model wrapper
            params.network = utils.TfLiteModelRunner(args.weight)
        elif args.weight.endswith('.state.pt'):
            params.load(args.weight)
        elif 'pruned' in args.weight:
            params.network.load_pruned(args.weight, strict=False)
        else:
            params.network.load(args.weight, strict=False)

    background_builder = None
    if args.bg_builder:
        log.info("Online background construction enabled")
        params.use_background_image = False     # we build background image online
        update_interval = 25
        background_builder = utils.BackgroundImageBuilder(update_interval=update_interval)

    # Select dataset
    if hasattr(params, 'temporal_size'):
        # Its a video network, load the video dataset
        log.info("Loading video test dataset")
        test_dataset = MlVideoDetectiontestDataset(params, False)
    else:
        # Its a single image network, load image dataset
        log.info("Loading single image test dataset")
        test_dataset = MlDetectionTestData(params)

    # Dataloaders
    test_loader = ln.data.DataLoader(
        test_dataset,
        batch_size = params.mini_batch_size,
        shuffle = False,
        drop_last = False,
        num_workers = args.workers,
        pin_memory = True,
        collate_fn = utils.brambox_collate,
    )

    params.network.eval()
    params.to(device)

    res = utils.test_detector(params.network, test_loader, device, params.post,
                              params.loss, return_input_data=args.input_data, background_builder=background_builder)
    input_data = None
    if args.input_data:
        anno, det, loss, input_data = res
    else:
        anno, det, loss = res

    ap, thresh, precision, recall, f1 = utils.find_optimal_working_point(anno, det, plot_pr=args.visual)
    log.info(f"Model: {args.weight}")
    loss_values = 'Loss: '
    for name, value in loss.items():
        loss_values += f'{name}:{value:.3f}, '
    log.info(loss_values)

    log.info(f'AP[0.50]: {100*ap:.2f}%, ')
    log.info(f'Optimal working point:')
    log.info(f'    thresh: {100*thresh:.2f}%, ')
    log.info(f'    Precision: {100*precision:.2f}%, ')
    log.info(f'    Recall: {100*recall:.2f}%, ')
    log.info(f'    F1: {100*f1:.2f}%')

    # Save detections
    if args.output:
        det.to_parquet(args.output)

    if args.visual:
        det = det[det['confidence'] >= args.thresh]
        visualize(anno, det, input_data, False, args.output_video)

