import brambox as bb
import cv2
import numpy as np
import utils

root = 'data'

def read_image(img_id):
    img_path = f'{root}/image/data/images/{img_id}.tiff'
    print(img_path)
    img = cv2.imread(img_path, -1)
    if img is None:
        print('Image', img_path, 'not found')
    img = utils.apply_agc_linear(img, 17, 40)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img


# Register this parser, so that it is known to brambox
bb.io.register_parser('ml_detection', utils.MlDetectionParser)
bb.io.register_parser('supervisely', utils.SuperviselyParser)

boxes = bb.io.load('anno_ml_detection', root + '/image/data/labels')
#boxes = bb.io.load('anno_supervisely', root + '/supervisely/images_norm_png/ann')

# Visualize
drawer = bb.util.BoxDrawer(
    images=read_image,
    boxes=boxes,
    size=1,
    method=bb.util.DrawMethod.CV
)

i=0
while True:
    img = drawer[i]
    img = cv2.resize(img, (0,0), fx=8.0, fy=8.0)
    cv2.imshow("image", img)
    print("image", i, "/", len(drawer))
    k = cv2.waitKey(0)
    if k == ord('f'):
        i += 1
        if i == len(drawer):
            i = 0
    elif k == ord('v'):
        i += 1000
        if i >= len(drawer):
            i = 0
    elif k == ord('d'):
        i -= 1
        if i < 0:
            i = len(drawer) - 1
    elif k == ord('c'):
        i -= 1000
        if i < 0:
            i = len(drawer) - 1
    elif k == 27:
        break
