from lightnet.data.transform.util import BaseTransform, BaseMultiTransform
import lightnet as ln
from torchvision import transforms as tvtf
import numpy as np
import random
import torch
import cv2


### Copied from lightnet with small modification to be able to manually randomize the parameters
class RandomFlip(BaseMultiTransform):
    """ Randomly flip image.

    Args:
        horizontal (Number [0-1]): Chance of flipping the image horizontally
        vertical (Number [0-1], optional): Chance of flipping the image vertically; Default **0**

    Note:
        Create 1 RandomFlip object and use it for both image and annotation transforms.
        This object will save data from the image transform and use that on the annotation transform.
    """
    def __init__(self, horizontal, vertical=0):
        super().__init__()
        self.horizontal = horizontal
        self.vertical = vertical
        self.flip_h = False
        self.flip_v = False
        self.im_w = None
        self.im_h = None

    def _randomize_params(self):
        self.flip_h = random.random() < self.horizontal
        self.flip_v = random.random() < self.vertical

    def _tf_pil(self, img):
        self.im_w, self.im_h = img.size

        if self.flip_h and self.flip_v:
            img = img.transpose(Image.ROTATE_180)
        elif self.flip_h:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
        elif self.flip_v:
            img = img.transpose(Image.FLIP_TOP_BOTTOM)

        return img

    def _tf_cv(self, img):
        self.im_h, self.im_w = img.shape[:2]

        if self.flip_h and self.flip_v:
            img = cv2.flip(img, -1)
        elif self.flip_h:
            img = cv2.flip(img, 1)
        elif self.flip_v:
            img = cv2.flip(img, 0)

        return img

    def _tf_torch(self, img):
        if self.flip_h and self.flip_v:
            img = torch.flip(img, (1, 2))
        elif self.flip_h:
            img = torch.flip(img, (2,))
        elif self.flip_v:
            img = torch.flip(img, (1,))

        return img

    def _tf_anno(self, anno):
        anno = anno.copy()

        if self.flip_h and self.im_w is not None:
            anno.x_top_left = self.im_w - anno.x_top_left - anno.width
            if 'motion_x' in anno:
                anno.motion_x = -anno.motion_x
        if self.flip_v and self.im_h is not None:
            anno.y_top_left = self.im_h - anno.y_top_left - anno.height
            if 'motion_y' in anno:
                anno.motion_y = -anno.motion_y

        return anno


### Copied from lightnet with small modification to be able to manually randomize the parameters
class RandomHSV(BaseTransform):
    """ Perform random HSV shift on the RGB data.

    Args:
        hue (Number): Random number between -hue,hue is used to shift the hue
        saturation (Number): Random number between 1,saturation is used to shift the saturation; 50% chance to get 1/dSaturation in stead of dSaturation
        value (Number): Random number between 1,value is used to shift the value; 50% chance to get 1/dValue in stead of dValue

    Warning:
        If you use OpenCV as your image processing library, make sure the image is RGB before using this transform.
        By default OpenCV uses BGR, so you must use `cvtColor`_ function to transform it to RGB.

    .. _cvtColor: https://docs.opencv.org/3.4/d8/d01/group__imgproc__color__conversions.html#ga397ae87e1288a81d2363b61574eb8cab
    """
    def __init__(self, hue, saturation, value):
        super().__init__()
        self.hue = hue
        self.saturation = saturation
        self.value = value

    def _randomize_params(self):
        self.dh = random.uniform(-self.hue, self.hue)

        self.ds = random.uniform(1, self.saturation)
        if random.random() < 0.5:
            self.ds = 1 / self.ds

        self.dv = random.uniform(1, self.value)
        if random.random() < 0.5:
            self.dv = 1 / self.dv

    def _tf_cv(self, img):
        img = img.astype(np.float32) / 255.0
        img = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

        img[:, :, 0] = self.wrap_hue(img[:, :, 0] + (360.0 * self.dh))
        img[:, :, 1] = np.clip(self.ds * img[:, :, 1], 0.0, 1.0)
        img[:, :, 2] = np.clip(self.dv * img[:, :, 2], 0.0, 1.0)

        img = cv2.cvtColor(img, cv2.COLOR_HSV2RGB)
        img = (img * 255).astype(np.uint8)
        return img

    @staticmethod
    def wrap_hue(h):
        h[h >= 360.0] -= 360.0
        h[h < 0.0] += 360.0
        return h


class RandomContrast(BaseTransform):

    def __init__(self, scale=0.0, max_value=255, min_value=0):
        super().__init__()
        self.scale = scale
        self.max_value = max_value
        self.min_value = min_value

    def _randomize_params(self):
        self.rnd = random.randint(0, 1)
        self.factor = random.uniform(1.0 - self.scale, 1.0 + self.scale)

    def _tf_cv(self, image):
        # randomly apply contrast or not
        if not self.rnd:
            return image

        orig_type = image.dtype.type
        im = image.astype(np.int32)
        mean = round(np.mean(im))
        image = np.clip((im - mean) * self.factor + mean,
                        a_min=self.min_value,
                        a_max=self.max_value).astype(orig_type)
        return image


class RandomBrightness(BaseTransform):
    """delta: percent of total pixel value range that can be added/subtracted to each pixel value"""

    def __init__(self, delta=0.0, max_value=255, min_value=0):
        super().__init__()
        assert delta >= 0.0
        assert delta < 1.0
        self.delta = delta
        self.max_value = max_value
        self.min_value = min_value

    def _randomize_params(self):
        self.rnd = random.randint(0, 1)
        self.factor = random.uniform(1.0 - self.delta, 1.0 + self.delta)

    def _tf_cv(self, image):
        # randomly apply contrast or not
        if not self.rnd:
            return image

        offset = (self.factor - 1) * (self.max_value - self.min_value)
        orig_type = image.dtype.type
        im = image.astype(np.int32)
        image = np.clip(im + offset,
                        a_min=self.min_value,
                        a_max=self.max_value).astype(orig_type)
        return image


class RandomGaussianNoise(BaseTransform):
    """Apply gaussian noise with random sigma"""

    def __init__(self, level=0.0, max_value=255, min_value=0):
        super().__init__()
        self.level = level
        self.sigma = 0.0
        self.max_value = max_value
        self.min_value = min_value

    def _randomize_params(self):
        self.sigma = random.uniform(0, self.level)

    def _tf_cv(self, image):
        noise = np.random.normal(loc=0.0, scale=self.sigma*(self.max_value - self.min_value),
                                 size=image.shape).astype(image.dtype.type)
        image = np.clip(image + noise, self.min_value, self.max_value)
        return image


class ToTensor(BaseTransform):
    """Convert a ``PIL.Image`` or ``numpy.ndarray`` to tensor.
    Converts a PIL.Image or numpy.ndarray (H x W x C) in the range
    [0, 255] to a torch.FloatTensor of shape (C x H x W) in the range [0.0, 1.0].
    """

    def __init__(self, mean, std):
        super().__init__()
        self.mean = mean
        self.std = std
        assert self.std > 0

    def _tf_cv(self, image):
        # add channel dimension if not present (grayscale images for instance)
        if len(image.shape) == 2:
            image = np.expand_dims(image, axis=2)

        # convert to torch tensor
        img = torch.from_numpy(image.transpose((2, 0, 1)))

        # normalise data according to given mean and std
        return img.float().sub(self.mean).div(self.std)


class Normalize:
    """Normalize an tensor image with mean and standard deviation.
    Given mean: (R, G, B) and std: (R, G, B),
    will normalize each channel of the torch.*Tensor, i.e.
    channel = (channel - mean) / std
    Args:
        mean (sequence): Sequence of means for R, G, B channels respecitvely.
        std (sequence): Sequence of standard deviations for R, G, B channels
            respecitvely.
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
        Returns:
            Tensor: Normalized image.
        """
        for t, m, s in zip(tensor, self.mean, self.std):
            t.sub_(m).div_(max(s, 0.000001))
        return tensor


def create_spatial_transform(params, augment):
    """Make spatial transformation
    """
    if params.color:
        def to_rgb(img):
            if len(img.shape) == 2:
                return cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        common_transform = [
                        ln.data.transform.Letterbox(dimension=params.input_dimension),
                        ln.data.transform.FitAnno(filter_threshold=0.01),
                        tvtf.ToTensor()
                        ]
        transform = ln.data.transform.Compose([to_rgb] + common_transform)
        if augment:
            transform = ln.data.transform.Compose([
                        to_rgb,
                        RandomHSV(params.hue, params.saturation, params.value),
                        RandomFlip(params.hflip, params.vflip)
                        ] + common_transform)

    else:
        common_transform = [
                        ln.data.transform.Letterbox(dimension=params.input_dimension),
                        ln.data.transform.FitAnno(filter_threshold=0.01),
                        ToTensor(params.mean, params.std),
                        ]

        transform = ln.data.transform.Compose(common_transform)
        if augment:
            transform = ln.data.transform.Compose([
                        RandomGaussianNoise(params.noise, params.max_value, params.min_value),
                        RandomContrast(params.contrast, params.max_value, params.min_value),
                        RandomBrightness(params.brightness, params.max_value, params.min_value),
                        RandomFlip(params.hflip, params.vflip)
                        ] + common_transform)
    return transform


