import torch
import sys
import models
import lightnet as ln
import copy
import ptflops
import argparse

ln.prune.dependency.add_nodetype_operation('replication_pad2d', ln.prune.dependency.NodeType.IGNORE)

parser = argparse.ArgumentParser(description='Give MACS and FLOPS stats of pruned network')
parser.add_argument('weight',  help='Path to weight file')
parser.add_argument('network', help='network config file')
args = parser.parse_args()

params = ln.engine.HyperParameters.from_file(args.network)

def calc_stats(params):
    h, w = params.input_dimension
    macs, num_params = ptflops.get_model_complexity_info(params.network, (params.network.input_channels, h, w), as_strings=False,
                                                     print_per_layer_stat=False, verbose=True)
    macs_str = ptflops.flops_counter.flops_to_string(macs, units='MMac')
    num_params_str = ptflops.flops_counter.params_to_string(num_params)

    return macs, num_params, macs_str, num_params_str

print('Original network:')
macs_orig, num_params_orig, macs_str, num_params_str = calc_stats(copy.deepcopy(params))
print('{:<30}  {:<8}'.format('Computational complexity: ', macs_str))
print('{:<30}  {:<8}'.format('Number of parameters: ', num_params_str))

print('Pruned network:')
params.network.load_pruned(args.weight)
macs_pruned, num_params_pruned, macs_str, num_params_str = calc_stats(params)
print('{:<30}  {:<8}'.format('Computational complexity: ', macs_str))
print('{:<30}  {:<8}'.format('Number of parameters: ', num_params_str))

print(f"Reduction MACS = {macs_orig / macs_pruned:.2f}")
print(f"Reduction params = {num_params_orig / num_params_pruned:.2f}")
