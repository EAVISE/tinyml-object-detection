import numpy as np
import time
import os
import cv2
import argparse
import serial
import struct
import lightnet as ln
import brambox as bb
from tqdm import tqdm
import logging

from dataset import MlDetectionTestData
import utils

log = logging.getLogger('test_on_target')
logging.basicConfig(level=logging.WARNING)


class TargetDevice:
    def __init__(self, port, baud):
        self.port = port
        self.baud = baud
        #self.detections_payload_fmt = "<iiiif"
        self.detections_payload_fmt = "<fffff"

    def send_image(self, image):
        """Send serialized image over serial port
        """
        with serial.Serial(self.port, self.baud) as ser:
            # serialize image data
            image_data = image.tobytes()

            # send data header: preamble|length
            header = struct.pack("<HH", 0xbeef, len(image_data))
            ser.write(header)

            # write image data
            ser.write(image_data)

    def get_ack(self):
        """Wait until the board acknowledges
        """
        with serial.Serial(self.port, self.baud) as ser:
            pre0 = pre1 = 0
            while pre0 != 0xbe or pre1 != 0xef:
                pre1 = pre0
                pre0 = ser.read(1)[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run test images on target')
    parser.add_argument('network', help='network config file')
    parser.add_argument('port', help='Serial port the device is connected too')
    parser.add_argument('--baud', help='Baudrate', type=int, default=115200)
    args = parser.parse_args()

    device = TargetDevice(args.port, args.baud)

    params = ln.engine.HyperParameters.from_file(args.network)

    # disable normalization and frame diff since this is all done on the target device
    params.mean = 0.0
    params.std = 1.0
    params.diff_channel_stride = 0

    test_dataset = MlDetectionTestData(params, False)

    data_loader = ln.data.DataLoader(
        test_dataset,
        batch_size = 1,
        shuffle = False,
        drop_last = False,
        num_workers = 1,
        collate_fn = utils.brambox_collate,
    )

    while True:
        for idx, (data, target) in enumerate(tqdm(data_loader)):

            data = data.numpy().astype(np.int16)
            device.send_image(data)
            device.get_ack()
