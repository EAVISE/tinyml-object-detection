#!/usr/bin/env python
import os
import logging
import time
import argparse
from math import isinf, isnan
from statistics import mean
import torch
import random
import torchvision
import numpy as np
import lightnet as ln
from lightnet.engine import Engine
from dataset import MlDetectionTrainData, MlDetectionValidData, MlVideoDetectionTrainData, MlVideoDetectionValidData
from torch.utils.tensorboard import SummaryWriter
import utils
import loss as loss_fn

torch.set_num_threads(8)
log = logging.getLogger('lightnet.MlDetection.train')


class TrainEngine(Engine):
    def start(self):
        self.params.to(self.device)
        self.optimizer.zero_grad()

        self.best_acc = 0.0
        self.best_loss = 10000
        self.validation_loss = 100

        self.train_loss = {}

    def process_batch(self, data):
        data, target = data
        data = data.to(self.device)

        out = self.network(data)
        self.latest_data = data.clone()

        # combine batch and temporal dims if temporal dim present
        if out.dim() == 5:
            # (B x T x C x H x W) -> (B*T x C x H x W)
            out = utils.squash_dims(out, (0, 1))
            # combine batch and frame numbers on target too
            seq_length = data.size(1)
            target['batch_number'] = target['frame_number'] + target['batch_number'] * seq_length
            # select only one batch sample in this case
            self.latest_data = self.latest_data[0]

        loss = self.loss(out, target) / self.batch_subdivisions
        loss.backward()
        if hasattr(self.network, 'temporal'):
            self.network.temporal.detach_hidden_state()

        if hasattr(self, 'enable_memory_step') and self.batch >= self.enable_memory_step and not self.network.enable_memory:
            log.info('Enabling heatmap memory')
            self.network.enable_memory = True

        # for logging
        for name, value in self.loss.values.items():
            if torch.is_tensor(value):
                value = value.item()

            if name not in self.train_loss:
                self.train_loss[name] = [value]
            else:
                self.train_loss[name] += [value]

    def train_batch(self):
        self.optimizer.step()
        self.optimizer.zero_grad()

        # workaround to provide the validation_loss to the scheduler step method,
        # if it changes to ReduceLROnPLateau
        sched = self.scheduler._get_scheduler(self.scheduler.last_epoch + 1)
        if isinstance(sched, torch.optim.lr_scheduler.ReduceLROnPlateau):
            self.scheduler.step(metrics=self.validation_loss)
        else:
            self.scheduler.step()

        # Get values from last batch
        loss_values = ''
        for name, values in self.train_loss.items():
            value = mean(values[-self.batch_subdivisions:])
            loss_values += f'{name}:{value:.3f} '
        self.log(f'{self.batch} | {loss_values}')

        if isinf(self.loss.loss_total) or isnan(self.loss.loss_total):
            log.error('Infinite loss')
            self.sigint = True
            return

    def resize(self):
        self.dataloader.change_input_dim(multiple=self.network.stride, random_range=self.train_dim_range)

    @Engine.batch_end(50)
    def plot(self):
        for name, values in self.train_loss.items():
            value = mean(values)
            self.writer.add_scalar(f'Train/Loss_{name}', value, self.batch)

        self.train_loss = {}
        self.writer.add_scalar('Train/LearningRate', self.optimizer.param_groups[0]['lr'], self.batch)

    @Engine.batch_end(50)
    def plot_input_data(self):
        def normalise(x):
            """normalise data between 0 and 1
            """
            minimum = x.min()
            maximum = x.max()
            return (x - minimum) / (maximum - minimum)

        # plot first dim (batch or time) in separate columns and the different channels (if any) in separate rows
        data = self.latest_data.permute(1, 0, 2, 3).reshape(-1, 1, *self.latest_data.shape[-2:])
        grid = torchvision.utils.make_grid(data, nrow=self.latest_data.size(0), normalize=True, scale_each=True)
        self.writer.add_image('input_data', grid, self.batch)

    @Engine.batch_end(1000)
    def test(self):
        if isinstance(self.loss, loss_fn.HeatmapLoss):
            self.test_points()
        else:
            self.test_bboxes()

    def test_bboxes(self):
        log.info('Start testing')
        anno, det, loss = utils.test_detector(self.network, self.valid_loader, self.device, self.params.post, self.params.loss, self)
        self.sigint = False

        self.network.train()
        self.loss.train()

        # stop in case ctrl-C was detected
        if anno is None:
            return

        self.validation_loss = loss['total']
        ap = utils.calculate_ap(anno, det)
        loss_values = 'Loss: '
        for name, value in loss.items():
            loss_values += f'{name}:{value:.3f}, '
            self.writer.add_scalar(f'Test/Loss_{name}', value, self.batch)
        self.log(loss_values)
        log.info(f'AP[0.50]: {ap:.2f}%')
        self.writer.add_scalar('Test/AP', ap, self.batch)

        if ap > self.best_acc:
            self.params.save(os.path.join(self.checkpoints_dir_name, 'best.state.pt'))
            self.log(f'Saved training state, best accuracy up until now')
            self.best_acc = ap

    def test_points(self):
        log.info('Start testing')
        loss = utils.test_point_detector(self.network, self.valid_loader, self.device, self.params.post, self.params.loss, self)
        self.sigint = False

        self.network.train()
        self.loss.train()
        self.validation_loss = loss

        log.info(f'loss_tot: {loss:.2f}')

        self.writer.add_scalar('Test/Loss_heatmap', loss, self.batch)

        if loss < self.best_loss:
            self.params.save(os.path.join(self.checkpoints_dir_name, 'best.state.pt'))
            self.log(f'Saved training state, lowest test loss up until now')
            self.best_loss = loss

    def quit(self):
        return self.batch >= self.max_batches or self.sigint


def seed_worker(worker_id):
    """Used for enabling deterministic behaviour (see https://pytorch.org/docs/stable/notes/randomness.html)
    """
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train network')
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file')
    parser.add_argument('-l', '--logdir', help='Logging folder', default='./logs')
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-w', '--workers', type=int, help='Number of dataloader workers', default=8)
    parser.add_argument('-s', '--cross_split', type=str, help='Provide data cross split folder', default="")
    parser.add_argument('-d', '--deterministic', action='store_true', help='Enable deterministic training')
    parser.add_argument('-m', '--manual_seed', type=int, help='Set manual seed value for random generators', default=0)
    args = parser.parse_args()

    # Parse arguments
    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    if args.deterministic:
        log.info('Deterministic training enabled')
        torch.manual_seed(args.manual_seed)
        torch.cuda.manual_seed(args.manual_seed)
        np.random.seed(args.manual_seed)
        random.seed(args.manual_seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    # Prepare logging directory
    tb_dir_name, checkpoints_dir_name = utils.prepare_logdir(args)

    params = ln.engine.HyperParameters.from_file(args.network)
    if args.weight is not None:
        if args.weight.endswith('.state.pt'):
            params.load(args.weight)
        elif 'pruned-' in args.weight:
            params.network.load_pruned(args.weight)
        else:
            params.network.load(args.weight, strict=False)

    # select cross split if applicable
    if args.cross_split:
        params.train_set = params.train_set%args.cross_split
        params.valid_set = params.valid_set%args.cross_split
        log.info(f"Selecting cross split training set {params.train_set} and validation set {params.valid_set}")

    # Select dataset
    if hasattr(params, 'temporal_size'):
        # Its a video network, load the video dataset
        log.info("Loading video train/valid dataset")
        train_dataset = MlVideoDetectionTrainData(params)
        valid_dataset = MlVideoDetectionValidData(params)
    else:
        # Its a single image network, load image dataset
        log.info("Loading single image train/valid dataset")
        train_dataset = MlDetectionTrainData(params)
        valid_dataset = MlDetectionValidData(params)

    log.info('Creating dataset loaders')

    # Dataloaders
    train_loader = ln.data.DataLoader(
        train_dataset,
        batch_size = params.mini_batch_size,
        shuffle = True,
        drop_last = True,
        num_workers = args.workers,
        pin_memory = True,
        worker_init_fn=seed_worker,
        collate_fn = utils.brambox_collate,
    )

    valid_loader = ln.data.DataLoader(
        valid_dataset,
        batch_size = params.mini_batch_size,
        shuffle = True,
        drop_last = True,
        num_workers = args.workers,
        pin_memory = True,
        collate_fn = utils.brambox_collate,
    )

    # Start training
    log.info('Starting train engine')
    eng = TrainEngine(
        params,
        train_loader,
        valid_loader=valid_loader,
        device=device,
        checkpoints_dir_name=checkpoints_dir_name,
        writer=SummaryWriter(tb_dir_name)
    )
    b1 = eng.batch
    t1 = time.time()
    eng()
    t2 = time.time()
    b2 = eng.batch
    log.info(f'Training {b2-b1} batches took {t2-t1:.2f} seconds [{(t2-t1)/(b2-b1):.3f} sec/batch]')
