import numpy as np
import os
import cv2
import torch
import argparse
import lightnet as ln
import brambox as bb
import logging
import pandas as pd
import tensorflow as tf
from tqdm import tqdm

from dataset import MlDetectionValidData
import utils

log = logging.getLogger('test_on_target')
logging.basicConfig(level=logging.WARNING)


def show_bboxes(det, anno):
    """Display annos and ground truth using brambox visualization
    """
    # display detections and annotations in different colors
    det['color'] = [(31, 119, 180)] * len(det)
    anno['color'] = [(255, 127, 14)] * len(anno)
    boxes = bb.util.concat([anno, det], ignore_index=True, sort=False)

    def read_image(img_id):
        img_path = os.path.join('data', 'image', 'data', 'images', img_id + '.tiff')
        print(img_path)
        img = cv2.imread(img_path, -1)
        img = utils.apply_agc(img)
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        return img

    # Visualize
    drawer = bb.util.BoxDrawer(
        images=read_image,
        boxes=boxes,
        size=1,
        method=bb.util.DrawMethod.CV
    )

    i=0
    while True:
        img = drawer[i]
        img = cv2.resize(img, (320, 240))
        cv2.imshow("image", img)
        print("image", i, "/", len(drawer))
        k = cv2.waitKey(0)
        if k == ord('d'):
            i += 1
            if i == len(drawer):
                i = 0
        elif k == ord('a'):
            i -= 1
            if i < 0:
                i = len(drawer) - 1
        elif k == 27:
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run test images on target')
    parser.add_argument('network', help='network config file')
    parser.add_argument('tflite', help='.tflite model file')
    parser.add_argument('--view_thresh', help='Detection threshold', type=float, default=0.5)
    parser.add_argument('--view', help='View detections rather than calculating AP metrics', action="store_true")
    parser.add_argument('-s', '--cross_split', type=str, help='Provide data cross split folder', default="")
    args = parser.parse_args()

    params = ln.engine.HyperParameters.from_file(args.network)

    # select cross split if applicable
    if args.cross_split:
        params.test_set = params.test_set%args.cross_split
    print(params.test_set)

    # disable normalization and frame diff since we do this ourselves
    data_mean = params.mean
    data_std = params.std
    params.mean = 0.0
    params.std = 1.0
    params.diff_channel_stride = 0

    valid_dataset = MlDetectionValidData(params, False)

    data_loader = ln.data.DataLoader(
        valid_dataset,
        batch_size = 1,
        shuffle = False,
        drop_last = False,
        num_workers = 1,
        collate_fn = utils.brambox_collate,
    )

    # Infer with tflite
    tflite_model = tf.lite.Interpreter(args.tflite)
    tflite_model.allocate_tensors()
    input_details = tflite_model.get_input_details()
    output_details = tflite_model.get_output_details()

    quant = False
    if 'quantization' in input_details[0]:
        print("Model with quantization")
        quant = True

    det = []
    anno = []
    frame_queue = []
    for idx, (data, target) in enumerate(tqdm(data_loader)):

        # NCHW to NHWC and to numpy
        tf_data = data.permute(0, 2, 3, 1).numpy()

        # Convert data to int16
        tf_data = tf_data.astype(np.int16)

        # get a frame of 10 samples ago
        frame_queue.append(tf_data)
        if len(frame_queue) == 11:
            tf_data_prev = frame_queue.pop(0)
        else:
            tf_data_prev = frame_queue[0]

        if quant:
            q_scale = input_details[0]['quantization'][0]
            q_zero = input_details[0]['quantization'][1]
            scale = data_std * q_scale
            tf_data = ((tf_data - data_mean) / float(scale) + q_zero)
            diff = (tf_data -((tf_data_prev - data_mean) / float(scale))).astype(int)
            tf_data = np.clip(tf_data.astype(int), -128, 127).astype(np.int8)
            diff = np.clip(diff, -128, 127).astype(np.int8)
        else:
            # Normalize and concat frames
            tf_data = (tf_data - data_mean) / float(data_std)
            tf_data_prev = (tf_data_prev - data_mean) / float(data_std)
            diff = tf_data - tf_data_prev

        tf_data = np.concatenate((tf_data, diff), axis=-1)

        # Infer TFLite
        tflite_model.set_tensor(input_details[0]['index'], tf_data)
        tflite_model.invoke()
        tf_out = tflite_model.get_tensor(output_details[0]['index'])

        if quant:
            q_scale = output_details[0]['quantization'][0]
            q_zero = output_details[0]['quantization'][1]
            tf_out = (tf_out.astype(np.int32) - q_zero) * q_scale

        # numpy to torch tensor and NHWC to NCHW
        output = torch.from_numpy(tf_out).permute(0, 3, 1, 2).contiguous()

        # Post process
        # NOTE: a slight difference in accuracy can be observed due to a difference in order in
        # which the raw detections are sent to the NMS algorithm.
        # in lightnet, the detections are traversed in NCHW order, while the real target traverses
        # the detections in NHWC order.
        output = params.post(output)
        output.image = pd.Categorical.from_codes(output.image, categories=target.image.cat.categories)
        det.append(output)
        anno.append(target)

    det = bb.util.concat(det, ignore_index=True, sort=False)
    anno = bb.util.concat(anno, ignore_index=True, sort=False)

    if args.view:
        det = det[det.confidence >= args.view_thresh]
        show_bboxes(det, anno)
    else:
        coco = bb.eval.COCO(det, anno, tqdm=True)
        coco.compute(map_50=True, map_75=True, map_coco=True)
        map_50 = coco.mAP_50 * 100
        map_75 = coco.mAP_75 * 100
        map_coco = coco.mAP_coco * 100
        print(f'mAP[0.50]:            {map_50:.2f}%')
        print(f'mAP[0.75]:            {map_75:.2f}%')
        print(f'mAP[0.50:0.95:0.05]:  {map_coco:.2f}%')
