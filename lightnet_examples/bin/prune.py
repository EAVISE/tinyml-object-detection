#!/usr/bin/env python
import datetime
import os
import sys
import logging
import time
import argparse
from math import isinf, isnan
from statistics import mean
import torch
import random
import torchvision
import numpy as np
import pandas as pd
from tqdm import tqdm
import lightnet as ln
import brambox as bb
from dataset import *
from torch.utils.tensorboard import SummaryWriter

torch.set_num_threads(8)
log = logging.getLogger('lightnet.MlDetection.prune')

ln.prune.dependency.add_nodetype_operation('replication_pad2d', ln.prune.dependency.NodeType.IGNORE)
ln.prune.dependency.add_nodetype_operation('hardtanh', ln.prune.dependency.NodeType.IGNORE)


class PruneEngine(ln.engine.Engine):
    def start(self):
        self.params.reset()
        self.params.to(self.device)

        # Prune
        self.finetune_success = False
        self.stop_pruning_loss  = self.original_loss * self.stop_pruning_loss_scale
        self.stop_finetune_loss  = self.original_loss * self.stop_finetune_loss_scale
        self.pruner(*self.prune_percentage)
        self.num_pruned = self.pruner.hard_pruned_channels
        log.info(f'[{self.prune_step:02d}] Pruned {self.num_pruned} filters')
        self.best_loss = {'total': 100}
        self.test()

        # Reset optimizer and scheduler
        self.scheduler.last_epoch = -1
        for group in self.optimizer.param_groups:
            group['lr'] = group['initial_lr']
        self.optimizer.zero_grad()

        # Setup training
        self.train_loss = {}

    def process_batch(self, data):
        data, target = data
        data = data.to(self.device)

        out = self.network(data)
        loss = self.loss(out, target) / self.batch_subdivisions
        loss.backward()

        # for logging
        for name, value in self.loss.values.items():
            if torch.is_tensor(value):
                value = value.item()

            if name not in self.train_loss:
                self.train_loss[name] = [value]
            else:
                self.train_loss[name] += [value]

    def train_batch(self):
        self.optimizer.step()
        self.optimizer.zero_grad()
        self.scheduler.step()

        # Get values from last batch
        loss_values = ''
        for name, values in self.train_loss.items():
            value = mean(values[-self.batch_subdivisions:])
            loss_values += f'{name}:{value:.3f} '
        self.log(f'{self.batch} | {loss_values}')
        self.train_loss = {}

        if isinf(self.loss.loss_total) or isnan(self.loss.loss_total):
            log.error('Infinite loss')
            self.sigint = True
            return

    @ln.engine.Engine.batch_end(500)
    def test(self):
        log.info('Start testing')
        anno, det, loss = utils.test_detector(self.network, self.valid_loader, self.device, self.params.post, self.params.loss, self)
        self.sigint = False

        self.network.train()
        self.loss.train()

        # stop in case ctrl-C was detected
        if anno is None:
            return

        ap = utils.calculate_ap(anno, det)
        loss_values = 'Loss: '
        for name, value in loss.items():
            loss_values += f'{name}:{value:.3f}, '
        self.log(loss_values)
        log.info(f'AP[0.50]: {ap:.2f}%')

        if loss['total'] < self.best_loss['total']:
            self.params.network.save(self.filename)
            self.log(f'Saved checkpoint, lowest loss up until now with current pruning configuration')
            self.best_loss = loss
            self.valid_ap = ap

    def quit(self):
        quit_pruning = True
        if self.num_pruned <= self.min_prune:
            log.info(f'Reached {self.num_pruned} filters, stopping pruning')
        elif self.best_loss['total'] <= self.stop_finetune_loss:
            log.info(f"Reached lowest loss of {self.best_loss['total']:.3f} <= {self.stop_finetune_loss:.3f}, finetune succeeded")
            self.finetune_success = True
        elif self.batch >= self.max_batches:
            if self.best_loss['total'] <= self.stop_pruning_loss:
                self.finetune_success = True
                log.info(f'Reached {self.max_batches} batches, loss target achieved, finetune succeeded')
            else:
                log.info(f'Reached {self.max_batches} batches, loss target not achieved, finetune failed')
        elif self.sigint:
            pass
        else:
            quit_pruning = False

        if quit_pruning:
            # Log to tensorboard
            self.writer.add_scalar('Prune/pruned_filers', self.num_pruned, self.prune_step)
            for name, value in self.best_loss.items():
                self.writer.add_scalar(f'Validation/Loss_{name}', value, self.prune_step)
            self.writer.add_scalar('Validation/AP', self.valid_ap, self.prune_step)

        return quit_pruning


def seed_worker(worker_id):
    """Used for enabling deterministic behaviour (see https://pytorch.org/docs/stable/notes/randomness.html)
    """
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Train network',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('weight', help='Path to weight file')
    parser.add_argument('-n', '--network', help='network config file', required=True)
    parser.add_argument('-l', '--logdir', help='Logging folder', default='./logs')
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-p', '--percentage', help='Pruning percentage stepsize', type=float, nargs='+')
    parser.add_argument('--minprune', help='Stop when pruning percentage results in less than X number of filters', type=int, default=1)
    parser.add_argument('-w', '--workers', type=int, help='Number of dataloader workers', default=8)
    parser.add_argument('-d', '--deterministic', action='store_true', help='Enable deterministic training')
    parser.add_argument('-m', '--manual_seed', type=int, help='Set manual seed value for random generators', default=0)
    args = parser.parse_args()

    # Parse arguments
    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    if args.deterministic:
        log.info('Deterministic training enabled')
        torch.manual_seed(args.manual_seed)
        torch.cuda.manual_seed(args.manual_seed)
        np.random.seed(args.manual_seed)
        random.seed(args.manual_seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    # Prepare logging
    tb_dir_name, checkpoints_dir_name = utils.prepare_logdir(args)
    writer = SummaryWriter(tb_dir_name)

    params = ln.engine.HyperParameters.from_file(args.network)
    if args.weight is not None:
        if args.weight.endswith('.state.pt'):
            params.load(args.weight)
        elif 'pruned-' in args.weight:
            params.network.load_pruned(args.weight)
        else:
            params.network.load(args.weight)
    else:
        log.warn('No weights were given, starting with random weights!')

    log.info('Creating datasets')

    # Dataloaders
    train_loader = ln.data.DataLoader(
        MlDetectionTrainData(params),
        batch_size = params.mini_batch_size,
        shuffle = True,
        drop_last = True,
        num_workers = args.workers,
        pin_memory = True,
        worker_init_fn=seed_worker,
        collate_fn = utils.brambox_collate,
    )

    valid_loader = ln.data.DataLoader(
        MlDetectionValidData(params),
        batch_size = params.mini_batch_size,
        shuffle = True,
        drop_last = True,
        num_workers = args.workers,
        pin_memory = True,
        collate_fn = utils.brambox_collate,
    )

    # Original validation loss
    log.info(f'Validating for original validation loss value')
    params.network.to(device)
    anno, det, loss = utils.test_detector(params.network, valid_loader, device, params.post, params.loss)
    ap = utils.calculate_ap(anno, det)
    loss_values = 'Loss: '
    for name, value in loss.items():
        loss_values += f'{name}:{value:.3f}, '
    log.info(f'Original network accuracy:')
    log.info(loss_values)
    log.info(f'AP[0.50]: {ap:.2f}%')

    # Log to tensorboard
    for name, value in loss.items():
        writer.add_scalar(f'Validation/Loss_{name}', value, 0)
    writer.add_scalar('Validation/AP', ap, 0)

    # Engine
    engine = PruneEngine(
        params, train_loader,
        prune_percentage=args.percentage,
        min_prune=args.minprune,
        valid_loader=valid_loader,
        original_loss=loss['total'],
        device=device,
        writer=writer,
    )

    # Pruning
    prune_step = 0
    prunable_channels_start = engine.pruner.prunable_channels
    prev_filename = None
    tt1 = time.time()

    while True:
        prune_step += 1
        engine.prune_step = prune_step
        engine.filename = os.path.join(checkpoints_dir_name, f'pruned-{prune_step:02d}.pt')

        if prev_filename is not None:
            # load best weights from previous prune step
            params.network.load(prev_filename)

        t1 = time.time()
        engine()
        t2 = time.time()

        percentage = engine.pruner.prunable_channels/prunable_channels_start*100
        log.info(f'Pruning step {prune_step:02d} [percentage={percentage:.02f}, status={engine.finetune_success}, time={t2-t1:.2f}s]')
        writer.add_scalar('Prune/percentage', percentage, prune_step)
        if not engine.finetune_success:
            break

        prev_filename = engine.filename

    tt2 = time.time()
    log.info(f'{prune_step} prunings took {tt2-tt1:.2f} seconds [{(tt2-tt1)/(prune_step):.3f} sec/pruning]')
