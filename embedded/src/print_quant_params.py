import sys
import tensorflow as tf


tflite_model = tf.lite.Interpreter(sys.argv[1])
tflite_model.allocate_tensors()
input_details = tflite_model.get_input_details()
output_details = tflite_model.get_output_details()

q_scale = input_details[0]['quantization'][0]
q_zero = input_details[0]['quantization'][1]
print("Input q scale =", q_scale)
print("Input q zero =", q_zero)

q_scale = output_details[0]['quantization'][0]
q_zero = output_details[0]['quantization'][1]
print("Output q scale =", q_scale)
print("Output q zero =", q_zero)
