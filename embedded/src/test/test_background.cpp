#include "gtest/gtest.h"
#include "background.h"
#include <iostream>
#include <cstring>
#include <algorithm>


template <typename T>
std::string image_data_to_string(const T* image_data, int width, int height)
{
    std::stringstream ss;

    for (int y=0; y<height; y++) {
        for (int x=0; x<width; x++) {
            ss << static_cast<int>(image_data[y*width + x]) << " ";
        }
        ss << std::endl;
    }

    return ss.str();
}

template <typename T>
void expect_eq_image_data(const T* expected, const T* actual, int width, int height)
{
    if (std::memcmp(expected, actual, width*height) != 0) {
        EXPECT_TRUE(false) << "Expected:" << std::endl << image_data_to_string<T>(expected, width, height)
                           << "Actual:" << std::endl << image_data_to_string<T>(actual, width, height);
    }
}

class TestCreateBboxMask : public testing::Test {
protected:
    struct Tensor input;
    struct Detection detections[2];
    constexpr static int k_w = 15;
    constexpr static int k_h = 10;
    uint8_t mask[k_w*k_h];
    float thresh = 0.1;

    virtual void SetUp()
    {
        input.w = k_w;
        input.h = k_h;

        memset(detections, 0, sizeof(detections));
        detections[0].conf = 0.5;
        detections[1].conf = 0.5;
    }

    virtual void TearDown()
    {
    }
};

TEST_F(TestCreateBboxMask, mask_within_image_bounds)
{
    // setup
    detections[0].x = 2.6;
    detections[0].y = 4.1;
    detections[0].w = 5.3;
    detections[0].h = 3.9;

    // act
    background_create_bbox_mask(&input, detections, 1, 1, thresh, mask);

    // assert
    const uint8_t expected_mask[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    expect_eq_image_data<uint8_t>(expected_mask, mask, k_w, k_h);
}

TEST_F(TestCreateBboxMask, mask_top_left_outside_image_bounds)
{
    // setup
    detections[0].x = -2.6;
    detections[0].y = -1.1;
    detections[0].w = 5.3;
    detections[0].h = 3.9;

    // act
    background_create_bbox_mask(&input, detections, 1, 1, thresh, mask);

    // assert
    const uint8_t expected_mask[] = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    expect_eq_image_data<uint8_t>(expected_mask, mask, k_w, k_h);
}

TEST_F(TestCreateBboxMask, mask_bottom_right_outside_image_bounds)
{
    // setup
    detections[0].x = k_w-3.0;
    detections[0].y = k_h-2.0;
    detections[0].w = k_w+7.0;
    detections[0].h = k_h+6.0;

    // act
    background_create_bbox_mask(&input, detections, 1, 1, thresh, mask);

    // assert
    const uint8_t expected_mask[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1};

    expect_eq_image_data<uint8_t>(expected_mask, mask, k_w, k_h);
}

TEST_F(TestCreateBboxMask, mask_multiple_boxes)
{
    // setup
    detections[0].x = 3.0;
    detections[0].y = 2.0;
    detections[0].w = 6.0;
    detections[0].h = 5.0;

    detections[1].x = 6.0;
    detections[1].y = 5.0;
    detections[1].w = 5.0;
    detections[1].h = 4.0;

    // act
    background_create_bbox_mask(&input, detections, 2, 1, thresh, mask);

    // assert
    const uint8_t expected_mask[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                                     0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                                     0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0};

    expect_eq_image_data<uint8_t>(expected_mask, mask, k_w, k_h);
}

TEST_F(TestCreateBboxMask, mask_detection_below_thresh)
{
    // setup
    detections[0].x = 3.0;
    detections[0].y = 2.0;
    detections[0].w = 6.0;
    detections[0].h = 5.0;
    detections[0].conf = 0.0;

    // act
    background_create_bbox_mask(&input, detections, 1, 1, thresh, mask);

    // assert
    const uint8_t expected_mask[k_w * k_h] = {0};

    expect_eq_image_data<uint8_t>(expected_mask, mask, k_w, k_h);
}

class TestBackgroundUpdate : public testing::Test {
protected:
    struct Detection detections[2];
    constexpr static int k_w = 15;
    constexpr static int k_h = 10;

    struct Tensor input;
    struct Tensor background;
    int16_t input_data[k_w * k_h];
    int16_t background_data[k_w * k_h];
    float thresh = 0.1;

    virtual void SetUp()
    {
        input.w = k_w;
        input.h = k_h;
        input.data.int16 = input_data;
        std::fill_n(input_data, k_w * k_h, 20);

        background.w = k_w;
        background.h = k_h;
        background.data.int16 = background_data;
        std::fill_n(background_data, k_w * k_h, 10);

        memset(detections, 0, sizeof(detections));
        detections[0].conf = 0.5;
        detections[1].conf = 0.5;
    }

    virtual void TearDown()
    {
    }
};

TEST_F(TestBackgroundUpdate, background_update_single_box)
{
    // setup
    detections[0].x = 3.0;
    detections[0].y = 2.0;
    detections[0].w = 6.0;
    detections[0].h = 5.0;

    // act
    background_update(&input, detections, 1, 0.9, thresh, &background);

    // assert
    const int16_t expected_background_data[] = {11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11,
                                                11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
                                                11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11};

    expect_eq_image_data<int16_t>(expected_background_data, background_data, k_w, k_h);
}

class TestBackgroundInit : public testing::Test {
protected:
    constexpr static int k_w = 15;
    constexpr static int k_h = 10;

    struct Tensor input;
    struct Tensor background;
    int16_t input_data[k_w * k_h];
    int16_t background_data[k_w * k_h];

    virtual void SetUp()
    {
        input.w = k_w;
        input.h = k_h;
        input.data.int16 = input_data;
        std::fill_n(input_data, k_w * k_h, 20);

        background.w = k_w;
        background.h = k_h;
        background.data.int16 = background_data;
    }

    virtual void TearDown()
    {
        // reset internal state of this function
        background_init(NULL, NULL);
    }
};

TEST_F(TestBackgroundInit, background_init_null)
{
    EXPECT_EQ(-1, background_init(&input, NULL));
    EXPECT_EQ(-1, background_init(NULL, &background));
}

TEST_F(TestBackgroundInit, background_init_count)
{
    for (int i=0; i<BACKGROUND_INIT_STEPS-1; i++) {
        EXPECT_EQ(-1, background_init(&input, &background));
    }
    EXPECT_EQ(0, background_init(&input, &background));
}

TEST_F(TestBackgroundInit, background_init_check_average)
{
    int16_t fill_values[BACKGROUND_INIT_STEPS];
    int16_t expected_background_data[k_w * k_h];

    // setup
    int16_t expected_fill_value = 0;
    for (int i=0; i<BACKGROUND_INIT_STEPS; i++) {
        fill_values[i] = 1000 * i;
        expected_fill_value += fill_values[i];
    }
    expected_fill_value /= BACKGROUND_INIT_STEPS;
    std::fill_n(expected_background_data, k_w * k_h, expected_fill_value);

    // act
    for (int i=0; i<BACKGROUND_INIT_STEPS-1; i++) {
        std::fill_n(input_data, k_w * k_h, fill_values[i]);
        EXPECT_EQ(-1, background_init(&input, &background));
    }
    std::fill_n(input_data, k_w * k_h, fill_values[BACKGROUND_INIT_STEPS-1]);
    EXPECT_EQ(0, background_init(&input, &background));

    // assert
    expect_eq_image_data<int16_t>(expected_background_data, background_data, k_w, k_h);
}
