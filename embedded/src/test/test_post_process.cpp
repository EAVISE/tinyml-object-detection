#include "gtest/gtest.h"
#include "post_process.h"


class TestGetBboxes : public testing::Test {
protected:
    Tensor tensor;
    Anchor anchors[2];
    int8_t tensor_data[10] = {0, 0, 0, 0, -90, 0, 0, 0, 0, -90};

    virtual void SetUp()
    {
        tensor.h = 1;
        tensor.w = 1;
        tensor.c = sizeof(tensor_data);
        tensor.q_zero = 0;
        tensor.q_scale = 3.f/128.f;
        tensor.data.int8 = tensor_data;

        anchors[0].w = 0.2;
        anchors[0].h = 0.1;
        anchors[1].w = 0.2;
        anchors[1].h = 0.1;

    }

    virtual void TearDown()
    {
    }
};

TEST_F(TestGetBboxes, all_detections_below_threshold)
{
    // setup
    struct Detection detections[2];

    // act
    const int res = post_get_bboxes(&tensor, 0.5,
                                    anchors, sizeof(anchors)/sizeof(anchors[0]),
                                    4, detections, sizeof(detections)/sizeof(detections[0]));
    // assert
    EXPECT_EQ(res, 0);
}

TEST_F(TestGetBboxes, more_detections_than_output_buffer_can_handle)
{
    // setup
    struct Detection detections[1];
    tensor_data[4] = 30;    // conf
    tensor_data[9] = 30;    // conf

    // act
    const int res = post_get_bboxes(&tensor, 0.5,
                                    anchors, sizeof(anchors)/sizeof(anchors[0]),
                                    8, detections, sizeof(detections)/sizeof(detections[0]));
    // assert
    EXPECT_EQ(res, 1);
}

TEST_F(TestGetBboxes, single_detection)
{
    // setup
    struct Detection detections[2];
    tensor_data[0] = -50;   // x
    tensor_data[1] = -20;   // y
    tensor_data[2] = 30;    // w
    tensor_data[3] = 80;    // h
    tensor_data[4] = 30;    // conf

    // act
    const int res = post_get_bboxes(&tensor, 0.5,
                                    anchors, sizeof(anchors)/sizeof(anchors[0]),
                                    8, detections, sizeof(detections)/sizeof(detections[0]));
    // assert
    EXPECT_EQ(res, 1);

    EXPECT_FLOAT_EQ(detections[0].conf, 0.6688802603710086);
    EXPECT_NEAR(detections[0].x, 0.276085, 1e-5);
    EXPECT_NEAR(detections[0].y, 0.47097, 1e-5);
    EXPECT_NEAR(detections[0].w, 3.23209, 1e-5);
    EXPECT_NEAR(detections[0].h, 5.21666, 1e-5);
}

class TestNms: public testing::Test {
protected:
    struct Detection detections[3];

    virtual void SetUp()
    {
        const int num_dets = sizeof(detections)/sizeof(detections[0]);
        for (int i=0; i<num_dets; ++i) {
            detections[i].x = i;
            detections[i].y = i;
            detections[i].w = 5;
            detections[i].h = 5;
            detections[i].conf = 0.1;
        }
    }

    virtual void TearDown()
    {
    }
};

TEST_F(TestNms, high_threshold_no_suppression)
{
    // setup
    const float thresh = 0.8;
    detections[0].conf = 0.5;
    detections[1].conf = 0.25;
    detections[2].conf = 0.125;

    // act
    post_nms(detections, sizeof(detections)/sizeof(detections[0]), thresh);

    // assert
    EXPECT_EQ(detections[0].conf, 0.5);
    EXPECT_EQ(detections[1].conf, 0.25);
    EXPECT_EQ(detections[2].conf, 0.125);
}

TEST_F(TestNms, low_threshold_suppression)
{
    // setup
    const float thresh = 0.4;
    detections[0].conf = 0.5;
    detections[1].conf = 0.25;
    detections[2].conf = 0.125;

    // act
    post_nms(detections, sizeof(detections)/sizeof(detections[0]), thresh);

    // assert
    EXPECT_EQ(detections[0].conf, 0.5);
    EXPECT_EQ(detections[1].conf, 0.0);
    EXPECT_EQ(detections[2].conf, 0.125);
}

TEST_F(TestNms, low_threshold_suppression_different_order)
{
    // setup
    const float thresh = 0.4;
    detections[0].conf = 0.125;
    detections[1].conf = 0.25;
    detections[2].conf = 0.5;

    // act
    post_nms(detections, sizeof(detections)/sizeof(detections[0]), thresh);

    // assert
    EXPECT_EQ(detections[0].conf, 0.5);
    EXPECT_EQ(detections[1].conf, 0.0);
    EXPECT_EQ(detections[2].conf, 0.125);
}
