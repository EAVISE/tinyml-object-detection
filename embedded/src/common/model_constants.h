#ifndef MODEL_CONSTANTS_H
#define MODEL_CONSTANTS_H

#include "data_types.h"

/*
 *  Model selection
 */
//#define MODEL_45
#define MODEL_90

/*
 *  Constants related to input/output shapes
 */

#define MODEL_INPUT_WIDTH           32
#define MODEL_INPUT_HEIGHT          24
#define MODEL_INPUT_LEN             (MODEL_INPUT_WIDTH * MODEL_INPUT_HEIGHT)

#define MODEL_OUTPUT_WIDTH          8
#define MODEL_OUTPUT_HEIGHT         6
#define MODEL_OUTPUT_CHANNELS       25
#define MODEL_OUTPUT_LEN            (MODEL_OUTPUT_WIDTH * MODEL_OUTPUT_HEIGHT * MODEL_OUTPUT_CHANNELS)

#define MODEL_COORD_SCALE           4
#define MODEL_NUM_ANCHORS           5
#define MODEL_MAX_DETECTIONS        (MODEL_OUTPUT_WIDTH * MODEL_OUTPUT_HEIGHT * MODEL_NUM_ANCHORS)

/*
 *  model specific constants
 */

#ifdef MODEL_45

// input/output quantization parameters. These are extracted from the .tflite model file with print_quant_params.py
#define MODEL_INPUT_QUANT_SCALE     0.10532574355602264
#define MODEL_INPUT_QUANT_ZERO      -74
#define MODEL_OUTPUT_QUANT_SCALE    0.03067750856280327
#define MODEL_OUTPUT_QUANT_ZERO     57

#define MODEL_THRESHOLD             0.385   // optimized by F1-score curve

#else

// input/output quantization parameters. These are extracted from the .tflite model file
#define MODEL_INPUT_QUANT_SCALE     0.13219480216503143
#define MODEL_INPUT_QUANT_ZERO      -92
#define MODEL_OUTPUT_QUANT_SCALE    0.028184430673718452
#define MODEL_OUTPUT_QUANT_ZERO     50

#define MODEL_THRESHOLD             0.391   // optimized by F1-score curve

#endif

extern const struct Anchor k_anchors[MODEL_NUM_ANCHORS];

/*
 *  Others
 */

// input normalization parameters, these can be found in microyolo.cfg training/testing config
//#define MODEL_INPUT_NORM_MEAN       2237
#define MODEL_INPUT_NORM_STD        155

// input scale and zero that combine the normalization and the quantization parameters
#define MODEL_INPUT_SCALE           (MODEL_INPUT_NORM_STD * MODEL_INPUT_QUANT_SCALE)
#define MODEL_INPUT_ZERO            MODEL_INPUT_QUANT_ZERO

#define MODEL_THRESHOLD_FOR_BG_SUB  0.3
#define MODEL_NMS_THRESHOLD         0.3

#define MODEL_EMA_DECAY             0.99    // for background subtraction

#define MODEL_BACKGROUND_INIT_STEPS 3

#endif /* MODEL_CONSTANTS_H */
