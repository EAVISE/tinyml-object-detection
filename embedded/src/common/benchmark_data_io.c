#include "benchmark_data_io.h"
#include "serial_port.h"

int benchmark_read_input(void* serial_port_handle, void* buffer, int len)
{
    uint16_t input_payload_len;
    uint8_t pre0=0, pre1;

    // wait for preamble detection
    do {
        pre1 = pre0;
        serial_read_block(serial_port_handle, &pre0, 1);
    } while(pre0 != 0xbe || pre1 != 0xef);


    // get length value
    serial_read_block(serial_port_handle, &input_payload_len, sizeof(input_payload_len));

    // wait for payload data
    if (input_payload_len > len) {
        return -1;
    }

    serial_read_block(serial_port_handle, buffer, input_payload_len);

    return input_payload_len;
}

void benchmark_write_output(void* serial_port_handle, const struct Detection* detections, int len, float thresh, unsigned long time)
{
    int effective_len = 0;

    // Send preamble
    const uint16_t preamble = 0xbeef;
    serial_write(serial_port_handle, &preamble, sizeof(preamble));

    // Send inference time
    const uint32_t _time = time;
    serial_write(serial_port_handle, &_time, sizeof(_time));

    // Find out effecitive len of detections (excluding detections with conf == 0.0)
    for (int i=0; i<len; i++) {
        if (detections[i].conf < thresh)
            continue;

        effective_len++;
    }

    // Send number of bytes we are going to transmit
    const uint16_t output_payload_len = effective_len * (5 * sizeof(float));
    serial_write(serial_port_handle, &output_payload_len, sizeof(output_payload_len));

    // Send number of detection:
    // x, y, w, h, conf, ... x, y, w, h, conf
    for (int i=0; i<len; i++) {
        if (detections[i].conf < thresh)
            continue;

        serial_write(serial_port_handle, &detections[i].x, sizeof(detections[i].x));
        serial_write(serial_port_handle, &detections[i].y, sizeof(detections[i].y));
        serial_write(serial_port_handle, &detections[i].w, sizeof(detections[i].w));
        serial_write(serial_port_handle, &detections[i].h, sizeof(detections[i].h));
        serial_write(serial_port_handle, &detections[i].conf, sizeof(detections[i].conf));

        delay(2);   // give remote side some time to process the data in case the amount of boxes is large
    }
}
