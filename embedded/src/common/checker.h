#ifndef CHECKER_H
#define CHECKER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

void diff_buffers(const int8_t* a, const int8_t* b, size_t len, float* mse, float* mae);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif /* CHECKER_H */
