#ifndef DETECTOR_H
#define DETECTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <stdio.h>
#include "data_types.h"

/*
 *  printf-like function that is called by the detector on failure
 */
void detector_fail(const char *format, ...);

/*
 *  Function prototype used by detector_main to acquire an image
 *  image_data:     array to store the image data, pre-allocated by detector_main. Expected data layout is (H x W)
 *  length:         number of int16_t elements in the array
 *  The function should return 0 on success and -1 on failure.
 */
int detector_get_image(int16_t* image_data, int length);

/*
 *  Function prototype used by detector_main to publish the detections and input image
 *  image_data:     array with the data of the input image
 *  image_data_len: number of int16_t elements in the array
 *  detections:     array with detected bounding boxes and scores
 *  detections_len: number of detections
 *  thresh:         minimum threshold for a detection to be considered
 *
 *  WARNING: Only detections with a confidence score >= thresh should be considered!!!!
 */
void detector_publish_result(const int16_t* image_data, int image_data_len, const struct Detection* detections, int detections_len, float thresh);

/*
 *  Detector main function. This function never returns and calls:
 *  1) acquire image through detector_get_image()
 *  2) run detector on image
 *  3) deliver detected bounding boxes and input image through detector_publish_result()
 */
void detector_main(void);

#ifdef __cplusplus
}
#endif

#endif /* DETECTOR_H */
