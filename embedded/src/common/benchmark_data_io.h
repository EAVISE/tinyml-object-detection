#ifndef BENCHMARK_DATA_IO_H
#define BENCHMARK_DATA_IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "data_types.h"

/*
 *  Delay function in ms. Needs to be implemented by the application
 */
void delay(int ms);

/*
 *  Get raw input data from benchmark program
 *  buffer: data buffer to store received data
 *  len:    number of bytes that the buffer can hold.
 *  The function will block until len bytes were received
 */
int benchmark_read_input(void* serial_port_handle, void* buffer, int len);

/*
 *  Write detection back to benchmark program
 *  detections: Calculated detections
 *  len:        Number of detections
 *  thresh:     Detections with a confidence below this threshold are not considered
 *  time:       Inference time
 */
void benchmark_write_output(void* serial_port_handle, const struct Detection* detections, int len, float thresh, unsigned long time);

#ifdef __cplusplus
}
#endif

#endif /* BENCHMARK_DATA_IO_H */
