#include "serial_port.h"

void serial_read_block(void* handle, void* data, int len)
{
    unsigned char* p = data;
    int read_bytes = 0;

    do {
        const int res = serial_read(handle, &p[read_bytes], len - read_bytes);
        if (res > 0)
            read_bytes += res;
    } while (read_bytes < len);
}
