#include "checker.h"
#include <stdio.h>


void diff_buffers(const int8_t* a, const int8_t* b, size_t len, float* mse, float* mae)
{
    size_t i;
    unsigned int sum_abs_values = 0;
    unsigned int sum_squared_values = 0;

    for (i=0; i<len; i++) {
        sum_abs_values += abs(a[i] - b[i]);
        sum_squared_values += (a[i] - b[i]) * (a[i] - b[i]);
    }
    *mse = sum_squared_values / (float)(len);
    *mae = sum_abs_values / (float)(len);
}
