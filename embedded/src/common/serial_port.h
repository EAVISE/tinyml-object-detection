#ifndef SERIAL_PORT_H
#define SERIAL_PORT_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  Write data to serial port.
 *  handle: handle to serial port object
 *  data:   data buffer
 *  len:    number of bytes to write
 *
 *  NOTE: Needs to be implemented by application
 */
void serial_write(void* handle, const void* data, int len);

/*
 *  Read data from serial port.
 *  handle: handle to serial port object
 *  data:   data buffer to store received data
 *  len:    length of the data buffer in bytes
 *  Returns the number of data bytes read
 *
 *  NOTE: Needs to be implemented by application
 */
int serial_read(void* handle, void* data, int len);

/*
 *  Read data from serial port. Block until len bytes received
 *  handle: handle to serial port object
 *  data:   data buffer to store received data
 *  len:    length of the data buffer in bytes
 */
void serial_read_block(void* handle, void* data, int len);

#ifdef __cplusplus
}
#endif

#endif /* SERIAL_PORT_H */
