#include "background.h"
#include "model_constants.h"
#include "macros.h"
#include <string.h>
#include <math.h>

int background_init(const struct Tensor* input, struct Tensor* background)
{
    static int counter = 0;
    int i;

    if (input == NULL || background == NULL) {
        counter = 0;
        return -1;
    }

    const int length = input->w * input->h;

    if (counter == 0) {
        for (i=0; i<length; i++) {
            background->data.int16[i] = input->data.int16[i];
        }
    } else {
        for (i=0; i<length; i++) {
            background->data.int16[i] += input->data.int16[i];
        }
    }
    counter++;

    if (counter == MODEL_BACKGROUND_INIT_STEPS) {
        for (i=0; i<length; i++) {
            background->data.int16[i] /= MODEL_BACKGROUND_INIT_STEPS;
        }
        counter = 0;
        return 0;
    }

    return -1;
}

void background_create_bbox_mask(const struct Tensor* input,
                                 const struct Detection* detections,
                                 int detections_len,
                                 float margin, float thresh,
                                 uint8_t* mask)
{
    int i, x, y;
    const int x_max = input->w;
    const int y_max = input->h;

    // fill mask with zeros
    memset(mask, 0, input->w * input->h);

    // iterate through detections
    for (i=0; i<detections_len; i++) {
        if (detections[i].conf < thresh || detections[i].w == 0 || detections[i].h == 0)
            continue;

        const int x1 = MAX(MIN((int)roundf(detections[i].x - margin), x_max), 0);
        const int y1 = MAX(MIN((int)roundf(detections[i].y - margin), y_max), 0);
        const int x2 = MAX(MIN((int)roundf(detections[i].x + detections[i].w + margin), x_max), 0);
        const int y2 = MAX(MIN((int)roundf(detections[i].y + detections[i].h + margin), y_max), 0);

        // fill bbox area with ones
        for (y=y1; y<y2; y++) {
            for (x=x1; x<x2; x++) {
                mask[y*input->w + x] = 1;
            }
        }
    }
}

void background_update(const struct Tensor* input,
                       const struct Detection* detections,
                       int detections_len,
                       float ema_decay, float thresh,
                       struct Tensor* background)
{
    int i;
    const int num_pixels = input->w * input->h;
    uint8_t mask[num_pixels];

    background_create_bbox_mask(input, detections, detections_len, 1, thresh, mask);

    for (i=0; i<num_pixels; i++) {
        const int16_t value = mask[i] ? background->data.int16[i] : input->data.int16[i];
        background->data.int16[i] = ema_decay * background->data.int16[i] + (1 - ema_decay) * value;
    }
}
