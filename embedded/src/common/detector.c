#include "detector.h"
#include "model_constants.h"
#include "tvmgen_default.h"
#include "post_process.h"
#include "background.h"
#include "macros.h"

void detector_main(void)
{
    // Made all buffers static because I was unable to increase the stack size in mbed-os
    // lazy fix :-D
    static int16_t input_data[MODEL_INPUT_LEN];
    static int16_t background_data[MODEL_INPUT_LEN];
    static int8_t input_data_i8[MODEL_INPUT_LEN];
    static int8_t output_data_i8[MODEL_OUTPUT_LEN];
    static struct Detection detections[MODEL_MAX_DETECTIONS];

    // 16-bit input and background images
    struct Tensor input = {
        .h = MODEL_INPUT_HEIGHT,
        .w = MODEL_INPUT_WIDTH,
        .c = 1,
        .q_scale = MODEL_INPUT_SCALE,
        .q_zero = MODEL_INPUT_ZERO,
        .data.int16 = input_data
    };
    struct Tensor background = {
        .h = MODEL_INPUT_HEIGHT,
        .w = MODEL_INPUT_WIDTH,
        .c = 1,
        .data.int16 = background_data
    };

    // 8-bit output tensor
    struct Tensor output = {
        .h = MODEL_OUTPUT_HEIGHT,
        .w = MODEL_OUTPUT_WIDTH,
        .c = MODEL_OUTPUT_CHANNELS,
        .q_scale = MODEL_OUTPUT_QUANT_SCALE,
        .q_zero = MODEL_OUTPUT_QUANT_ZERO,
        .data.int8 = output_data_i8
    };
    struct tvmgen_default_inputs tvm_input = {.input_1_int8 = input_data_i8};
    struct tvmgen_default_outputs tvm_output = {.Identity_int8 = output_data_i8};

    // initialize background image
    int ret = -1;
    while (ret != 0) {
        if (detector_get_image(input_data, MODEL_INPUT_LEN) != 0) {
            detector_fail("Acquiring input image during init\r\n");
        }

        // init background with read image
        ret = background_init(&input, &background);

        // publish an empty detection list since the detector is not yet initialized
        detector_publish_result(input_data, MODEL_INPUT_LEN, detections, 0, MODEL_THRESHOLD);
    }

    // main loop
    unsigned int counter = 1;
    int num_dets = 0;
    while(1) {

        // get input image
        if (detector_get_image(input_data, MODEL_INPUT_LEN) != 0) {
            //continue;   // try again
            detector_fail("Acquire input image\r\n");
        }

        // TODO: check for potential frame corruption, in that case we should read another frame (continue statement)

        // pre-process input
        for (int i=0; i<MODEL_INPUT_LEN; i++) {
            const int32_t value = (input.data.int16[i] - background.data.int16[i]) / input.q_scale + input.q_zero;
            input_data_i8[i] = MAX(MIN(127, value), -128);
        }

        // infer model
        if (tvmgen_default_run(&tvm_input, &tvm_output) != 0) {
            detector_fail("Infer model\r\n");
        }

        // post-process output
        num_dets = post_get_bboxes(&output, MIN(MODEL_THRESHOLD_FOR_BG_SUB, MODEL_THRESHOLD),
                                   k_anchors, MODEL_NUM_ANCHORS,
                                   MODEL_COORD_SCALE, detections,
                                   MODEL_MAX_DETECTIONS);
        if (num_dets < 0) {
            detector_fail("Get bounding boxes\r\n");
        }
        post_nms(detections, num_dets, MODEL_NMS_THRESHOLD);    // suppress by setting confidence to zero

        // update background image once in a while
        if (counter % 25 == 0) {
            background_update(&input, detections, num_dets, MODEL_EMA_DECAY, MODEL_THRESHOLD_FOR_BG_SUB, &background);
        }
        counter++;

        // publish result
        detector_publish_result(input.data.int16, MODEL_INPUT_LEN, detections, num_dets, MODEL_THRESHOLD);
    }
}
