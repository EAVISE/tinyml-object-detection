#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

struct Detection
{
    float x;
    float y;
    float w;
    float h;
    float conf;
};

struct Anchor
{
    float w;
    float h;
};

union TensorDataType
{
    int8_t* int8;
    int16_t* int16;
    void* data;
};

struct Tensor
{
    int h;
    int w;
    int c;
    int32_t q_zero;
    float q_scale;
    union TensorDataType data;
};

#ifdef __cplusplus
}
#endif

#endif /* DATA_TYPES_H */
