#include "mbed.h"
#include "stdlib.h"
#include <math.h>

#include "tensorflow/lite/micro/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "model.h"
#include "checker.h"

#ifdef MODEL_90
#include "input_90.h"
#include "output_90.h"
#else
#include "input_45.h"
#include "output_45.h"
#endif

namespace {

constexpr int k_baudrate = 115200;

Timer timer;

#if MBED_MAJOR_VERSION == 5
Serial serial(USBTX, USBRX);
#else
BufferedSerial serial(USBTX, USBRX);
#endif

tflite::MicroInterpreter* g_interpreter = nullptr;
}  // namespace


bool setup() {
    static tflite::MicroErrorReporter micro_error_reporter;
    constexpr int kTensorArenaSize = 37 * 1000;
    static uint8_t tensor_arena[kTensorArenaSize];

    // inspect model version
    const tflite::Model* model = tflite::GetModel(g_model);
    if (model->version() != TFLITE_SCHEMA_VERSION) {
        printf("Model provided is schema version %ld not equal "
               "to supported version %d.\n", model->version(), TFLITE_SCHEMA_VERSION);
        return false;
    }

    // define resolver with required operations
    static tflite::MicroMutableOpResolver<3> micro_op_resolver;
    micro_op_resolver.AddConv2D();
    micro_op_resolver.AddDepthwiseConv2D();
    micro_op_resolver.AddPad();

    // Build an interpreter to run the model with.
    static tflite::MicroInterpreter static_interpreter(
        model, micro_op_resolver, tensor_arena, kTensorArenaSize, &micro_error_reporter);
    g_interpreter = &static_interpreter;

    // Allocate memory from the tensor_arena for the model's tensors.
    TfLiteStatus allocate_status = g_interpreter->AllocateTensors();
    if (allocate_status != kTfLiteOk) {
        printf("AllocateTensors() failed\n");
        return false;
    }

    return true;
}

void infer() {
    float mse, mae;
    TfLiteTensor* input = g_interpreter->input(0);
    TfLiteTensor* output = g_interpreter->output(0);

    // copy to input buffer
    memcpy(input->data.int8, input_0_bin, input_0_bin_len);

    timer.start();
    if (kTfLiteOk != g_interpreter->Invoke()) {
        printf("Invoke failed\n");
        return;
    }
    timer.stop();

    // compare output buffer with expected values
    diff_buffers(reinterpret_cast<const int8_t*>(output_0_bin), output->data.int8, output_0_bin_len, &mse, &mae);
    printf("MSE = %d\r\n", static_cast<int>(mse * 1000000));
    printf("MAE = %d\r\n", static_cast<int>(mae * 1000000));

    #if MBED_MAJOR_VERSION == 5
    const int time = timer.read_us();
    printf("Inference time = %d us\r\n", time);
    #else
    const unsigned long time = timer.elapsed_time().count();
    printf("Inference time = %lu us\r\n", time);
    #endif

    timer.reset();
}

int main(void) {
    #if MBED_MAJOR_VERSION == 5
    serial.baud(k_baudrate);
    #else
    serial.set_baud(k_baudrate);
    #endif

    if (!setup()) {
        while(true) {
        }
    }

    infer();
    while (true) {
    }
}



