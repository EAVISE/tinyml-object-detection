# Install toolchains

Make sure to first install the mbed commandline tools and a compiler (this code was tested with gcc): https://os.mbed.com/docs/mbed-os/v6.7/build-tools/install-and-set-up.html

# Compile and flash code for STM32F746G-DISCO board

First download latest mbed-os (This code was tested with version 6.7). This can be done with:

```
mbed deploy
```

Then compile and flash the code with:

```
mbed compile -m DISCO_F746NG -t GCC_ARM --flash
```

# Compile and flash code for STM32F407VG-DISCO board

This board is not officially supported anymore (as a lot of boards are) by mbed-os 6. We therefore need mbed-os 5.X.
We tested this with the latest mbed-os 5 release (version 5.15.6 at the time):

```
mbed deploy
cd mbed-os
git checkout mbed-os-5.15.6
```

In addition, we need to update some outdated header files in mbed-os 5 which are included in the tensorflow code repository:

```
cp tensorflow/lite/micro/tools/make/downloads/cmsis/CMSIS/Core/Include/* mbed-os/cmsis/TARGET_CORTEX_M/
cp tensorflow/lite/micro/tools/make/downloads/cmsis/CMSIS/DSP/Include/arm_math.h mbed-os/cmsis/TARGET_CORTEX_M/
```

Then compile the code with:

```
mbed compile -m DISCO_F407VG -t GCC_ARM
```

This board has no virtual serial port through the st-link usb port, so flashing the mbed software with --flash is not supported.
You need to download the code using the st-flash tool:

```
st-flash write BUILD/DISCO_F407VG/GCC_ARM-RELEASE/mbed.bin 0x8000000
```

Then hookup an FTDI chip using pins PA3 (RX) and PA2 (TX) and GND to be able to communicate with the application.

# Optimizations

## Compiler

You can compile the code in release mode with:

```
mbed compile -m <TARGET> -t GCC_ARM --profile mbed-os/tools/profiles/release.json
```

By default, the 'release' profile optimizes for smallest size. If you want to optimize for speed,
you can replace ```-Os``` the file (under ```GCC_ARM```) with ```-O3```.

## Optimized TFLite CMSIS kernels

Folder ```tensorflow/lite/micro/kernels/cmsis_nn``` contains optimized kernels for some layer ops like convolutions, fully connected
layers, add, softmax, etc... By default this code uses those kernels which greatly improve the performance.
If you want to compare with the reference kernels from tensorflow lite (which are slower) to see the speedup,
modify the content of file ```tensorflow/lite/micro/kernels/.mbedignore```

from:

```
add.cc
conv.cc
depthwise_conv.cc
fully_connected.cc
mul.cc
pooling.cc
softmax.cc
svdf.cc
```

to:

```
cmsis_nn/*.cc
```

and compile the code again.
