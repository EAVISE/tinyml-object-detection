#include "mbed.h"
#include "stdlib.h"
#include "detector.h"
#include "benchmark_data_io.h"

namespace {

constexpr int k_baudrate = 500000;

Timer timer;

#if MBED_MAJOR_VERSION == 5
Serial serial(USBTX, USBRX);
#else
BufferedSerial serial(USBTX, USBRX);
#endif

DigitalOut led1(LED1);

}  // namespace

void delay(int ms)
{
    ThisThread::sleep_for(ms);
}

void detector_fail(const char *format, ...)
{
    led1 = 1;

    va_list args;
    printf("FAIL: ");

    va_start(args, format);
    vprintf(format, args);
    va_end(args);

    // loop forever
    while(1);
}

int detector_get_image(int16_t* image_data, int length)
{
    // read image from serial port
    int res = benchmark_read_input(&serial, image_data, length * sizeof(int16_t));

    if (res != (length * (int)sizeof(int16_t)))
        return -1;

    // start timer
    timer.start();
    return 0;
}

void detector_publish_result(const int16_t* image_data, int image_data_len, const struct Detection* detections, int detections_len, float thresh)
{
    // we don't use these in this application
    (void)image_data;
    (void)image_data_len;

    // stop timer and get time in micro seconds
    timer.stop();
    #if MBED_MAJOR_VERSION == 5
    const int time = timer.read_us();
    #else
    const unsigned long time = timer.elapsed_time().count();
    #endif

    // write detections to serial port
    benchmark_write_output(&serial, detections, detections_len, thresh, time);

    timer.reset();
}

int main(void) {

    #if MBED_MAJOR_VERSION == 5
    serial.baud(k_baudrate);
    #else
    serial.set_baud(k_baudrate);
    #endif

    detector_main();
}
