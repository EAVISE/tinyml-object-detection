#include "serial_port.h"
#include "mbed.h"

#if MBED_MAJOR_VERSION == 5
typedef Serial mbed_serial_port_t;
#else
typedef BufferedSerial mbed_serial_port_t;
#endif /* MBED_MAJOR_VERSION */

void serial_write(void* handle, const void* data, int len)
{
    mbed_serial_port_t* serial_port = reinterpret_cast<mbed_serial_port_t*>(handle);
    serial_port->write(data, len);
}

int serial_read(void* handle, void* data, int len)
{
    mbed_serial_port_t* serial_port = reinterpret_cast<mbed_serial_port_t*>(handle);
    return serial_port->read(data, len);
}
