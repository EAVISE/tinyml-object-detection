#include "mbed.h"
#include "stdlib.h"

#include "tvmgen_default.h"
#include "checker.h"

#define MODEL_90

#ifdef MODEL_90
#include "input_90.h"
#include "output_90.h"
#else
#include "input_45.h"
#include "output_45.h"
#endif

namespace {

constexpr int k_baudrate = 115200;

Timer timer;

#if MBED_MAJOR_VERSION == 5
Serial serial(USBTX, USBRX);
#else
BufferedSerial serial(USBTX, USBRX);
#endif

int8_t input_buffer[32*24*1];
int8_t output_buffer[6*8*25];
}  // namespace


bool setup() {
    printf("Ready\r\n");

    return true;
}

void infer() {
    float mse, mae;
    struct tvmgen_default_inputs input;
    struct tvmgen_default_outputs output;

    // copy to input buffer
    memcpy(input_buffer, input_0_bin, input_0_bin_len);

    input.input_1_int8 = &input_buffer;
    output.Identity_int8 = &output_buffer;

    timer.start();
    if (tvmgen_default_run(&input, &output) != 0) {
        printf("Failed to run TVM network model\r\n");
    }
    timer.stop();

    // compare output buffer with expected values
    diff_buffers(reinterpret_cast<const int8_t*>(output_0_bin), output_buffer, output_0_bin_len, &mse, &mae);
    printf("MSE = %d\r\n", static_cast<int>(mse * 1000000));
    printf("MAE = %d\r\n", static_cast<int>(mae * 1000000));

    #if MBED_MAJOR_VERSION == 5
    const int time = timer.read_us();
    #else
    const unsigned long time = timer.elapsed_time().count();
    #endif

    printf("Inference time = %lu us\r\n", time);

    timer.reset();
}

int main(void) {
    #if MBED_MAJOR_VERSION == 5
    serial.baud(k_baudrate);
    #else
    serial.set_baud(k_baudrate);
    #endif

    if (!setup()) {
        while(true) {
        }
    }

    infer();
    while (true) {
    }
}



