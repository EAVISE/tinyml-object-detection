import tvm
from tvm import relay
from tvm.relay import transform
from tvm.driver.tvmc.model import TVMCModel
from tvm.driver.tvmc.compiler import compile_model
from tvm.relay.backend import Executor, Runtime
import argparse
import tflite

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('model_file', help='The .tflite model file')
parser.add_argument('output_file', help='The .tar output file')
args = parser.parse_args()

# load from tflite file
with open(args.model_file, "rb") as f:
    tflite_model_buf = f.read()

tflite_model = tflite.Model.GetRootAsModel(tflite_model_buf, 0)
mod, params = relay.frontend.from_tflite(tflite_model)

model = TVMCModel(mod, params)
compile_model(tvmc_model=model,
              target="cmsis-nn,c",
              executor=Executor("aot",
                                {"interface-api": "c",
                                 "unpacked-api": 1}
                                ),
              runtime=Runtime("crt"),
              output_format="mlf",
              package_path=args.output_file,
              pass_context_configs=[
                    'tir.usmp.enable=1',
                    'tir.usmp.algorithm=hill_climb',
                    'tir.disable_storage_rewrite=1',
                    'tir.disable_vectorize=1']
            )
