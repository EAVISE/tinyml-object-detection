// tvm target: c -keys=cpu -link-params=0
#define TVM_EXPORTS
#include "tvm/runtime/c_runtime_api.h"
#include "tvm/runtime/c_backend_api.h"
#include <math.h>
#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_pad(int8_t* placeholder, int8_t* T_pad, uint8_t* global_const_workspace_8_var, uint8_t* global_workspace_9_var) {
  for (int32_t ax0_ax1_fused = 0; ax0_ax1_fused < 25; ++ax0_ax1_fused) {
    for (int32_t ax2 = 0; ax2 < 33; ++ax2) {
      for (int32_t ax3_outer = 0; ax3_outer < 2; ++ax3_outer) {
        for (int32_t ax3_inner = 0; ax3_inner < 16; ++ax3_inner) {
          if (((ax3_outer * 8) + (ax3_inner >> 1)) < 9) {
            int32_t cse_var_2 = (ax2 * 18);
            int32_t cse_var_1 = (ax3_outer * 16);
            T_pad[((((ax0_ax1_fused * 594) + cse_var_2) + cse_var_1) + ax3_inner)] = (((1 <= ax0_ax1_fused) && (1 <= ax2)) ? placeholder[(((((ax0_ax1_fused * 576) + cse_var_2) + cse_var_1) + ax3_inner) - 594)] : (int8_t)-128);
          }
        }
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default_fused_nn_pad_1(int8_t* placeholder, int8_t* T_pad, uint8_t* global_const_workspace_22_var, uint8_t* global_workspace_23_var) {
  for (int32_t ax0_ax1_fused = 0; ax0_ax1_fused < 13; ++ax0_ax1_fused) {
    for (int32_t ax2 = 0; ax2 < 17; ++ax2) {
      for (int32_t ax3_outer = 0; ax3_outer < 3; ++ax3_outer) {
        for (int32_t ax3_inner = 0; ax3_inner < 16; ++ax3_inner) {
          if (((ax3_outer * 2) + (ax3_inner >> 3)) < 5) {
            int32_t cse_var_2 = (ax2 * 40);
            int32_t cse_var_1 = (ax3_outer * 16);
            T_pad[((((ax0_ax1_fused * 680) + cse_var_2) + cse_var_1) + ax3_inner)] = (((1 <= ax0_ax1_fused) && (1 <= ax2)) ? placeholder[(((((ax0_ax1_fused * 640) + cse_var_2) + cse_var_1) + ax3_inner) - 680)] : (int8_t)-128);
          }
        }
      }
    }
  }
  return 0;
}

#ifdef __cplusplus
extern "C"
#endif
TVM_DLL int32_t tvmgen_default___tvm_main__(int8_t* input_1_int8_buffer_var, int8_t* Identity_int8_buffer_var, uint8_t* global_const_workspace_0_var, uint8_t* global_workspace_1_var) {
  void* constant_54_let = (&(global_const_workspace_0_var[6544]));
  void* constant_53_let = (&(global_const_workspace_0_var[9040]));
  void* constant_52_let = (&(global_const_workspace_0_var[9200]));
  void* constant_50_let = (&(global_const_workspace_0_var[9520]));
  void* constant_47_let = (&(global_const_workspace_0_var[11440]));
  void* constant_45_let = (&(global_const_workspace_0_var[11728]));
  void* constant_34_let = (&(global_const_workspace_0_var[13136]));
  void* constant_44_let = (&(global_const_workspace_0_var[11872]));
  void* constant_33_let = (&(global_const_workspace_0_var[13264]));
  void* constant_58_let = (&(global_const_workspace_0_var[8400]));
  void* constant_25_let = (&(global_const_workspace_0_var[14160]));
  void* constant_43_let = (&(global_const_workspace_0_var[12016]));
  void* constant_8_let = (&(global_const_workspace_0_var[17328]));
  void* constant_42_let = (&(global_const_workspace_0_var[7264]));
  void* constant_56_let = (&(global_const_workspace_0_var[8720]));
  void* constant_41_let = (&(global_const_workspace_0_var[12160]));
  void* constant_29_let = (&(global_const_workspace_0_var[13648]));
  void* constant_38_let = (&(global_const_workspace_0_var[12592]));
  void* constant_64_let = (&(global_const_workspace_0_var[10800]));
  void* constant_67_let = (&(global_const_workspace_0_var[10480]));
  void* constant_37_let = (&(global_const_workspace_0_var[12736]));
  void* constant_35_let = (&(global_const_workspace_0_var[13008]));
  void* constant_30_let = (&(global_const_workspace_0_var[7568]));
  void* constant_48_let = (&(global_const_workspace_0_var[1568]));
  void* constant_14_let = (&(global_const_workspace_0_var[17008]));
  void* constant_4_let = (&(global_const_workspace_0_var[17424]));
  void* constant_71_let = (&(global_const_workspace_0_var[9840]));
  void* constant_51_let = (&(global_const_workspace_0_var[9360]));
  void* constant_73_let = (&(global_const_workspace_0_var[16272]));
  void* constant_9_let = (&(global_const_workspace_0_var[17296]));
  void* constant_40_let = (&(global_const_workspace_0_var[12304]));
  void* constant_28_let = (&(global_const_workspace_0_var[13776]));
  void* constant_7_let = (&(global_const_workspace_0_var[17360]));
  void* constant_49_let = (&(global_const_workspace_0_var[9680]));
  void* constant_57_let = (&(global_const_workspace_0_var[8560]));
  void* constant_15_let = (&(global_const_workspace_0_var[16928]));
  void* constant_89_let = (&(global_const_workspace_0_var[14848]));
  void* constant_39_let = (&(global_const_workspace_0_var[12448]));
  void* constant_11_let = (&(global_const_workspace_0_var[17520]));
  void* constant_13_let = (&(global_const_workspace_0_var[17088]));
  void* constant_70_let = (&(global_const_workspace_0_var[10000]));
  void* constant_16_let = (&(global_const_workspace_0_var[16848]));
  void* constant_6_let = (&(global_const_workspace_0_var[17168]));
  void* constant_86_let = (&(global_const_workspace_0_var[15184]));
  void* constant_31_let = (&(global_const_workspace_0_var[13520]));
  void* constant_3_let = (&(global_const_workspace_0_var[17456]));
  void* constant_23_let = (&(global_const_workspace_0_var[16368]));
  void* constant_55_let = (&(global_const_workspace_0_var[8880]));
  void* constant_1_let = (&(global_const_workspace_0_var[17584]));
  void* constant_21_let = (&(global_const_workspace_0_var[16528]));
  void* constant_36_let = (&(global_const_workspace_0_var[2896]));
  void* constant_27_let = (&(global_const_workspace_0_var[13904]));
  void* constant_32_let = (&(global_const_workspace_0_var[13392]));
  void* constant_12_let = (&(global_const_workspace_0_var[12880]));
  void* constant_2_let = (&(global_const_workspace_0_var[17488]));
  void* constant_26_let = (&(global_const_workspace_0_var[14032]));
  void* constant_0_let = (&(global_const_workspace_0_var[17232]));
  void* constant_10_let = (&(global_const_workspace_0_var[17552]));
  void* constant_17_let = (&(global_const_workspace_0_var[16768]));
  void* constant_18_let = (&(global_const_workspace_0_var[8064]));
  void* constant_20_let = (&(global_const_workspace_0_var[16608]));
  void* constant_22_let = (&(global_const_workspace_0_var[16448]));
  void* constant_5_let = (&(global_const_workspace_0_var[17392]));
  void* constant_19_let = (&(global_const_workspace_0_var[16688]));
  void* constant_24_let = (&(global_const_workspace_0_var[5424]));
  void* constant_46_let = (&(global_const_workspace_0_var[11584]));
  void* constant_59_let = (&(global_const_workspace_0_var[8240]));
  void* constant_68_let = (&(global_const_workspace_0_var[10320]));
  void* constant_76_let = (&(global_const_workspace_0_var[15984]));
  void* constant_60_let = (&(global_const_workspace_0_var[0]));
  void* constant_61_let = (&(global_const_workspace_0_var[11280]));
  void* constant_62_let = (&(global_const_workspace_0_var[11120]));
  void* constant_63_let = (&(global_const_workspace_0_var[10960]));
  void* constant_92_let = (&(global_const_workspace_0_var[14624]));
  void* constant_65_let = (&(global_const_workspace_0_var[10640]));
  void* constant_66_let = (&(global_const_workspace_0_var[6912]));
  void* constant_69_let = (&(global_const_workspace_0_var[10160]));
  void* constant_72_let = (&(global_const_workspace_0_var[3920]));
  void* constant_74_let = (&(global_const_workspace_0_var[16176]));
  void* constant_75_let = (&(global_const_workspace_0_var[16080]));
  void* constant_77_let = (&(global_const_workspace_0_var[15888]));
  void* constant_78_let = (&(global_const_workspace_0_var[7856]));
  void* constant_79_let = (&(global_const_workspace_0_var[15792]));
  void* constant_80_let = (&(global_const_workspace_0_var[15696]));
  void* constant_81_let = (&(global_const_workspace_0_var[15600]));
  void* constant_82_let = (&(global_const_workspace_0_var[15504]));
  void* constant_83_let = (&(global_const_workspace_0_var[15408]));
  void* constant_87_let = (&(global_const_workspace_0_var[15072]));
  void* constant_84_let = (&(global_const_workspace_0_var[5984]));
  void* constant_85_let = (&(global_const_workspace_0_var[15296]));
  void* constant_88_let = (&(global_const_workspace_0_var[14960]));
  void* constant_90_let = (&(global_const_workspace_0_var[4784]));
  void* constant_91_let = (&(global_const_workspace_0_var[14736]));
  void* constant_93_let = (&(global_const_workspace_0_var[14512]));
  void* constant_94_let = (&(global_const_workspace_0_var[14400]));
  void* constant_95_let = (&(global_const_workspace_0_var[14288]));
  void* sid_36_let = (&(global_workspace_1_var[5952]));
  void* sid_107_let = (&(global_workspace_1_var[0]));
  void* sid_57_let = (&(global_workspace_1_var[0]));
  void* sid_21_let = (&(global_workspace_1_var[0]));
  void* sid_93_let = (&(global_workspace_1_var[1872]));
  void* sid_7_let = (&(global_workspace_1_var[0]));
  void* sid_14_let = (&(global_workspace_1_var[13824]));
  void* sid_29_let = (&(global_workspace_1_var[0]));
  void* sid_43_let = (&(global_workspace_1_var[0]));
  void* sid_22_let = (&(global_workspace_1_var[13824]));
  void* sid_50_let = (&(global_workspace_1_var[6336]));
  void* sid_64_let = (&(global_workspace_1_var[8848]));
  void* sid_65_let = (&(global_workspace_1_var[0]));
  void* sid_72_let = (&(global_workspace_1_var[8848]));
  void* sid_79_let = (&(global_workspace_1_var[1872]));
  void* sid_86_let = (&(global_workspace_1_var[0]));
  void* sid_100_let = (&(global_workspace_1_var[2928]));
  if (tvmgen_default_cmsis_nn_main_0(input_1_int8_buffer_var, constant_0_let, constant_1_let, constant_3_let, constant_5_let, sid_7_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_1(sid_7_let, constant_6_let, constant_7_let, constant_9_let, constant_11_let, sid_14_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_2(sid_14_let, constant_12_let, constant_13_let, constant_15_let, constant_17_let, sid_21_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_fused_nn_pad(sid_21_let, sid_22_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_3(sid_22_let, constant_18_let, constant_19_let, constant_21_let, constant_23_let, sid_29_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_4(sid_29_let, constant_24_let, constant_25_let, constant_27_let, constant_29_let, sid_36_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_5(sid_36_let, constant_30_let, constant_31_let, constant_33_let, constant_35_let, sid_43_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_6(sid_43_let, constant_36_let, constant_37_let, constant_39_let, constant_41_let, sid_50_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_7(sid_50_let, constant_42_let, constant_43_let, constant_45_let, constant_47_let, sid_57_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_8(sid_57_let, constant_48_let, constant_49_let, constant_51_let, constant_53_let, sid_64_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_fused_nn_pad_1(sid_64_let, sid_65_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_9(sid_65_let, constant_54_let, constant_55_let, constant_57_let, constant_59_let, sid_72_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_10(sid_72_let, constant_60_let, constant_61_let, constant_63_let, constant_65_let, sid_79_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_11(sid_79_let, constant_66_let, constant_67_let, constant_69_let, constant_71_let, sid_86_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_12(sid_86_let, constant_72_let, constant_73_let, constant_75_let, constant_77_let, sid_93_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_13(sid_93_let, constant_78_let, constant_79_let, constant_81_let, constant_83_let, sid_100_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_14(sid_100_let, constant_84_let, constant_85_let, constant_87_let, constant_89_let, sid_107_let, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  if (tvmgen_default_cmsis_nn_main_15(sid_107_let, constant_90_let, constant_91_let, constant_93_let, constant_95_let, Identity_int8_buffer_var, global_const_workspace_0_var, global_workspace_1_var) != 0 ) return -1;
  return 0;
}

