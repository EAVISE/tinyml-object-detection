# Embedded implementation

We use the *mbed* framework in our experiments

Experiments:

* `src/mbed_tflite` project code for evaluating the model's inference speed with *tflite*
* `src/mbed_tvm` project code for evaluating the model's inference speed with *micro tvm*
* `src/mbed_tvm_eval` project code for evaluating the model's accuracy on the test dataset with *micro tvm*

The first two projects measure the inference time of the model only (no pre/post processing is done).
The `mbed_tvm_eval` project includes on target pre and post processing and measure the total time.

For more detail, see the README.md files of each project
